/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LIBNUSCHEME_H_INCLUDED
#define LIBNUSCHEME_H_INCLUDED

#include <assert.h>
#include <inttypes.h>
#include <signal.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdnoreturn.h>
#include <threads.h>

#if BUILDING_LIBNUSCHEME && HAVE_VISIBILITY
# define LIBNUSCHEME_DLL_EXPORTED __attribute__ ((visibility ("default")))
#else
# define LIBNUSCHEME_DLL_EXPORTED
#endif

#if defined (__GNUC__) && defined (__x86_64__) && !defined (_WIN64)
# define NU_CALLEE_SAVED_REGISTERS 1
#endif

#if defined (__GNUC__) && (__GNUC__ > 3 || __GNUC__ == 3 && __GNUC_MINOR__ >= 1)
# define NU_ALWAYS_INLINE 1
#endif

#ifdef NU_ALWAYS_INLINE
# define NU_INLINE static inline  __attribute__ (( always_inline ))
#else
# define NU_INLINE static inline
#endif

#define NU_UNREACHABLE				\
  do						\
    {						\
      assert (false);				\
      __builtin_unreachable ();			\
    }						\
  while (false)

#define NU_PRIiFX PRIiPTR

#define NU_FALSE       NU_BOOLEAN_FALSE
#define NU_TRUE        NU_BOOLEAN_TRUE
#define NU_UNSPECIFIED NU_IMMEDIATE_UNSPECIFIED

#define NU_LABEL_INITIALIZER(num_slots_, num_labels_, self_slot_,	\
			     entry_0_, entry_1_, entry_2_, entry_3_,	\
			     entry_x_)					\
  {									\
    .num_slots = num_slots_,						\
    .num_labels = num_labels_,						\
    .self_slot = self_slot_,						\
    .entry_0 = entry_0_ ? entry_0_ : nu_default_entry_0,		\
    .entry_1 = entry_1_ ? entry_1_ : nu_default_entry_1,		\
    .entry_2 = entry_2_ ? entry_2_ : nu_default_entry_2,		\
    .entry_3 = entry_3_ ? entry_3_ : nu_default_entry_3,		\
    .entry_x = entry_x_ ? entry_x_ : nu_default_entry_x			\
  }

#define NU_CALL(proc, cont, c1, c2, c3, ...)				\
  (NU_GET_MACRO_4 (__VA_ARGS__, nu_call_3, nu_call_2, nu_call_1, nu_call_0) \
   (proc, cont, c1, c2, c3, __VA_ARGS__))

#define NU_MAKE_PROCEDURE(f, i, ...)			\
  NuProcedure f##_closure [] = {__VA_ARGS__};		\
  NuValue f = nu_get_procedure (f##_closure, i)

#define NU_MAKE_CONT_PROCEDURE(c, c_1, c_x, n, ...)			\
  NU_CONT_PROCEDURE (n) c##_cont_procedure				\
  = { .header = NU_MAKE_HEADER (NU_OBJECT_CONTINUATION, n),		\
      .entry_1 = c_1 ? c_1 : nu_default_cont_entry,			\
      .entry_x = c_x ? c_x : nu_default_cont_entry,			\
      .var = { __VA_ARGS__ }};						\
  NuValue c = nu_value_from_cont_procedure				\
    ((NuContProcedure *) &c##_cont_procedure)

#define NU_CONT_VARIABLE(v, c, i)			\
  NuValue v = nu_get_cont_closure_variable (c, i)

#define NU_FIXNUM_INITIALIZER(x) (((uintptr_t) x) << 1)

#define NU_FX_ADD(rop, op1, op2) NuValue rop = nu_fx_add (op1, op2)
#define NU_FX_ZERO_P(rop, op)	 NuValue rop = nu_fx_zero_p (op)

#define NU_CAR(obj, pair)       NuValue obj = nu_car (pair)
#define NU_CDR(obj, pair)       NuValue obj = nu_cdr (pair)
#define NU_SET_CAR_X(pair, obj) nu_pair_set_car_x (pair, obj)
#define NU_SET_CDR_X(pair, obj) nu_pair_set_cdr_x (pair, obj)

#define NU_VECTOR_REF(el, vec, idx) NuValue el = nu_vector_ref (vec, idx)
#define NU_VECTOR_SET_X(vec, idx, el) nu_vector_set_x (vec, idx, el)

/* C type of a fixnum. */
typedef intptr_t NuFixnumInt;

/* C type of a flonum. */
typedef double NuFlonumValue;

typedef uintptr_t NuCell;
typedef const NuCell NuValue;
typedef struct nu_pair NuPair;
typedef struct nu_vector NuVector;
typedef struct nu_cont_procedure NuContProcedure;
typedef const struct nu_label NuLabel;
typedef struct nu_procedure NuProcedure;
typedef NuProcedure *NuClosure;

typedef void NuContinuation (NuValue res);

#ifdef NU_CALLEE_SAVED_REGISTERS
# define NU_INIT_SIGNAL nu_sig = &nu_the_sig
# define NU_SIG (*nu_sig)
# define NU_REG_1 nu_reg_1
# define NU_REG_2 nu_reg_2
# define NU_REG_3 nu_reg_3
# define NU_REG_4 nu_reg_4
register volatile sig_atomic_t *nu_sig asm ("rbx");
register NuCell nu_reg_1 asm ("r12");
register NuCell nu_reg_2 asm ("r13");
register NuCell nu_reg_3 asm ("r14");
register NuCell nu_reg_4 asm ("r15");
extern thread_local volatile sig_atomic_t nu_the_sig;
#else
# define NU_INIT_SIGNAL
# define NU_SIG (nu_thread_local_register_file.sig)
# define NU_REG_1 (nu_thread_local_register_file.reg [0])
# define NU_REG_2 (nu_thread_local_register_file.reg [1])
# define NU_REG_3 (nu_thread_local_register_file.reg [2])
# define NU_REG_4 (nu_thread_local_register_file.reg [3])
struct nu_thread_local_register_file
{
  volatile sig_atomic_t sig;
  NuCell reg[4];
};
extern thread_local struct nu_thread_local_register_file nu_thread_local_register_file;
#endif

NU_INLINE uintptr_t nu_get_object_header (NuValue val);
NU_INLINE size_t nu_get_object_header_payload (NuValue val);
NU_INLINE bool nu_fixnum_p (NuValue val);
NU_INLINE bool nu_pointer_p (NuValue val);
NU_INLINE bool nu_object_p (NuValue val);
NU_INLINE bool nu_pair_p (NuValue val);
NU_INLINE bool nu_vector_p (NuValue val);
NU_INLINE bool nu_procedure_p (NuValue val);
NU_INLINE bool nu_boolean_p (NuValue val);
NU_INLINE bool nu_false_p (NuValue val);
NU_INLINE bool nu_unspecified_p (NuValue val);
NU_INLINE bool nu_cont_procedure_p (NuValue val);
NU_INLINE NuFixnumInt nu_value_to_int (NuValue val);
NU_INLINE NuValue nu_value_from_int (NuFixnumInt i);
NU_INLINE NuPair *nu_value_to_pair (NuValue val);
NU_INLINE NuValue nu_value_from_pair (const NuPair *pair);
NU_INLINE NuVector *nu_value_to_vector (NuValue val);
NU_INLINE NuValue nu_value_from_vector (const NuVector *vec);
NU_INLINE bool nu_value_to_bool (NuValue val);
NU_INLINE NuValue nu_value_from_bool (bool flag);
NU_INLINE NuClosure nu_value_to_closure (NuValue val);
NU_INLINE NuValue nu_value_from_closure (NuClosure clo);
NU_INLINE NuProcedure *nu_value_to_procedure (NuValue val);
NU_INLINE NuValue nu_value_from_procedure (NuProcedure *proc);
NU_INLINE NuContProcedure *nu_value_to_cont_procedure (NuValue val);
NU_INLINE NuValue nu_value_from_cont_procedure (NuContProcedure *proc);
NU_INLINE NuValue nu_fx_add (NuValue op1, NuValue op2);
NU_INLINE NuValue nu_fx_zero_p (NuValue op);
NU_INLINE NuValue nu_car (NuValue val);
NU_INLINE NuValue nu_cdr (NuValue val);
NU_INLINE void nu_set_car_x (NuValue pair, NuValue obj);
NU_INLINE void nu_set_cdr_x (NuValue pair, NuValue obj);
NU_INLINE NuValue nu_vector_length (NuValue vec);
NU_INLINE NuValue nu_vector_ref (NuValue vec, NuValue idx);
NU_INLINE void nu_vector_set_x (NuValue vec, NuValue idx, NuValue obj);
NU_INLINE NuValue nu_get_procedure (NuClosure clo, size_t index);
NU_INLINE NuValue nu_get_closure_variable (NuValue self, size_t self_slot,
					   size_t other_slot);
NU_INLINE NuValue nu_get_sibling_procedure (NuValue self, size_t self_slot,
					    size_t other_slot);
NU_INLINE noreturn void nu_call_0 (NuValue proc, NuValue cont, NuValue c1, NuValue c2,
				   NuValue c3, NuValue c4);
NU_INLINE noreturn void nu_call_1 (NuValue proc, NuValue cont, NuValue c1, NuValue c2,
				   NuValue c3, NuValue c4, NuValue arg1);
NU_INLINE noreturn void nu_call_2 (NuValue proc, NuValue cont, NuValue c1, NuValue c2,
				   NuValue c3, NuValue c4, NuValue arg1, NuValue arg2);
NU_INLINE noreturn void nu_call_3 (NuValue proc, NuValue cont, NuValue c1, NuValue c2,
				   NuValue c3, NuValue c4, NuValue arg1,
				   NuValue arg2, NuValue arg3);
NU_INLINE noreturn void nu_call_x (NuValue proc, NuValue cont, NuValue c1, NuValue c2,
				   NuValue c3, NuValue c4, NuValue arg1,
				   NuValue arg2, NuValue arg3, NuValue args);
NU_INLINE NuValue nu_get_cont_closure_variable (NuValue self, size_t slot);
NU_INLINE noreturn void nu_cont_call (NuValue proc, NuValue c1, NuValue c2,
				      NuValue c3, NuValue c4, NuValue arg);
NU_INLINE noreturn void nu_cont_apply (NuValue proc, NuValue c1, NuValue c2,
				       NuValue c3, NuValue c4, NuValue args);
NU_INLINE bool nu_thread_signal_p ();
LIBNUSCHEME_DLL_EXPORTED void nu_thread_suspend (NuContinuation*, NuValue);
LIBNUSCHEME_DLL_EXPORTED noreturn void nu_thread_exit (void);
LIBNUSCHEME_DLL_EXPORTED void nu_run ();

/* I/O */
void nu_write (/* TODO: bare port */ NuValue);

/* Implementation */

#define NU_GET_MACRO_4(_1, _2, _3, _4, NAME, ...) NAME
#define NU_GET_MACRO_5(_1, _2, _3, _4, _5, NAME, ...) NAME

#define NU_MIN_ALIGN 8
#define NU_ALIGN _Alignas (NU_MIN_ALIGN)

#define NU_POINTER_MASK 1

#define NU_TAG_MASK 7

#define NU_TAG_OBJECT    1
#define NU_TAG_PAIR      3
#define NU_TAG_PROCEDURE 5
#define NU_TAG_IMMEDIATE 7

#define NU_OBJECT_MASK 7

#define NU_OBJECT_FORWARD      0
#define NU_OBJECT_CONTINUATION 1
#define NU_OBJECT_VECTOR       2
#define NU_OBJECT_RECORD       3

#define NU_IMMEDIATE_MASK 255

#define NU_IMMEDIATE_BOOLEAN     (NU_TAG_IMMEDIATE +   0)
#define NU_IMMEDIATE_CHAR        (NU_TAG_IMMEDIATE +   8)
#define NU_IMMEDIATE_UNSPECIFIED (NU_TAG_IMMEDIATE +  16)
#define NU_IMMEDIATE_FORWARD     (NU_TAG_IMMEDIATE + 248)

#define NU_BOOLEAN_FALSE (NU_IMMEDIATE_BOOLEAN +   0)
#define NU_BOOLEAN_TRUE  (NU_IMMEDIATE_BOOLEAN + 256)

#define NU_MAKE_HEADER(t, s) ((s * (NU_OBJECT_MASK + 1)) + t)

typedef void NuEntry0 (NuValue self, NuValue cont);
typedef void NuEntry1 (NuValue self, NuValue cont, NuValue arg1);
typedef void NuEntry2 (NuValue self, NuValue cont, NuValue arg1,
		       NuValue arg2);
typedef void NuEntry3 (NuValue self, NuValue cont, NuValue arg1,
		       NuValue arg2, NuValue arg3);
typedef void NuEntryX (NuValue self, NuValue cont, NuValue arg1,
		       NuValue arg2, NuValue arg3, NuValue args);
typedef void NuContEntry1 (NuValue self, NuValue arg);
typedef void NuContEntryX (NuValue self, NuValue args);

LIBNUSCHEME_DLL_EXPORTED void nu_default_entry_0 (NuValue self,
						  NuValue cont);
LIBNUSCHEME_DLL_EXPORTED void nu_default_entry_1 (NuValue self,
						  NuValue cont, NuValue arg1);
LIBNUSCHEME_DLL_EXPORTED void nu_default_entry_2 (NuValue self,
						  NuValue cont, NuValue arg1,
						  NuValue arg2);
LIBNUSCHEME_DLL_EXPORTED void nu_default_entry_3 (NuValue self,
						  NuValue cont, NuValue arg1,
						  NuValue arg2, NuValue arg3);
LIBNUSCHEME_DLL_EXPORTED void nu_default_entry_x (NuValue self,
						  NuValue cont, NuValue arg1,
						  NuValue arg2, NuValue arg3,
						  NuValue args);
LIBNUSCHEME_DLL_EXPORTED void nu_default_cont_entry (NuValue self,
						     NuValue arg);

#define NU_CONT_PROCEDURE(n) \
  struct		     \
  {			     \
    uintptr_t header;	     \
    size_t num_vars;	     \
    NuContEntry1 *entry_1;   \
    NuContEntryX *entry_x;   \
    NuValue var [n];	     \
  }

struct nu_cont_procedure
{
  uintptr_t header;
  size_t num_vars;
  NuContEntry1 *entry_1;
  NuContEntryX *entry_x;
  NuValue var [];
};

struct nu_pair
{
  NuCell car;
  NuCell cdr;
};

#define NU_VECTOR(n)				\
  struct {					\
    uintptr_t header;				\
    NuCell [n];					\
  }

struct nu_vector
{
  uintptr_t header;
  NuCell element [];
};

struct nu_procedure
{
  union {
    NU_ALIGN NuLabel *label;
    NuValue var;
  };
};

struct nu_label
{
  size_t num_slots;
  size_t num_labels;
  size_t self_slot;
  NuEntry0 *entry_0;
  NuEntry1 *entry_1;
  NuEntry2 *entry_2;
  NuEntry3 *entry_3;
  NuEntryX *entry_x;
};

inline uintptr_t
nu_get_object_header (NuValue val)
{
  assert (nu_object_p (val));
  return *(uintptr_t *) (val - NU_TAG_OBJECT);
}

inline size_t
nu_get_object_header_payload (NuValue val)
{
  return nu_get_object_header (val) / (NU_OBJECT_MASK + 1);
}

inline bool
nu_fixnum_p (NuValue val)
{
  return (val & NU_POINTER_MASK) == 0;
}

inline bool
nu_pointer_p (NuValue val)
{
  return (val & NU_POINTER_MASK) == 1;
}

inline bool
nu_object_p (NuValue val)
{
  return (val & NU_TAG_MASK) == NU_TAG_OBJECT;
}

inline bool
nu_pair_p (NuValue val)
{
  return (val & NU_TAG_MASK) == NU_TAG_PAIR;
}

inline bool
nu_vector_p (NuValue val)
{
  return (nu_object_p (val)
	  && ((nu_get_object_header (val) & NU_OBJECT_MASK)
	      == NU_OBJECT_VECTOR));
}

inline bool
nu_procedure_p (NuValue val)
{
  return (val & NU_TAG_MASK) == NU_TAG_PROCEDURE;
}

inline bool
nu_boolean_p (NuValue val)
{
  return (val & NU_IMMEDIATE_MASK) == NU_IMMEDIATE_BOOLEAN;
}

inline bool
nu_cont_procedure_p (NuValue val)
{
  return (nu_object_p (val)
	  && ((nu_get_object_header (val) & NU_OBJECT_MASK)
	      == NU_OBJECT_CONTINUATION));
}

inline bool
nu_false_p (NuValue val)
{
  return val == NU_BOOLEAN_FALSE;
}

inline bool
nu_unspecified_p (NuValue val)
{
  return val == NU_IMMEDIATE_UNSPECIFIED;
}

inline NuFixnumInt
nu_value_to_int (NuValue val)
{
  assert (nu_fixnum_p (val));
  return val >> 1;
}

inline NuValue
nu_value_from_int (NuFixnumInt val)
{
  return (uintptr_t) val << 1;
}

inline NuPair *
nu_value_to_pair (NuValue val)
{
  assert (nu_pair_p (val));
  return (NuPair *) (val - NU_TAG_PAIR);
}

inline NuValue
nu_value_from_pair (const NuPair *pair)
{
  return (uintptr_t) pair + NU_TAG_PAIR;
}

inline NuVector *
nu_value_to_vector (NuValue val)
{
  assert (nu_vector_p (val));
  return (NuVector *) (val - NU_TAG_OBJECT);
}

inline NuValue
nu_value_from_vector (const NuVector *vec)
{
  return (uintptr_t) vec + NU_TAG_OBJECT;
}

inline bool
nu_value_to_bool (NuValue val)
{
  return !nu_false_p (val);
}

inline NuValue
nu_value_from_bool (bool flag)
{
  return flag ? NU_BOOLEAN_TRUE : NU_BOOLEAN_FALSE;
}

inline NuClosure
nu_value_to_closure (NuValue val)
{
  assert (nu_procedure_p (val));
  return (NuClosure) (val - NU_TAG_PROCEDURE);
}

inline NuValue
nu_value_from_closure (NuClosure clo)
{
  return (uintptr_t) clo + NU_TAG_PROCEDURE;
}

inline NuProcedure *
nu_value_to_procedure (NuValue val)
{
  assert (nu_procedure_p (val));
  return (NuProcedure *) (val - NU_TAG_PROCEDURE);
}

inline NuValue
nu_value_from_procedure (NuProcedure *proc)
{
  return (uintptr_t) proc + NU_TAG_PROCEDURE;
}

inline NuContProcedure *
nu_value_to_cont_procedure (NuValue val)
{
  assert (nu_cont_procedure_p (val));
  return (NuContProcedure *) (val - NU_TAG_OBJECT);
}

inline NuValue
nu_value_from_cont_procedure (NuContProcedure *proc)
{
  return (uintptr_t) proc + NU_TAG_OBJECT;
}

inline NuValue
nu_fx_add (NuValue op1, NuValue op2)
{
  assert (nu_fixnum_p (op1));
  assert (nu_fixnum_p (op2));
  return op1 + op2;
}

inline NuValue nu_fx_zero_p (NuValue op)
{
  assert (nu_fixnum_p (op));
  return nu_value_from_bool (op == 0);
}

inline NuValue
nu_car (NuValue val)
{
  assert (nu_pair_p (val));
  return nu_value_to_pair (val)->car;
}

inline NuValue
nu_cdr (NuValue val)
{
  assert (nu_pair_p (val));
  return nu_value_to_pair (val)->cdr;
}

inline void
nu_set_car_x (NuValue pair, NuValue obj)
{
  assert (nu_pair_p (pair));
  nu_value_to_pair (pair)->car = obj;
}

inline void
nu_set_cdr_x (NuValue pair, NuValue obj)
{
  assert (nu_pair_p (pair));
  nu_value_to_pair (pair)->cdr = obj;
}

inline NuValue
nu_vector_length (NuValue vec)
{
  assert (nu_vector_p (vec));
  return nu_value_from_int (nu_get_object_header_payload (vec));
}

inline NuValue
nu_vector_ref (NuValue vec, NuValue idx)
{
  return nu_value_to_vector (vec)->element [nu_value_to_int (idx)];
}

inline void
nu_vector_set_x (NuValue vec, NuValue idx, NuValue obj)
{
  nu_value_to_vector (vec)->element [nu_value_to_int (idx)] = obj;
}

inline NuValue
nu_get_procedure (NuClosure clo, size_t slot)
{
  return nu_value_from_procedure (clo + slot);
}

inline NuValue
nu_get_closure_variable (NuValue self, size_t self_slot,
			 size_t other_slot)
{
  return nu_value_to_closure (self) [other_slot - self_slot].var;
}

inline NuValue
nu_get_sibling_procedure (NuValue self, size_t self_slot, size_t other_slot)
{
  return nu_value_from_closure (nu_value_to_closure (self)
				+ other_slot - self_slot);
}

inline void
nu_call_0 (NuValue proc, NuValue cont, NuValue c1, NuValue c2, NuValue c3, NuValue c4)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_procedure (proc)->label->entry_0 (proc, cont);
  NU_UNREACHABLE;
}

inline void
nu_call_1 (NuValue proc, NuValue cont, NuValue c1, NuValue c2, NuValue c3,
	   NuValue c4, NuValue arg1)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;  
  nu_value_to_procedure (proc)->label->entry_1 (proc, cont, arg1);
  NU_UNREACHABLE;
}

inline void
nu_call_2 (NuValue proc, NuValue cont, NuValue c1, NuValue c2, NuValue c3,
	   NuValue c4, NuValue arg1, NuValue arg2)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_procedure (proc)->label->entry_2 (proc, cont, arg1, arg2);
  NU_UNREACHABLE;
}

inline void
nu_call_3 (NuValue proc, NuValue cont, NuValue c1, NuValue c2, NuValue c3,
	   NuValue c4, NuValue arg1, NuValue arg2, NuValue arg3)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_procedure (proc)->label->entry_3 (proc, cont, arg1, arg2, arg3);
  NU_UNREACHABLE;
}

inline void
nu_call_x (NuValue proc, NuValue cont, NuValue c1, NuValue c2, NuValue c3,
	   NuValue c4, NuValue arg1, NuValue arg2, NuValue arg3, NuValue args)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_procedure (proc)->label->entry_x (proc, cont, arg1, arg2, arg3,
						args);
  NU_UNREACHABLE;
}

inline NuValue nu_get_cont_closure_variable (NuValue cont, size_t slot)
{
  return nu_value_to_cont_procedure (cont)->var [slot];
}

inline void
nu_cont_call (NuValue proc, NuValue c1, NuValue c2, NuValue c3,
	      NuValue c4, NuValue arg)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_cont_procedure (proc)->entry_1 (proc, arg);
  NU_UNREACHABLE;
}

inline void
nu_cont_apply (NuValue proc, NuValue c1, NuValue c2, NuValue c3,
	       NuValue c4, NuValue args)
{
  NU_REG_1 = c1;
  NU_REG_2 = c2;
  NU_REG_3 = c3;
  NU_REG_4 = c4;
  nu_value_to_cont_procedure (proc)->entry_x (proc, args);
  NU_UNREACHABLE;
}

/* Signal */

inline bool
nu_thread_signal_p ()
{
  return NU_SIG != 0;
}

#endif /* LIBNUSCHEME_H_INCLUDED */
