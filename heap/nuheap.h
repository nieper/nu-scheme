/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NUHEAP_H_INCLUDED
#define NUHEAP_H_INCLUDED

#if BUILDING_LIBNUHEAP && HAVE_VISIBILITY
# define LIBNUHEAP_DLL_EXPORTED __attribute__((__visibility("default")))
#else
# define LIBNUHEAP_DLL_EXPORTED
#endif

#include <stddef.h>

#include "nuvalue.h"

#define NU_DEFAULT_HEAP_SIZE (1ULL << 20)

typedef struct nu_heap NuHeap;
struct nu_heap
{
  size_t heap_size;
  NuValue *start, *free;
};

NuHeap *nu_heap_create (size_t);
void nu_heap_free (NuHeap *);

#endif /* NUHEAP_H_INCLUDED */
