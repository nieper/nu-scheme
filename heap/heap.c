/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>

#include "nucommon.h"
#include "nuheap.h"
#include "xalloc.h"

static NuValue *flip (NuHeap *);

LIBNUHEAP_DLL_EXPORTED NuHeap *
nu_heap_create (size_t heap_size)
{
  NuHeap *heap = XMALLOC (NuHeap);
  heap->heap_size = heap_size;
  flip (heap);

  return heap;
}

LIBNUHEAP_DLL_EXPORTED void
nu_heap_free (NuHeap *heap)
{
  free (heap->start);
  free (heap);
}

NuValue *
flip (NuHeap *heap)
{
  NuValue *old_start = heap->start;
  heap->free = heap->start = xaligned_alloc (NU_ALIGNMENT, heap->heap_size);
  return old_start;
}
