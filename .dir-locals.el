;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

((c-mode
  (eval progn
	(font-lock-add-keywords
	 nil
	 '(("\\<\\(NU_INLINE\\)\\>" . font-lock-keyword-face)))))
 (scheme-mode
  (eval progn
	(put 'bind 'scheme-indent-function 1)
	(put 'with-syntax 'scheme-indent-function 1)
	(put 'receive 'scheme-indent-function 2)
	(put 'emit-with-output-file 'scheme-indent-function 1)
	(put 'match 'scheme-indent-function 1)
	(put 'call-with-output 'scheme-indent-function 1)
	(font-lock-add-keywords
	 nil
	 '(("(\\(with-syntax\\)\\>" 1 font-lock-keyword-face)
	   ("(\\(receive\\)\\>" 1 font-lock-keyword-face)
	   ("(\\(emit-with-output-file\\)\\>" 1 font-lock-keyword-face)
	   ("(\\(match\\))\\>" 1 font-lock-keyword-face))))))
