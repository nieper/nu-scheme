/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NUGC_H_INCLUDED
#define NUGC_H_INCLUDED

#include <libnuscheme.h>
#include <stddef.h>

/* Interface */

#define NU_ALLOCATE(T) (nu_allocate (sizeof (T)))
#define NU_ALLOCATE_TEMP(T) (nu_allocate_temp (sizeof (T)))

NuObject *nu_allocate (size_t);
unsigned char *nu_allocate_temp (size_t);
NuObject *nu_object_forward (NuObject *);
void nu_object_update (NuObject *);
void nu_forward_init (NuForward *, NuObject *);

#endif /* NUGC_H_INCLUDED */
