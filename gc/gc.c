/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <stdalign.h>

#include "nugc.h"

static unsigned char *nu_heap_start;
static unsigned char *nu_heap_end;
static unsigned char *nu_heap_free;
static unsigned char *nu_heap_free_end;

static NuObject *forward_forward (NuObject *);

NuObject *
nu_allocate (size_t n)
{
  NuObject *obj = (NuObject *) nu_heap_free;
  nu_heap_free += (n + (alignof (NuObject) - 1)) & ~(alignof (NuObject) - 1);
  return obj;
}

unsigned char *
nu_allocate_temp (size_t n)
{
  nu_heap_free_end
    -= (n + (alignof (NuObject) - 1)) & ~(alignof (NuObject) - 1);
  return nu_heap_free_end;
}

NuObject *
nu_object_forward (NuObject *obj)
{
  unsigned char *p = (unsigned char *) obj;
  if (nu_heap_start <= p && p < nu_heap_end)
    return obj;
  return obj->tag->forward (obj);
}

void
nu_object_update (NuObject *obj)
{
  return obj->tag->update (obj);
}

void
nu_forward_init (NuForward *fwd, NuObject *obj)
{
  static NuTag TAG_FORWARD = { forward_forward, NULL };
  fwd->tag = &TAG_FORWARD;
  fwd->obj = obj;
}

NuObject *
forward_forward (NuObject *obj)
{
  return ((NuForward *) obj)->obj;
}
