/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef OBJECT_STACK_H_INCLUDED
#define OBJECT_STACK_H_INCLUDED

#include "obstack.h"
#include "value.h"
#include "xalloc.h"

typedef struct nu_object_stack NuObjectStack;
struct nu_object_stack
{
  struct obstack obstack;
  void *base;
};

inline void nu_object_stack_init (NuObjectStack *);
inline void nu_object_stack_clear (NuObjectStack *);
inline void nu_object_stack_destroy (NuObjectStack *);
inline void nu_object_stack_grow0 (NuObjectStack *);
inline void nu_object_stack_align (NuObjectStack *);
inline NuValue nu_object_stack_finish (NuObjectStack *);

#define obstack_chunk_alloc xmalloc
#define obstack_chunk_free  free

inline void
nu_object_stack_init (NuObjectStack *stack)
{
  obstack_init (&stack->obstack);
  obstack_alignment_mask (&stack->obstack) = NU_TAG_MASK;
  stack->base = obstack_finish (&stack->obstack);
}

inline void
nu_object_stack_clear (NuObjectStack *stack)
{
  obstack_free (&stack->obstack, stack->base);
}

inline void
nu_object_stack_destroy (NuObjectStack *stack)
{
  obstack_free (&stack->obstack, NULL);
}

inline void
nu_object_stack_grow (NuObjectStack *stack, NuValue val)
{
  obstack_grow (&stack->obstack, &val, sizeof (val));
}

inline void
nu_object_stack_grow0 (NuObjectStack *stack)
{
  obstack_1grow (&stack->obstack, 0);
}

inline void
nu_object_stack_align (NuObjectStack *stack)
{
  size_t size = obstack_object_size (&stack->obstack);
  obstack_blank (&stack->obstack,
		 ((size + NU_TAG_MASK) & ~NU_TAG_MASK) - size);
}

inline NuValue
nu_object_stack_finish (NuObjectStack *stack)
{
  return (NuValue) obstack_finish (&stack->obstack);
}

#undef obstack_chunk_alloc
#undef obstack_chunk_free

#endif /* OBJECT_STACK_H_INCLUDED */
