/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>

#include "nuvalue.h"
#include "xalloc.h"

extern LIBNUVALUE_DLL_EXPORTED inline void nu_object_stack_init (NuObjectStack *);
extern LIBNUVALUE_DLL_EXPORTED inline void nu_object_stack_clear (NuObjectStack *);
extern LIBNUVALUE_DLL_EXPORTED inline void nu_object_stack_destroy (NuObjectStack *);
extern LIBNUVALUE_DLL_EXPORTED inline void nu_object_stack_grow0 (NuObjectStack *);
extern LIBNUVALUE_DLL_EXPORTED inline void nu_object_stack_align (NuObjectStack *);
extern LIBNUVALUE_DLL_EXPORTED inline NuValue nu_object_stack_finish (NuObjectStack *);
