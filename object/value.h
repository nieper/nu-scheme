/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VALUE_H_INCLUDED
#define VALUE_H_INCLUDED

#include <lightning.h>
#include <stdbool.h>

#define NU_WORDSIZE  (sizeof (NuValue))
#define NU_ALIGNMENT (2 * NU_WORDSIZE)
#define NU_TAG_MASK  (NU_ALIGNMENT - 1)

#define NU_TAG_PAIR      0
#define NU_TAG_IMMEDIATE 1
#define NU_TAG_HEADER    2
#define NU_TAG_LINK      3
#define NU_TAG_POINTER   NU_WORDSIZE
#define NU_TAG_MARKED    5
#define NU_TAG_FLONUM    6

#define NU_IMMEDIATE_PAYLOAD_SHIFT 8
#define NU_IMMEDIATE_TYPE_MASK (16 * NU_ALIGNMENT - 1)

#define NU_MAKE_IMMEDIATE_TYPE(tag)		\
  ((tag) * NU_ALIGNMENT | NU_TAG_IMMEDIATE)

#define NU_BOOLEAN_TYPE    NU_MAKE_IMMEDIATE_TYPE (0)
#define NU_CHAR_TYPE       NU_MAKE_IMMEDIATE_TYPE (1)
#define NU_NULL_TYPE       NU_MAKE_IMMEDIATE_TYPE (2)
#define NU_EOF_OBJECT_TYPE NU_MAKE_IMMEDIATE_TYPE (3)
#define NU_VOID_TYPE       NU_MAKE_IMMEDIATE_TYPE (4)
#define NU_FIXNUM_TYPE     NU_MAKE_IMMEDIATE_TYPE (7)

#define NU_MAKE_IMMEDIATE_VALUE(type, payload)		\
  ((type) + (payload) << NU_IMMEDIATE_PAYLOAD_SHIFT)

#define NU_NULL       NU_MAKE_IMMEDIATE_VALUE (NU_NULL_TYPE, 0)
#define NU_FALSE      NU_MAKE_IMMEDIATE_VALUE (NU_BOOLEAN_TYPE, 0)
#define NU_TRUE       NU_MAKE_IMMEDIATE_VALUE (NU_BOOLEAN_TYPE, 1)
#define NU_EOF_OBJECT NU_MAKE_IMMEDIATE_VALUE (NU_EOF_OBJECT_TYPE, 0)
#define NU_VOID       NU_MAKE_IMMEDIATE_VALUE (NU_VOID_TYPE, 0)

typedef jit_uword_t NuValue;

inline unsigned nu_tag (NuValue);
inline unsigned nu_immediate_type (NuValue);
inline bool nu_immediatep (NuValue);
inline bool nu_pointerp (NuValue);
inline NuValue nu_null (void);
inline bool nu_nullp (NuValue);
inline NuValue nu_eof_object (void);
inline bool nu_eof_object_p (NuValue);
inline NuValue nu_void (void);
inline bool nu_void_p (NuValue);
inline NuValue nu_boolean (bool);
inline bool nu_boolean_p (NuValue);
inline bool nu_false_p (NuValue);
inline bool nu_true_p (NuValue);
inline bool nu_pairp (NuValue);

inline unsigned
nu_tag (NuValue val)
{
  return val & NU_TAG_MASK;
}

inline unsigned
nu_immediate_type (NuValue val)
{
  return val & NU_IMMEDIATE_TYPE_MASK;
}

inline bool
nu_immediatep (NuValue val)
{
  return nu_tag (val) == NU_TAG_IMMEDIATE;
}

inline bool
nu_pointerp (NuValue val)
{
  return !nu_immediatep (val);
}

inline NuValue
nu_null (void)
{
  return NU_NULL;
}

inline bool
nu_nullp (NuValue val)
{
  return val == NU_NULL;
}

inline NuValue
nu_eof_object (void)
{
  return NU_EOF_OBJECT;
}

inline bool
nu_eof_object_p (NuValue val)
{
  return val == NU_EOF_OBJECT;
}

inline NuValue
nu_void (void)
{
  return NU_VOID;
}

inline bool
nu_void_p (NuValue val)
{
  return val == NU_VOID;
}

inline NuValue
nu_boolean (bool flag)
{
  return flag ? NU_TRUE : NU_FALSE;
}

inline bool
nu_boolean_p (NuValue val)
{
  return nu_immediate_type (val) == NU_BOOLEAN_TYPE;
}

inline bool
nu_false_p (NuValue val)
{
  return val == NU_FALSE;
}

inline bool
nu_true_p (NuValue val)
{
  return val == NU_TRUE;
}

inline bool
nu_pairp (NuValue val)
{
  return nu_tag (val) == NU_TAG_PAIR;
}

#endif /* VALUE_H_INCLUDED */
