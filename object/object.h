/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED

#include "object_stack.h"
#include "value.h"

inline NuValue nu_cons (NuObjectStack, NuValue, NuValue);
inline NuValue nu_car (NuValue);
inline NuValue nu_cdr (NuValue);

inline NuValue
nu_cons (NuObjectStack *stack, NuValue car, NuValue cdr)
{
  nu_object_stack_grow (stack, car);
  nu_object_stack_grow (stack, cdr);
  return object_stack_finish (stack) | NU_TAG_PAIR;
}

inline NuValue
nu_car (NuValue pair)
{
  return ((NuValue *) pair)[0];
}

inline NuValue
nu_cdr (NuValue pair)
{
  return ((NuValue *) pair)[1];
}

#endif /* OBJECT_H_INCLUDED */
