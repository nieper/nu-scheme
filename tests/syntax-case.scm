;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu syntax-object)
             (nu syntax-case))

;;; XXX
(use-modules (ice-9 pretty-print)
             (language tree-il))

;;; XXX
(define (l x)
  (newline)
  (pretty-print (tree-il->scheme x))
  (newline)
  x)

;;; XXX
(l
 (macroexpand
  '(syntax-case p ()
     ((a ...) #'(a ...)))))

(test-begin "Syntax Case")

(test-group "Syntax"

  (test-equal "Constant"
    '1
    (syntax->datum #'1))

  (test-equal "Empty list"
    '()
    (syntax->datum #'()))

  (test-equal "Symbol"
    'a
    (syntax->datum #'a))

  (test-equal "Escaped ellipsis"
    '...
    (syntax->datum #'(... ...))))

(test-group "Quasisyntax"

  (test-equal "Constant"
    '1
    (syntax->datum #`1))

  (test-equal "Quasisyntax level"
    '#`#,1
    (syntax->datum #`#`#,1))

  (test-equal "Unsyntax"
    '1
    (syntax->datum #`#,#'1)))


(test-group "Syntax Case"

  (test-equal "Empty list"
    3
    (syntax-case (syntax ()) ()
      (() #f 1)
      (2 #t #'a)
      (() 3)))

  (test-error "No match"
    #t
    (syntax-case (syntax #f) ()
      (#t #t)))

  (test-equal "Underscore"
    2
    (syntax-case #'a ()
      (() 1)
      (_ 2)))

  (test-equal "Pair pattern"
    '2
    (syntax-case #'(1 . 2) ()
      ((1 . 3) 1)
      ((1 . a) (syntax->datum #'a))))

  (test-equal "Pattern variable"
    '1
    (syntax-case #'1 ()
      (2 1)
      (a (syntax->datum #'a))))

  (test-equal "Ellipsis"
    '(1 2 3)
    (syntax-case #'(1 2 3) ()
      ((a ...) (syntax->datum #'(a ...)))))

  (test-equal "Ellipsis with constant"
    '((1 x) (2 x))
    (syntax-case #'(1 2 x) ()
      ((a ... b) (syntax->datum #'((a b) ...)))))

  (test-equal "Nested ellipsis"
    '(((1 x) (2 y)) ((3 x) (4 y)))
    (syntax-case #'((1 2 x) (3 4 y)) ()
      (((a ... b) ...)
       (syntax->datum #'(((a b) ...) ...)))))

  (test-equal "Tail pattern"
    '((1 2) 3 4)
    (syntax-case #'(1 2 3 . 4) ()
      ((a ... b . c) (syntax->datum #'((a ...) b c))))))

(test-end)
