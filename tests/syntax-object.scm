;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu syntax-object))

(test-begin "Syntax Objects")

(test-group "Source Locations"

  (define srcloc1
    (make-source-location "source" 1 1 2 3))

  (define srcloc2
    (make-source-location "source" 1 1 1 3))

  (test-assert "source-location?: true"
    (source-location? srcloc1))

  (test-assert "source-location?: false"
    (not (source-location? #(1 1 2 3))))

  (test-equal "source-location->string: two lines"
    "source: 1.2-2.4"
    (source-location->string srcloc1))

  (test-equal "source-location->string: one line"
    "source: 1.2-4"
    (source-location->string srcloc2))

  (test-equal "source-location->prefix: empty string"
    ""
    (source-location->prefix #f))

  (test-equal "source-location->prefix: non-empty string"
    "source: 1.2-4: "
    (source-location->prefix srcloc2)))

(test-group "Syntax object"

  (define stx1
    (datum->syntax #f 'datum1))

  (define stx2
    (datum->syntax stx1 'datum2))

  (define stx3
    (datum->syntax #f
                   'datum3
                   (make-source-location "source" 1 1 2 2)))

  (define stx4
    (datum->syntax stx3 'datum4))

  (test-assert "datum->syntax: no context"
    (syntax? stx1))

  (test-assert "datum->syntax: no source location"
    (not (syntax-object-source-location stx2)))

  (test-assert "datum->syntax: source location"
    (syntax-object-source-location stx4))

  (test-equal "syntax->datum: symbol"
    'datum1
    (syntax->datum stx1))

  (test-equal "syntax->datum: pair"
    '(datum1 . #f)
    (syntax->datum (datum->syntax #f (cons stx1 #f))))

  (test-equal "syntax->datum: vector"
    #(datum1 datum2)
    (syntax->datum (datum->syntax #f (vector stx1 stx2)))))

(test-group "Unwrapping syntax"

  (define stx1
    (datum->syntax #f 'datum1))

  (define stx2
    (datum->syntax #f '(1 2 3)))

  (define stx3
    (datum->syntax #f '(x . y)))

  (test-assert "unwrap-syntax: identifier"
    (syntax? (unwrap-syntax stx1)))

  (test-assert "unwrap-syntax: pair"
    (pair? (unwrap-syntax stx2)))

  (test-assert "syntax-pair?: true"
    (syntax-pair? stx3))

  (test-assert "syntax-pair?: false"
    (not (syntax-pair? stx1)))

  (test-equal "syntax->list"
    '(1 2 3)
    (map syntax->datum
         (syntax->list stx2))))

(test-end)


  ;; (define (stx e) (make-syntax-object e #f #f))

  ;; (define (flatten f)
  ;;   (cond
  ;;    ((null? f)
  ;;     '())
  ;;    ((pair? f)
  ;;     (cons (car f) (flatten (cdr f))))
  ;;    (else
  ;;     (list f))))

  ;; (test-begin "Syntax")

  ;; (test-begin "syntax->list")

  ;; (test-assert
  ;;  (every syntax-object?
  ;;         (flatten (syntax->list (stx `(,(stx 1) . ,(stx 2)))))))

  ;; (test-assert
  ;;  (list? (syntax->list (stx `(,(stx 1) . ,(stx '()))))))

  ;; (test-assert
  ;;  (list? (syntax->list (stx `(,(stx 1) . ,'())))))

  ;; (test-end)

  ;; (define (lit? x) #f)
  ;; (define (ell? id)
  ;;   (eq? id '...))


  ;; (define s (stx 'e))
  ;; (define t (stx 'f))

  ;; (test-equal "syntax-match"
  ;;   '(1 (2 3))
  ;;   (syntax-match (datum->syntax #f '(1 (2 3))) '()
  ;;     `((a b) ,(lambda (a b)
  ;;                (syntax->datum (list a b))))))

  ;; ;; TODO: Write more tests.

  ;; (test-end)
