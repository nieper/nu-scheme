;; TODO!
(define (f clo c c1 c2 c3 c4 n a)
  (receive (z) (fxzero? n)
    (if z
	(continuation-call c c1 c2 c3 c4 a)
	(receive (d) (fx+ n mone)
	  (receive (k) (make-continuation s #f c n)
	    (call clo k c1 c2 c3 c4 d a))))))

(define (s c c1 c2 c3 c4 a)
  (receive (n) (continuation-variable c 1)
    (receive (sum) (fx+ a n)
      (receive (k) (continuation-variable c 0)
	(continuation-call k c1 c2 c3 c4 sum)))))

(define (h c c1 c2 c3 c4 a)
  (thread-exit))

(define (nu_run c1 c2 c3 c4) ;; rename to main?
  (receive (f) (make-procedure 1 0 l)
    (receive (k) (make-continuation h #f unspecified)
      (call f k c1 c2 c3 c4 ten zero))))

(define l (make-label 1 1 0 #f #f f #f #f))

(define x '129)

(define mone '-1)
(define zero '0)
(define ten '10)
