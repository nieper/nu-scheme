;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu environment-monad))

(test-begin "Environment Monad")

(test-assert (not (eqv? (make-environment-variable)
			(make-environment-variable))))

(test-assert (make-environment))

(test-eqv #f
  (let ((x (make-environment-variable)))
    (environment-ref (make-environment) x)))

(test-eqv 42
  (let ((x (make-environment-variable)))
    (environment-ref (environment-update (make-environment) x 42)
		     x)))

(test-eqv #f
  (let ((x (make-environment-variable))
	(y (make-environment-variable)))
    (environment-ref (environment-update (make-environment) x 42)
		     y)))

(test-eqv #f
  (let ((x (make-environment-variable))
	(env (make-environment)))
    (environment-update env x 42)
    (environment-ref env x)))

(test-eqv 42
  (let ((x (make-environment-variable))
	(env (make-environment)))
    (environment-update! env x 42)
    (environment-ref env x)))

(test-eqv 42
  (let ((x (make-environment-variable))
	(env (make-environment)))
    (environment-update! env x 42)
    (environment-update env x 10)
    (environment-ref env x)))

(test-eqv #f
  (let ((x (make-environment-variable))
	(env (make-environment)))
    (environment-update! (environment-update env x 10) x 42)
    (environment-ref env x)))

(test-eqv 42
  (let ((x (make-environment-variable))
	(y (make-environment-variable))
	(env (make-environment)))
    (environment-update! (environment-update env y 10) x 42)
    (environment-ref env x)))

(test-eqv 42
  (let* ((x (make-environment-variable))
	 (env (environment-update (make-environment) x 42))
	 (copy (environment-copy env)))
    (environment-update! env x 10)
    (environment-ref copy x)))

(test-eqv #f
  (let ((flag #f))
    (make-computation
     (lambda (compute)
       (set! flag #t)))
    flag))

(test-eqv 42
  (run (make-computation
	(lambda (compute)
	  42))))

(test-eqv 42
  (run (make-computation
	(lambda (compute)
	  (compute (return 42))))))

(test-equal '(10 42)
  (call-with-values
      (lambda () (run (make-computation
		       (lambda (compute)
			 (compute (return 10 42))))))
    list))

(test-equal '(42 (b a))
  (let* ((acc '())
	 (result
	  (run (sequence (make-computation
			  (lambda (compute)
			    (set! acc (cons 'a acc))))
			 (make-computation
			  (lambda (compute)
			    (set! acc (cons 'b acc))
			    42))))))
    (list result acc)))

(test-equal 83
  (run (bind (return 42)
	     (lambda (x)
	       (return (* x 2)))
	     (lambda (x)
	       (return (- x 1))))))

(test-equal '(42 #f)
  (let ((x (make-environment-variable)))
    (run (make-computation
	  (lambda (compute)
	    (let ((a (compute
		      (local (lambda (env)
			       (environment-update env x 42))
			(bind (ask)
			      (lambda (env)
				(return (environment-ref env x))))))))
	      (list a (environment-ref (compute (ask)) x))))))))

(test-eqv 42
  (let ((x (make-environment-variable)))
    (run (with ((,x 42))
	   (fn ((y ,x))
	     (return y))))))

(test-eqv #f
  (let ((x (make-environment-variable)))
    (run (sequence (with ((,x 42))
		     (fn ((y ,x))
		       (return y)))
		   (fn ((y ,x))
		     (return y))))))

(test-eqv 42
  (let ((x (make-environment-variable)))
    (run (sequence (with! (,x 42))
		   (fn ((y ,x))
		     (return y))))))

(test-eqv #f
  (let ((x (make-environment-variable)))
    (run (forked (with! (,x 42))
		 (fn ((y ,x))
		   (return y))))))

(test-eqv 42
  (run (with ((,default-computation return))
	 42)))

(test-eqv 42
  (run (with ((x 10))
	 (with ((x 42))
	   (fn ((x x))
	     (return x))))))

(test-end)
