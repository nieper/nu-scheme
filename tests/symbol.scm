;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu symbol))

(test-begin "Symbols")

(test-equal "symbol?"
  '(#t #t #f)
  (map symbol? (list 'a (gensym) "a")))

(test-equal "symbol-interned?"
  '(#t #f)
  (map symbol-interned? (list 'a (gensym))))

(test-equal "string->symbol"
  '("Aa" #t #t)
  (list (symbol->string (string->symbol "Aa"))
        (symbol? (string->symbol "a"))
        (symbol-interned? (string->symbol "A"))))

(test-equal "string->uninterned-symbol"
  '("Aa" #t #f)
  (list (symbol->string (string->uninterned-symbol "Aa"))
        (symbol? (string->uninterned-symbol "a"))
        (symbol-interned? (string->uninterned-symbol "A"))))

(test-equal "eq?"
  '(#t #f)
  (list (eq? (string->symbol "a") (string->symbol "a"))
        (eq? (string->uninterned-symbol "a") (string->uninterned-symbol "a"))))

(test-equal "gensym"
  '#f
  (eq? (gensym) (gensym)))

(test-equal "symbol=?"
  '(#t #t #f #f)
  (list (symbol=? 'a)
        (symbol=? 'a 'a)
        (symbol=? 'a 'a 'b)
        (symbol=? 'a 'b 'a)))

(test-end)
