;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu expression))

(test-begin "Expressions")

(test-group "Constructor"

  (define var (make-variable 'x))

  (test-equal "Self-evaluating"
    10
    (data-datum `10))

  (test-equal "String"
    "foo"
    (data-datum `"foo"))

  (test-equal "Quoted"
    'a
    (data-datum `'a))

  (test-equal "Unquoted"
    '(1 5)
    (data-datum ``(1 ,(+ 2 3))))

  (test-equal "Unquote"
    42
    `,42)

  (test-equal "Variable reference"
    var
    (lexical-reference-variable `,var))

  (test-equal "Conditional"
    3
    (data-datum (conditional-alternate `(if 1 2 3))))

  (test-assert "Conditional without alternate clause"
               (unspecified? (conditional-alternate `(if #f #f))))

  (test-equal "Application"
    '(2)
    (map data-datum (application-operands `(1 2))))

  (test-equal "Application with many operands"
    '(1 2 3)
    (map data-datum (application-operands
		     `(1 ,@(list `1 `2 `3)))))

  (test-equal "Annotation"
    '(42 foo)
    (let ((e `(annotation ,42 'foo)))
      (list (annotation-source-location e)
	    (data-datum (annotation-expression e)))))

  (test-equal "Function"
    #t
    (let ((f `(lambda (,var) 42)))
      (function? f)))

  (test-assert "Letrec"
    (letrec? `(letrec ((,var 1)) 2)))

  (test-assert "Primitive"
    (primitive-reference? `display)))

(test-group "Matcher"

  (define var (make-variable 'x))

  (test-equal "Datum"
    #t
    (match `10
      (0 #f)
      (10 #t)))

  (test-equal "Quote"
    #t
    (match `'foo
      (0 #f)
      ('foo #t)
      (,_ #f)))

  (test-equal "Unquote"
    'foo
    (match `'foo
      ('bar #f)
      (,foo (data-datum foo))
      (,_ #f)))

  (test-equal "Datum: Match"
    'foo
    (match `'foo
      (`,x (data-datum x))))

  (test-equal "Datum: No match"
    #f
    (match var
      (`,x (data-datum x))
      (,_ #f)))

  (test-equal "If"
    (list 1 2 3)
    (match `(if 1 2 3)
      ((if ,x ,y)
       #f)
      ((if ,x ,y ,z)
       (list (data-datum x) (data-datum y) (data-datum z)))
      (,_
       #f)))

  (test-equal "Function"
    (list var 42)
    (match `(lambda (,var) 42)
      ((lambda (,x) ,(data-datum -> y))
       (list x y)))))

(test-group "Expression to datum transformer"

  (define var 'x)
  (define var2 'y)

  (test-equal "String"
    "foo"
    (expression->datum `"foo"))

  (test-equal "Application"
    '(1 2 3)
    (expression->datum `(1 2 3)))

  (test-equal "Function"
    '(lambda (x) 1)
    (expression->datum `(lambda (,var) 1)))

  (test-equal "Let"
    '((lambda (x) x) 1)
    (expression->datum `(let ((,var 1)) ,var)))

  (test-equal "Let with ellipsis"
    '((lambda (x y) x) 1 2)
    (expression->datum `(let ((,var 1) (,(list var2) ,(list `2)) ...) ,var)))

  (test-equal "Letrec"
    '(letrec ((x 1)) x)
    (expression->datum `(letrec ((,var 1)) ,var))))

;; TODO: Write many more tests.

(test-end)
