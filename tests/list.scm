;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu list))

(test-begin "Lists")

(test-equal "Empty list"
  0
  (length '()))

(test-equal "Dotted list of zero length"
  0
  (length #t))

(test-equal "Proper list"
  3
  (length '(1 2 3)))

(test-equal "Dotted list"
  3
  (length '(1 2 3 . 4)))

(test-assert "length+: circular list"
  (not (length+ (circular-list 1 2))))

(test-equal "length+: proper list"
  3
  (length+ '(1 2 3)))

(test-equal "length+: dotted list"
  3
  (length+ '(1 2 3 . 4)))

(test-equal "map: dotted list"
  '(1 2 3 . 4)
  (map (lambda (x) (+ x 1)) '(0 1 2 . 3)))

(test-equal "for-each: dotted list"
  10
  (let ((x 0))
    (for-each (lambda (y) (set! x (+ x y))) '(1 2 3 . 4))
    x))

(test-equal "for-each: more than one list"
  '((3 . c) (2 . b) (1 . a))
  (let ((x '()))
    (for-each (lambda (y z)
		(set! x (cons (cons y z) x)))
	      '(1 2 3 4)
	      '(a b . c))
    x))

(test-end)
