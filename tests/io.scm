;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu io))

(define (test-read name expected representations)
  (let ((actual (map (lambda (repr)
                       (let* ((port (open-input-string repr))
                              (datum (read port)))
                         (if (eof-object? datum) 'eof datum)))
                     representations)))
    (test-equal name expected actual)))

(test-begin "Input/Output")

(test-read "Numbers"
           '(1 1/2 -5)
           '("1" "   1/2" "-5"))

(test-read "Eof"
           '(eof eof not-eof)
           `(" "
             " ; comment"
             ,(string-append "; comment" (string #\newline) " not-eof ")))

(test-read "List"
           '((a b c))
           '("(a b c)"))

(test-read "Two lists"
           '(((a) (b)))
           '("((a) (b))"))

(test-read "Dotted list"
           '((a b . c))
           '("(a b . c)"))

(test-read "Vector"
           '(#(a b "hi"))
           '("#(a b \"hi\")"))

(test-read "Two vectors"
           '((#(a) #(b)))
           '("(#(a)#(b))"))

(test-read "Quote"
           '('x)
           '("'x"))

;; TODO: Write more tests.

(test-end)
