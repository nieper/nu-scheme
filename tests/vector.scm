;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu vector))

(test-begin "Vectors")

(test-equal "vector-map"
  '#(1 4 9)
  (vector-map (lambda (x) (* x x)) '#(1 2 3)))

(test-equal "vector-for-each"
  '((2 . b) (1 . a))
  (let ((out '()))
    (vector-for-each
     (lambda (a b)
       (set! out (cons (cons a b) out)))
     '#(1 2 3) '#(a b))
    out))

;; TODO: Write more tests.

(test-end)
