;; Every function has the following parameters:
;; sig SELF CONT ARG1 ARG2 ARG3 ARG*

(define (f clo c c1 c2 c3 c4 n a)
  (receive (z) (fxzero? n)
    (if z
	(continuation-call c c1 c2 c3 c4 a)
	(receive (d) (fx+ n mone)
	  (receive (s) (fx+ a n)
	    (call clo c c1 c2 c3 c4 d s))))))

(define (h c c1 c2 c3 c4 a)
  (thread-exit))

(define (nu_run c1 c2 c3 c4) ;; rename to main?
  (receive (f) (make-procedure 1 0 l)
    (receive (k) (make-continuation h #f unspecified)
      (call f k c1 c2 c3 c4 ten zero))))

(define l (make-label 1 1 0 #f #f f #f #f))

(define mone '-1)
(define zero '0)
(define ten '10)
