;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu match)
	     (nu list))

(test-begin "Match")

(test-assert "Constant"
  (match 1
    (0 #f)
    (1 #t)
    (,_ #f)))

(test-assert "Pair"
  (match '(1 . 2)
    (0 #f)
    ((1 . 3) #f)
    ((1 . 2) #t)
    (,_ #f)))

(test-equal "Variables"
  '(1 . 2)
  (match '(1 . 2)
    ((,x . ,y)
     (cons x y))
    (,_ #f)))


(test-equal "Catamorphism"
  2
  (match '(1 . 2)
    ((1 . ,(x)) x)
    (2 2)
    (,_ #f)))

(test-equal "Multiple values"
  '(1 . 2)
  (match '(1 . 2)
    ((1 . ,(x y)) (cons x y)) 
    (2 (values 1 2))
    (,_ #f)))

(test-equal "Specified catamorphism"
  #(1 2)
  (match '(1 . 2)
    (,(car+cdr -> x y) (vector x y))
    (,_ #f)))

(test-equal "Guard expression"
  'symbol
  (match 'foo
    (,x (guard (integer? x)) 'integer)
    (,x (guard (symbol? x)) 'symbol)
    (,_ #f)))

(test-assert "Ellipsis"
  (match '(1 1 2)
    ((1 2 ...) #f)
    ((1 ... 2) #t)
    (,_ #f)))

(test-equal "Ellipsis with variables"
  '((1 2) 3 ())
  (match '(1 2 3)
    ((,x ... ,y . ,z) (list x y z))
    (,_ #f)))

(test-equal "Nested ellipsis"
  '((1 2) (3 4) (5 6))
  (match '((1 2) (3 4) (5 6))
    (((,x ...) ...) x)
    (,_ #f)))

(test-equal "Ellipsis with catamorphisms"
  '(((1) (3)) ((2) (4)))
  (match '((1 2) (3 4))
    (((,(list -> x) ,(list -> y)) ...)
     (list x y))
    (,_
     #f)))

(test-equal "Vector pattern"
  '((1 2) 3)
  (match '#(1 2 3)
    (#(,a ... ,b)
     (list a b))))

;; TODO: Vector tests.

(test-end)
