;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nu expand))

(test-begin "Expander")

;; TODO: Write tests.

(test-end "Expander")

;; ;; FIXME: The following tests depend on the host system having the
;; ;; same preferred symbol case than Nu Scheme (lowercase).

;; (define (expression=? expr1 expr2)
;;   (let ((real-names (make-hash-table (make-comparator symbol?
;;                                                       eq?
;;                                                       #f
;;                                                       symbol-hash)))
;;         (dummy-names (make-hash-table (make-comparator symbol?
;;                                                        eq?
;;                                                        #f
;;                                                        symbol-hash))))
;;     (let loop ((expr1 expr1) (expr2 expr2))
;;       (cond
;;        ((pair? expr1)
;;         (and (pair? expr2)
;;              (if (eq? (car expr1) 'quote)
;;                  (and (eq? (car expr2) 'quote)
;;                       (equal? (cadr expr1) (cadr expr2)))
;;                  (and (loop (car expr1) (car expr2))
;;                       (loop (cdr expr1) (cdr expr2))))))
;;        ((null? expr1)
;;         (null? expr2))
;;        ((symbol? expr1)
;;         (and (symbol? expr2)
;;              (and
;;               (eq? (hash-table-intern! real-names expr1 (lambda () expr2))
;;                    expr2)
;;               (eq? (hash-table-intern! dummy-names expr2 (lambda () expr1))
;;                    expr1))))
;;        (else
;;         #f)))))

;; ;;; XXX: Just for debugging purposes.
;; (define (log x)
;;   (display "LOG: ")
;;   (display x)
;;   (newline)
;;   x)

;; (define (test-expand name expansion source)
;;   (test-assert name
;;     (expression=? expansion
;;                   (log (expression->datum
;;                         (expand-global
;;                          (datum->syntax #f source)))))))

;; (test-begin "Expander")

;; (test-expand "Quoting of constants"
;;   '('3)
;;   '(3))

;; (test-expand "Quote"
;;   ''a
;;   ''a)

;; (test-expand "Functions"
;;   '(lambda (a b)
;;      a)
;;   '(lambda (a b)
;;      a))

;; (test-expand "Primitive references"
;;   '(lambda (%car a)
;;      (car a))
;;   '(lambda (car a)
;;      ($car a)))

;; (test-expand "Letrec without bindings"
;;   ''a
;;   '(letrec ()
;;      'a))

;; (test-expand "Letrec with bindings"
;;   '(letrec ((a '1))
;;      a)
;;   '(letrec ((a 1))
;;      a))


;; (test-expand "Internal definitions"
;;   '(lambda (y)
;;      (letrec ((x y))
;;        x))
;;   '(lambda (y)
;;      (define x y)
;;      x))

;; (test-expand "Internal function definition"
;;   '(lambda ()
;;      (letrec* ((x (lambda () '1)))
;;        x))
;;   '(lambda ()
;;      (define (x) 1)
;;      x))

;; (test-expand "Internal begin"
;;   '(lambda (y)
;;      (letrec ((x y))
;;        x))
;;   '(lambda (y)
;;      (begin
;;        (define x y))
;;      x))

;; (test-expand "Let macro"
;;   '(lambda (x)
;;      ((lambda (y)
;;         y)
;;       x))
;;   '(lambda (x)
;;      (let ((y x))
;;        y)))

;; (test-expand "Named let"
;;   '(letrec* ((loop (lambda (x)
;;                      (loop x))))
;;      (loop '1))
;;   '(let loop ((x 1))
;;      (loop x)))

;; (test-expand "Named let and hygiene"
;;   '(lambda (l)
;;      (letrec* ((loop (lambda (x)
;;                        (loop x))))
;;        (loop l)))
;;   '(lambda (lambda)
;;      (let loop ((x lambda))
;;        (loop x))))

;; (test-expand "And 0"
;;   ''#t
;;   '(and))

;; (test-expand "And 1"
;;   ''a
;;   '(and 'a))

;; (test-expand "And 2"
;;   '(if 'a 'b '#f)
;;   '(and 'a 'b))

;; (test-expand "Or 0"
;;   ''#f
;;   '(or))

;; (test-expand "Or 1"
;;   ''a
;;   '(or 'a))

;; (test-expand "Or 2"
;;   '((lambda (r)
;;       (if r r 'b))
;;     'a)
;;   '(or 'a 'b))

;; (test-expand "Hygiene in or"
;;   '(lambda (let)
;;      ((lambda (r)
;;         (if r r 'b))
;;       'a))
;;   '(lambda (let)
;;      (or 'a 'b)))

;; (test-expand "Case"
;;   '(lambda (memv)
;;      ((lambda (key)
;;         (if (memv key '(a))
;;             '1
;;             '2))
;;       'key))
;;   '(lambda (memv)
;;      (syntax-parameterize
;;          (($memv (identifier-syntax memv)))
;;        (case 'key
;;          ((a) 1)
;;          (else 2)))))

;; (test-expand "Hygiene"
;;   '(lambda (x)
;;      ((lambda (y)
;;         y)
;;       x))
;;   '(lambda (lambda)
;;      (let ((y lambda))
;;        y)))

;; (test-expand "Module"
;;   '(letrec*
;;        ((x1 '1)
;;         (x2 '2))
;;      x2)
;;   '(letrec* ()
;;      ($define-module a ()
;;        (define x 1))
;;      (define x 2)
;;      x))

;; (test-expand "Module imports"
;;   '(letrec*
;;        ((x1 '1))
;;      x1)
;;   '(letrec* ()
;;      ($define-module a (x)
;;        (define x 1))
;;      ($import a)
;;      x))

;; (test-expand "Import only"
;;   '(letrec*
;;        ((f1 '1)
;;         (x1 '1)
;;         (f2 '2))
;;      x1)
;;   '(letrec* ()
;;      (define f 1)
;;      ($define-module a (define x)
;;        (define x 1))
;;      ($import-only a)
;;      (define f 2)
;;      x))

;; (test-expand "Top-level"
;;   '(lambda (y)
;;      (letrec ((y1 y)
;;               (x '1)
;;               (dummy1 (set! y1 '2))
;;               (dummy2 x))
;;        'unspecified))
;;   '(lambda (y)
;;      ($define-module a (define y))
;;      ($top-level a
;;        (define x 1)
;;        (define y 2)
;;        x)))

;; (test-expand "Top-level with redefinition of begin"
;;   '(lambda (y)
;;      (letrec ((x y)
;;               (begin '2)
;;               (dummy x))
;;        'unspecified))
;;   '(lambda (y)
;;      ($define-module a (define begin y))
;;      ($top-level a
;;        (begin (define x y))
;;        (define begin 2)
;;        x)))

;; (test-expand "Include"
;;   '(lambda ()
;;      (letrec ((x '1))
;;        x))
;;   '(lambda ()
;;      (include-at/relative-to #f #f "expand-input.scm")
;;      x))

;; (test-expand "Syntax definitions"
;;   '(lambda (x)
;;      x)
;;   '(lambda (x)
;;      (define-syntax foo
;;        (identifier-syntax x))
;;      foo))

;; (test-expand "Syntax rules"
;;   '(lambda (x)
;;      x)
;;   '(lambda (x)
;;      (define-syntax foo
;;        (syntax-rules (a)
;;          ((foo a a) b)
;;          ((foo a y) y)))
;;      (foo a x)))

;; (test-expand "let-syntax"
;;   '(lambda (foo)
;;      (letrec ((x '1))
;;        foo))
;;   '(lambda (foo)
;;      (let-syntax
;;          ((foo (identifier-syntax foo)))
;;        (define x 1)
;;        foo)))

;; (test-expand "letrec-syntax"
;;   '(lambda (foo x)
;;      ((lambda (d)
;;         foo)
;;       x))
;;   '(lambda (foo x)
;;      (letrec-syntax
;;          ((foo (identifier-syntax x))
;;           (bar (identifier-syntax foo)))
;;        bar)
;;      foo))

;; (test-expand "let-syntax in expression context"
;;   '((lambda (dummy) '1) '1)
;;   '(let-syntax
;;        ((foo (identifier-syntax 1)))
;;      foo
;;      foo))

;; (test-expand "Syntax parameters"
;;   '(lambda (call)
;;      (letrec* ((x '0))
;;        ((lambda (d1)
;;           ((lambda (d2)
;;              ((lambda (d3)
;;                 '42)
;;               (call '42)))
;;            '0))
;;         (call (quote 0)))))
;;   '(lambda (call)
;;      (define-syntax-parameter foo (identifier-syntax 42))
;;      (syntax-parameterize
;;          ((foo (identifier-syntax 0)))
;;        (define x foo)
;;        (call foo)
;;        foo)
;;      (call foo)
;;      foo))

;; (test-expand "Syntax parameters in expression context"
;;   '((lambda (d)
;;       'b)
;;     'a)
;;   '(letrec* ()
;;      (define-syntax-parameter foo (identifier-syntax 'a))
;;      foo
;;      (syntax-parameterize ((foo (identifier-syntax 'b)))
;;        foo)))

;; (test-expand "Macro that expands into macro"
;;   '((lambda (d1)
;;       ((lambda (d2)
;;          '2)
;;        '1))
;;     'foo)
;;   '(letrec* ()
;;      (define-syntax bar
;;        (syntax-rules ()
;;          ((bar)
;;           (begin
;;             (define-syntax baz
;;               (syntax-rules ()
;;                 ((baz) 'foo)))
;;             (baz)))))
;;      (bar)
;;      1
;;      2))

;; (test-expand "Custom Ellipsis"
;;   ''($top-level module (include-at/relative-to-ci $top-level #f "filename.scm"))
;;   '(letrec* ()
;;      (define-syntax %include-top-level-ci
;;        (syntax-rules ()
;;          ((%include-top-level-ci context module filename filename* ...)
;;           (begin
;;             (define-syntax wrapper
;;               (syntax-rules ::: ()
;;                 ((wrapper include-at/relative-to-ci c)
;;                  '($top-level module
;;                     (include-at/relative-to-ci $top-level c filename)
;;                     (include-at/relative-to-ci $top-level c filename*) ...))))
;;             (wrapper include-at/relative-to-ci context)))))
;;      (%include-top-level-ci #f module "filename.scm")))

;; (test-expand "Modules with marks"
;;   '(lambda (x)
;;      (letrec ((y x))
;;        y))
;;   '(lambda (x)
;;      ($define-module m (y)
;;        (define y x))
;;      (let-syntax
;;          ((foo (syntax-rules ()
;;                  ((foo m)
;;                   (begin
;;                     ($import m)
;;                     y)))))
;;        (foo m))))

;; (test-expand "Modules with extra contour"
;;   '(lambda (x)
;;      (letrec ((y x))
;;        y))
;;   '(lambda (x)
;;      ($define-module m (y)
;;        (define y x))
;;      (letrec* ()
;;        ($import m)
;;        y)))

;; (test-expand "Modules with marks and extra contour"
;;   '(lambda (x)
;;      (letrec ((y x))
;;        y))
;;   '(lambda (x)
;;      ($define-module m (y)
;;        (define y x))
;;      (let-syntax
;;          ((foo (syntax-rules ()
;;                  ((foo m)
;;                   (letrec* ()
;;                     ($import m)
;;                     y)))))
;;        (foo m))))

;; (test-expand "Nested macros"
;;   '(lambda (x)
;;      (letrec* ((y x))
;;        (letrec* ((dummy1 '1)
;;                  (dummy2 y))
;;          'unspecified)))
;;   '(lambda (x)
;;      ($define-module m (x y)
;;        (define y x))
;;      (define-syntax goo
;;        (syntax-rules ()
;;          ((goo)
;;           (let-syntax
;;               ((foo (syntax-rules ()
;;                       ((_ d)
;;                        (begin
;;                          ($top-level m
;;                            d
;;                            y))))))
;;             (foo 1)))))
;;      (goo)))

;; (test-expand "Syntax parameterize and local definitions"
;;   '(letrec* ((x '1)
;;             (y '2)
;;             (z '3))
;;     '4)
;;   '(letrec* ()
;;      (define x 1)
;;      (syntax-parameterize ()
;;        (define y 2))
;;      (define z 3)
;;      4))

;; (test-expand "Quote"
;;   '(lambda (list)
;;      (letrec* ((%values (list 'values)))
;;        '0))
;;   '(lambda (list)
;;      (define %values (list 'values))
;;     0))

;; (test-end)

;; Local Variables:
;; eval: (put 'test-expand 'scheme-indent-function 1)
;; eval: (font-lock-add-keywords nil
;;                               '(("(\\(test-expand\\)\\>" 1 font-lock-keyword-face)))
;; End:
