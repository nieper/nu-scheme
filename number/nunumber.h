/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2018 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NUNUMBER_H_INCLUDED
#define NUNUMBER_H_INCLUDED

#if BUILDING_LIBNUNUMBER && HAVE_VISIBILITY
# define LIBNUNUMBER_DLL_EXPORTED __attribute__((__visibility("default")))
#else
# define LIBNUNUMBER_DLL_EXPORTED
#endif

#include <mpc.h>
#include <mpfr.h>
#include <gmp.h>

void nu_complex_init (mpc_t);
long nu_get_z_10exp (mpz_t, mpfr_t);
void nu_inexact_to_exact (mpq_t, mpfr_t);

#endif /* NUNUMBER_H_INCLUDED */
