;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu vicinity)
  #:export (program-vicinity library-vicinity implementation-vicinity
                             user-vicinity home-vicinity
                             sub-vicinity make-vicinity
                             pathname->vicinity vicinity:suffix?)
  #:use-module (srfi srfi-98)
  #:replace (in-vicinity))

(define (program-vicinity) "")

(define (library-vicinity) "")

(define (implementation-vicinity) "")

(define (user-vicinity) "")

(define (home-vicinity)
  (get-environment-variable "HOME"))

(define (in-vicinity vicinity filename)
  (string-append vicinity filename))

(define (sub-vicinity vicinity name)
  (string-append vicinity name "/"))

(define (make-vicinity dirname)
  dirname)

(define (pathname->vicinity pathname)
  (let loop ((chars (reverse! (string->list pathname))))
    (cond
     ((null? chars)
      "")
     ((vicinity:suffix? (car chars))
      (list->string (reverse! chars)))
     (else
      (loop (cdr chars))))))

(define (vicinity:suffix? char)
  (char=? #\/ char))
