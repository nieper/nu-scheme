;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu expression)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs exceptions)
  #:use-module (nu list)
  #:use-module (nu symbol)
  #:use-module (nu syntax-object)
  #:re-export (guard)
  #:replace (unspecified?
             (expression . quasiquote)
             make-variable)
  #:export (->
            expression->datum variable->datum
            make-data data? data-datum
            variable? variable-name variable-source-location
            make-lexical-reference lexical-reference? lexical-reference-variable
            make-lexical-assignment lexical-assignment? lexical-assignment-variable
            lexical-assignment-expression
            make-primitive-reference primitive-reference? primitive-reference-operator
            make-application application? application-operator application-operands
            make-function function? function-formals function-body
            make-conditional condition? conditional-test conditional-consequent
            conditional-alternate
            make-letrec letrec? letrec-variables letrec-inits letrec-body
            make-letrec* letrec*? letrec*-variables letrec*-inits letrec*-body
            make-unspecified <unspecified>
            make-undefined <undefined>
	    make-annotation annotation-source-location annotation-expression
	    make-begin make-let
	    annotation
            match))

;;; Auxiliary syntax:

(define-syntax ->
  (lambda (stx)
    (syntax-case stx ()
      (k
       #'(raise (syntax-violation '-> "misplaced aux keyword" k))))))

(define-syntax annotation
  (lambda (stx)
    (syntax-case stx ()
      (k
       #'(raise (syntax-violation 'annotation "misplaced aux keyword" k))))))

;;; Expression data types:

(define-record-type <data>
  (make-data datum)
  data?
  (datum data-datum))

(define-record-type <variable>
  (%make-variable name srcloc)
  variable?
  (name variable-name)
  (srcloc variable-source-location))

(define-record-type <lexical-reference>
  (make-lexical-reference var)
  lexical-reference?
  (var lexical-reference-variable))

(define-record-type <lexical-assignment>
  (make-lexical-assignment var expr)
  lexical-assignment?
  (var lexical-assignment-variable)
  (expr lexical-assignment-expression))

(define-record-type <primitive-reference>
  (make-primitive-reference op)
  primitive-reference?
  (op primitive-reference-operator))

(define-record-type <conditional>
  (make-conditional test consequent alternate)
  conditional?
  (test conditional-test)
  (consequent conditional-consequent)
  (alternate conditional-alternate))

(define-record-type <application>
  (make-application operator operands)
  application?
  (operator application-operator)
  (operands application-operands))

(define-record-type <function>
  (make-function formals body)
  function?
  (formals function-formals)
  (body function-body))

(define-record-type <letrec>
  (%make-letrec bindings body)
  letrec?
  (bindings letrec-bindings)
  (body letrec-body))

(define-record-type <letrec*>
  (%make-letrec* bindings body)
  letrec*?
  (bindings letrec*-bindings)
  (body letrec*-body))

(define-record-type <unspecified>
  (make-unspecified)
  unspecified?)

(define-record-type <annotation>
  (%make-annotation srcloc expr)
  annotation?
  (srcloc annotation-source-location)
  (expr annotation-expression))

(define (make-variable id)
  (let ((srcloc (and (syntax? id)
		     (syntax-object-source-location id)))
	(name (if (syntax? id)
		  (syntax-object-expression id)
		  id)))
    (%make-variable (gensym (symbol->string name))
		    srcloc)))

(define (make-letrec bindings body)
  (if (null? bindings)
      body
      (%make-letrec bindings body)))

(define (make-letrec* bindings body)
  (if (null? bindings)
      body
      (%make-letrec* bindings body)))

(define (make-expression e)
  (cond ((variable? e)
	 (make-lexical-reference e))
	((symbol? e)
	 (make-primitive-reference e))
	(else
	 e)))

(define (make-annotation srcloc expr)
  (cond ((if (syntax? srcloc)
	     (syntax-object-source-location srcloc)
	     srcloc)
	 => (lambda (srcloc)
	      (%make-annotation srcloc expr)))
	(else
	 expr)))

;;; Expression builder:

(define-syntax expression
  (lambda (stx)
    (syntax-case stx ()
      ((_ e) (build-expression #'e)))))

(eval-when (expand load eval)

  (define (build-expression e)
    (syntax-case e (if let letrec letrec* expression quote unquote
		       lambda
                       begin annotation
                       <undefined> <unspecified>)
      ((if x y)
       (with-syntax ((test (build-expression #'x))
                     (consequent (build-expression #'y))
                     (alternate #'(make-unspecified)))
         #'(make-conditional test consequent alternate)))
      ((if x y z)
       (with-syntax ((test (build-expression #'x))
                     (consequent (build-expression #'y))
                     (alternate (build-expression #'z)))
         #'(make-conditional test consequent alternate)))
      ;; Letrec
      ((letrec bindings . body)
       (with-syntax ((bindings (build-bindings #'bindings))
		     (body (build-expression* #'body)))
	 #'(make-letrec bindings (make-begin body))))
      ;; Letrec*
      ((letrec* bindings . body)
       (with-syntax ((bindings (build-bindings #'bindings))
		     (body (build-expression* #'body)))
	 #'(make-letrec* bindings (make-begin body))))
      ;; Function
      ((lambda arg* . body)
       (with-syntax ((arg* (build-var* #'arg*))
		     (body (build-expression* #'body)))
	 #'(make-function arg* (make-begin body))))
      ;; Sequence
      ((begin . x)
       (with-syntax ((body (build-expression* #'x)))
	 #'(make-begin body)))
      ;; Let
      ((let bindings . body)
       (with-syntax ((bindings (build-bindings #'bindings))
		     (body (build-expression* #'body)))
	 #'(make-let (map car bindings) (map cadr bindings) body)))
      ;; Annotation
      ((annotation ,a x)
       (with-syntax ((expr (build-expression #'x)))
	 #'(make-annotation a expr)))
      (,x
       #'(make-expression x))
      ((expression x)
       #'(make-data `x)) ;; FIXME: We want our own quasiquote here!
      ((quote x)
       #'(make-data 'x))
      ((x . y)
       (with-syntax ((operator (build-expression #'x))
                     (operand* (build-expression* #'y)))
         #'(make-application operator operand*)))
      (<unspecified>
       #'(make-unspecified))
      (<undefined>
       #'(make-undefined))
      (x
       (identifier? #'x)
       #'(make-primitive-reference 'x))
      (x
       #'(make-data 'x))))

  (define (build-expression* e*)
    (syntax-case e* (unquote unquote-splicing)
      (((unquote-splicing x* ...) . y)
       (with-syntax ((expr* (build-expression* #'y)))
         #'(append (map make-expression x*) ... expr*)))
      (,x
       #'(map make-expression x))
      ((x . y)
       (with-syntax ((expr (build-expression #'x))
                     (expr* (build-expression* #'y)))
         #'(cons expr expr*)))
      (()
       #''())))

  (define (build-var* e*)
    (syntax-case e* (unquote unquote-splicing)
      (((unquote-splicing x* ...) . y)
       (with-syntax ((arg* (build-var* #'y)))
	 #'(append x* ... arg*)))
      (,x
       #'x)
      ((,x . y)
       (with-syntax ((arg* (build-var* #'y)))
	 #'(cons x arg*)))
      (()
       #''())))

  (define (build-bindings e*)
    (syntax-case e* (unquote)
      ;; TODO: Repeated ,x
      (((,var* ,init*) ::: . e*)
       (ellipsis-identifier? #':::)
       (with-syntax ((bindings (build-bindings #'e*)))
	 #'`(,@(map (lambda (var init)
		      `(,var ,(make-expression init)))
		    var* init*)
	     . ,bindings)))
      (((,var init) . e*)
       (with-syntax ((init (build-expression #'init))
		     (bindings (build-bindings #'e*)))
	 #'`((,var ,init) . ,bindings)))
      (,x
       #'x)
      (()
       #''()))))

;;; Derived expressions:

(define (make-begin body)
  (if (null? (cdr body))
      (car body)
      (let ((var (make-variable 'dummy/begin)))
	(expression (let ((,var ,(car body)))
		      ,(make-begin (cdr body)))))))

(define (make-let vars inits body)
  (expression ((lambda (,@vars)
		 ,@body)
	       ,@inits)))

;;; Expression matcher:

(define-syntax match
  (lambda (stx)
    (syntax-case stx ()
      ((_ expr clause* ... clause)
       #`(let loop ((e expr))
           (let ((fail (lambda ()
                            (error "invalid expression" e))))
             #,(let loop ((clause #'clause)
                          (clause* (reverse! #'(clause* ...))))
                 (if (null? clause*)
                     (gen-clause clause)
                     #`(let ((fail (lambda ()
                                        #,(gen-clause clause))))
                         #,(loop (car clause*) (cdr clause*)))))))))))

(eval-when (expand load eval)

  (define (ellipsis-identifier? e)
    (and (identifier? e)
         (free-identifier=? e #'(... ...))))

  (define (gen-clause clause)
    (let*-values (((pattern guard-expr body)
                   (parse-clause clause))
                  ((matcher vars catas)
                   (gen-expression-matcher #'e pattern '() '())))
      (with-syntax ((((x e) ...) vars)
                    (((y (cata var* ...) lvl) ...) catas))
        (with-syntax (((c ...) (generate-temporaries #'(cata ...))))
          (matcher (lambda ()
                     #`(let ((c cata) ...)
                         (let ((x e) ...)
                           (if #,guard-expr
                               (let*-values (((var* ...) (mapn lvl c y)) ...)
                                 #,@body)
                               (fail))))))))))

  (define (parse-clause clause)
    (syntax-case clause (guard)
      ((pattern (guard guard-expr) body body* ...)
       (values #'pattern #'guard-expr #'(body body* ...)))
      ((pattern body body* ...)
       (values #'pattern #'#t #'(body body* ...)))))

  (define (gen-expression-matcher e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern (-> if lambda letrec letrec* quote expression unquote
                               annotation <unspecified> <undefined>)
        (,(cata -> var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'cata #'(var* ...) vars catas))
        (,(var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'loop #'(var* ...) vars catas))
        (,var
         (identifier? #'var)
         (gen-var-matcher #'e #'var vars catas))
	;; Conditional without alternative
	((if x y)
         (with-syntax (((test consequent) (generate-temporaries '(#f #f))))
           (let*-values (((test-matcher vars catas)
                          (gen-expression-matcher #'test #'x vars catas))
                         ((consequent-matcher vars catas)
                          (gen-expression-matcher #'consequent #'y vars catas)))
             (values (lambda (k)
                       #`(if (and (conditional? e)
                                  (unspecified? (conditional-alternate e)))
                             (let ((test (conditional-test e))
                                   (consequent (conditional-consequent e)))
                               #,(test-matcher (lambda ()
                                                 (consequent-matcher k))))
                             (fail)))
                     vars
                     catas))))
	;; Conditional
	((if x y z)
         (with-syntax (((test consequent alternate)
                        (generate-temporaries '(#f #f #f))))
           (let*-values (((test-matcher vars catas)
                          (gen-expression-matcher #'test #'x vars catas))
                         ((consequent-matcher vars catas)
                          (gen-expression-matcher #'consequent #'y vars catas))
                         ((alternate-matcher vars catas)
                          (gen-expression-matcher #'alternate #'z vars catas)))
             (values (lambda (k)
                       #`(if (conditional? e)
                             (let ((test (conditional-test e))
                                   (consequent (conditional-consequent e))
                                   (alternate (conditional-alternate e)))
                               #,(test-matcher
                                  (lambda ()
                                    (consequent-matcher
                                     (lambda ()
                                       (alternate-matcher k))))))
                             (fail)))
                     vars
                     catas))))
	;; Function
	((lambda args expr)
	 (with-syntax (((formals body) (generate-temporaries '(#f #f))))
	   (let*-values (((formals-matcher vars catas)
			  (gen-formals-matcher #'formals #'args vars catas))
			 ((body-matcher vars catas)
			  (gen-expression-matcher #'body #'expr vars catas)))
	     (values (lambda (k)
		       #`(if (function? e)
			     (let ((formals (function-formals e))
				   (body (function-body e)))
			       #,(formals-matcher (lambda () (body-matcher k))))
			     (fail)))
		     vars catas))))
	;; Letrec
	((letrec x y)
	 (with-syntax (((bindings body) (generate-temporaries '(#f #f))))
	   (let*-values (((bindings-matcher vars catas)
			  (gen-bindings-matcher #'bindings #'x vars catas))
			 ((body-matcher vars catas)
			  (gen-expression-matcher #'body #'y vars catas)))
	     (values (lambda (k)
		       #`(if (letrec? e)
			     (let ((bindings (letrec-bindings e))
				   (body (letrec-body e)))
			       #,(bindings-matcher (lambda ()
						     (body-matcher k))))
			     (fail)))
		     vars catas))))
	;; Letrec*
	((letrec* x y)
	 (with-syntax (((bindings body) (generate-temporaries '(#f #f))))
	   (let*-values (((bindings-matcher vars catas)
			  (gen-bindings-matcher #'bindings #'x vars catas))
			 ((body-matcher vars catas)
			  (gen-expression-matcher #'body #'y vars catas)))
	     (values (lambda (k)
		       #`(if (letrec*? e)
			     (let ((bindings (letrec*-bindings e))
				   (body (letrec*-body e)))
			       #,(bindings-matcher (lambda ()
						     (body-matcher k))))
			     (fail)))
		     vars catas))))
        ;; Annotation
        ((annotation ,srcloc x)
         (identifier? #'srcloc)
         (with-syntax (((expr) (generate-temporaries '(#f))))
           (let*-values (((matcher vars catas)
                          (gen-expression-matcher #'expr #'x vars catas)))
             (values (lambda (k)
                       #`(if (annotation? e)
                             (let ((expr (annotation-expression e)))
                               #,(matcher k))
                             (fail)))
                     (cons #'(srcloc (annotation-source-location e)) vars)
                     catas))))
        ;; Datum
        ('x
         (values (lambda (k)
                   #`(if (and (data? e)
                              (equal? (data-datum e) 'x))
                         #,(k)
                         (fail)))
                 vars
                 catas))
        ;; Quasiquoted datum
        ((expression x)
         (receive (matcher vars catas)
             (gen-datum-matcher #'e #'x vars catas)
           (values (lambda (k)
                     #`(if (data? e)
                           #,(matcher k)
                           (fail)))
                   vars
                   catas)))
        ;; Application
        ((x . y)
         (with-syntax (((operator operand*) (generate-temporaries '(#f #f))))
           (let*-values (((operator-matcher vars catas)
                          (gen-expression-matcher #'operator #'x vars catas))
                         ((operand*-matcher vars catas)
                          (gen-expression-matcher* #'operand* #'y vars catas)))
             (values (lambda (k)
                       #`(if (application? e)
                             (let ((operator (application-operator e))
                                   (operand* (application-operands e)))
                               #,(operator-matcher (lambda ()
                                                     (operand*-matcher k))))
                             (fail)))
                     vars
                     catas))))
        ;; Unspecified
        (<unspecified>
         (values (lambda (k)
                   #`(if (unspecified? e)
                         #,(k)
                         (fail)))))
	;; TODO: Remove undefined.
        ;; Undefined
        (<undefined>
         (values (lambda (k)
                   #`(if (undefined? e)
                         #,(k)
                         (fail)))))
        ;; TODO: Primitive!

        ;; Datum
        (x
         (not (identifier? #'x))
         (values (lambda (k)
                   #`(if (and (data? e)
                              (equal? (data-datum e) 'x))
                         #,(k)
                         (fail)))
                 vars
                 catas)))))

  (define (gen-expression-matcher* e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern ()
        ((x ::: y* ...)
         (ellipsis-identifier? #':::)
	 (gen-ellipsis #'e #'x #'(y* ...) #'() vars catas gen-expression-matcher))
        ((x . y)
         (with-syntax (((car-e cdr-e) (generate-temporaries '(#f #f))))
           (let*-values (((car-matcher vars catas)
                          (gen-expression-matcher #'car-e #'x vars catas))
                         ((cdr-matcher vars catas)
                          (gen-expression-matcher* #'cdr-e #'y vars catas)))
             (values (lambda (k)
                       #`(if (null? e)
                             (fail)
                             (let*-values (((car-e cdr-e) (car+cdr e)))
                               #,(car-matcher (lambda ()
                                                (cdr-matcher k))))))
                     vars catas))))
        (()
	 (gen-null-matcher #'e vars catas)))))

  (define (gen-cata-matcher e cata var* vars catas)
    (with-syntax ((e e) (cata cata) (var*  var*)
                  ((var) (generate-temporaries '(#f))))
      (values (lambda (k)
                (k))
              (cons #'(var e) vars)
              (cons #'(var (cata . var*) 0) catas))))

  (define (gen-var-matcher e var vars catas)
    (with-syntax ((e e) (var var))
      (values (lambda (k)
                (k))
              (cons #'(var e) vars)
              catas)))

  (define (gen-dotted-list-matcher e pattern vars catas gen-matcher)
    (with-syntax ((e e))
      (syntax-case pattern (unquote)
	((x ::: y* ... . ,z)
	 (ellipsis-identifier? #':::)
	 (gen-ellipsis #'e #'x #'(y* ...) #',z vars catas gen-matcher))
	((x ::: y* ... . z)
	 (ellipsis-identifier? #':::)
	 (gen-ellipsis #'e #'x #'(y* ...) #'z vars catas gen-matcher))
	(,x
	 (gen-matcher #'e #',x vars catas))
	((x . y)
	 (gen-pair-matcher #'e #'x #'y vars catas gen-matcher
			   (lambda (e pattern vars catas)
			     (gen-dotted-list-matcher e pattern vars catas gen-matcher))))
	(()
	 (gen-null-matcher #'e vars catas))
	(x
	 (gen-matcher #'e #'x vars catas)))))

  (define (gen-list-matcher e pattern vars catas gen-matcher)
    (with-syntax ((e e))
      (syntax-case pattern (unquote)
	((x ::: y* ...)
	 (ellipsis-identifier? #':::)
	 (gen-ellipsis #'e #'x #'(y* ...) #'() vars catas gen-matcher))
	((x . y)
	 (gen-pair-matcher #'e #'x #'y vars catas gen-matcher
			   (lambda (e pattern vars catas)
			     (gen-list-matcher e pattern vars catas gen-matcher))))
	(()
	 (gen-null-matcher #'e vars catas)))))

  (define (gen-pair-matcher e car-pattern cdr-pattern vars catas gen-car-matcher
			    gen-cdr-matcher)
    (with-syntax ((e e))
      (with-syntax (((car-e cdr-e) (generate-temporaries '(#f #f))))
	(let*-values (((car-matcher vars catas)
		       (gen-car-matcher #'car-e car-pattern vars catas))
		      ((cdr-matcher vars catas)
		       (gen-cdr-matcher #'cdr-e cdr-pattern vars catas)))
	  (values (lambda (k)
		    #`(if (pair? e)
			  (let ((car-e (car e))
				(cdr-e (cdr e)))
			    #,(car-matcher (lambda ()
					     (cdr-matcher k))))
			  (fail)))
		  vars
		  catas)))))

  (define (gen-null-matcher e vars catas)
    (with-syntax ((e e))
      (values (lambda (k)
		#`(if (null? e)
		      #,(k)
		      (fail)))
	      vars catas)))

  (define (gen-ellipsis e x y* z vars catas gen-matcher)
    (with-syntax ((e e) (x x) ((y* ...) y*) (z z))
      (let ((l (length #'(y* ...))))
	(with-syntax ((l l)
		      ((head-e tail-e) (generate-temporaries '(#f #f))))
	  (let*-values (((head-matcher vars catas)
			 (gen-map #'head-e #'x vars catas
				  gen-matcher))
			((tail-matcher vars catas)
			 (gen-matcher* #'tail-e #'(y* ...) #'z vars catas
				       gen-matcher)))
	    (values (lambda (k)
		      #`(let ((n (length e)))
			  (if (>= n l)
			      (let*-values (((head-e tail-e)
					     (split-at! e (- n l))))
				#,(head-matcher (lambda ()
						  (tail-matcher k)))))))
		    vars catas))))))

  (define (gen-formals-matcher e pattern vars catas)
    (gen-dotted-list-matcher e pattern vars catas gen-formal-matcher))

  (define (gen-formal-matcher e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern (-> unquote)
        (,(cata -> var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'cata #'(var* ...) vars catas))
        (,(var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'loop #'(var* ...) vars catas))
        (,var
         (identifier? #'var)
         (gen-var-matcher #'e #'var vars catas)))))

  (define (gen-bindings-matcher e pattern vars catas)
    (gen-list-matcher e pattern vars catas gen-binding-matcher))

  (define (gen-binding-matcher e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern ()
	((x y)
	 (with-syntax (((var init) (generate-temporaries '(#f #f))))
	   (let*-values (((var-matcher vars catas)
			  (gen-formal-matcher #'var #'x vars catas))
			 ((init-matcher vars catas)
			  (gen-expression-matcher #'init #'y vars catas)))
	     (values (lambda (k)
		       #`(let ((var (car e))
			       (init (cadr e)))
			   #,(var-matcher (lambda () (init-matcher k)))))
		     vars catas)))))))

  (define (gen-datum-matcher e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern (-> unquote)
        (,(cata -> var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'cata #'(var* ...) vars catas))
        (,(var* ...)
         (every identifier? #'(var* ...))
         (gen-cata-matcher #'e #'loop #'(var* ...) vars catas))
        (,var
         (identifier? #'var)
         (gen-var-matcher #'e #'var vars catas))
        ((x ::: y* ... . ,z)
         (ellipsis-identifier? #':::)
         (gen-ellipsis #'e #'x #'(y* ...) #',z vars catas gen-datum-matcher))
        ((x ::: y* ... . z)
         (ellipsis-identifier? #':::)
         (gen-ellipsis #'e #'x #'(y* ...) #'z vars catas gen-datum-matcher))
	((x . y)
	 (gen-pair-matcher #'e #'x #'y vars catas gen-datum-matcher
			   gen-datum-matcher))
        (#(x ...)
         (let*-values (((matcher vars catas)
                        (gen-datum-matcher #'e #'(x ...) vars catas)))
           (values (lambda (k)
                     #`(if (vector? e)
                           (let ((e (vector->list e)))
                             #,(matcher k))))
                   vars
                   catas)))
        (x
         (values (lambda (k)
                   #`(if (equal? e 'x)
                         #,(k)
                         (fail)))
                 vars
                 catas)))))

  (define (gen-datum-map e pattern vars catas)
    (gen-map e pattern vars catas gen-datum-matcher))

  (define (gen-datum-matcher* e pattern* tail vars catas)
    (gen-matcher* e pattern* tail vars catas gen-datum-matcher*))

  (define (gen-map e pattern vars catas gen-matcher)
    (receive (matcher inner-vars inner-catas)
	(gen-matcher #'f pattern '() '())
      (with-syntax ((e e)
                    ((loop) (generate-temporaries '(#f)))
                    ((f* ...) (generate-temporaries inner-vars))
                    (((x* e*) ...) inner-vars)
                    (((var (cata var* ...) level) ...) inner-catas))
        (values (lambda (k)
                  #`(let loop ((e (reverse e))
                               (f* '())
                               ...)
                      (if (null? e)
                          #,(k)
                          (let ((f (car e)))
                            #,(matcher (lambda ()
                                         #`(loop (cdr e)
                                                 (cons e* f*)
                                                 ...)))))))
                (append #'((x* f*) ...) vars)
                (append #'((var (cata var* ...) (+ level 1)) ...) catas)))))

  (define (gen-matcher* e pattern* tail vars catas gen-matcher)
    (let loop ((e e) (pattern* pattern*) (vars vars) (catas catas))
      (with-syntax ((e e))
        (if (null? pattern*)
            (syntax-case tail ()
              (() (values (lambda (k)
                            #`(if (null? e)
                                  #,(k)
                                  (fail)))
                          vars
                          catas))
              (tail (gen-matcher #'e #'tail vars catas)))
            (with-syntax (((car-e cdr-e) (generate-temporaries '(#f #f))))
              (let*-values (((car-matcher vars catas)
                             (gen-matcher #'car-e (car pattern*) vars catas))
                            ((cdr-matcher vars catas)
                             (loop #'cdr-e (cdr pattern*) vars catas)))
                (values (lambda (k)
                          #`(receive (car-e cdr-e)
                                (car+cdr e)
                              #,(car-matcher (lambda ()
                                               (cdr-matcher k)))))
                        vars
                        catas))))))))

(define (mapn n proc l)
  (let loop ((n n) (l l))
    (if (zero? n)
        (proc l)
        (let ((n (- n 1)))
          (map (lambda (x)
                 (loop n x))
               l)))))

;;; Expression converter:

(define (expression->datum expr)
  (match expr
    ((if ,(x) ,(y))
     `(if ,x ,y))
    ((if ,(x) ,(y) ,(z))
     `(if ,x ,y ,z))
    ((lambda (,(variable->datum -> x) ... . ,(variable->datum -> y)) ,(z))
     `(lambda (,@x . ,y) ,z))
    ((letrec ((,(variable->datum -> x*) ,(y*)) ...) ,(z))
     `(letrec (,@(map list x* y*)) ,z))
    ((annotation ,_ ,(x))
     x)
    ((expression ,x)
     (let ((datum (data-datum x)))
       (if (symbol? datum)
	   `',datum
	   datum)))
    ((,(x) ,(y*) ...)
     `(,x . ,y*))
    (,x
     (guard (lexical-reference? x))
     (variable->datum (lexical-reference-variable x)))
    (,x
     (guard (primitive-reference? x))
     (primitive-reference-operator x))))

(define (variable->datum var)
  (cond ((null? var)
	 '())
	((symbol? var)
	 var)
	(else
	 (string->symbol (or (gensym->unique-string (variable-name var))
			     (symbol->string (variable-name var)))))))
