;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu io)
  #:use-module ((ice-9 ports)
                #:select (port-filename
                          (port-line . scheme-port-line)
                          port-column))
  #:use-module (rnrs io simple)
  #:use-module (nu error)
  #:use-module (nu list)
  #:use-module (nu vicinity)
  #:use-module (nu syntax-object)
  #:export (write-string
            port-line
            read-syntax
            read-file)
  #:re-export ((force-output . flush-output-port)
	       (write . write-simple)
	       port-filename
               port-column))

(define fold-case?-property (make-object-property))

(define write-string display)

(define (port-fold-case? port)
  (fold-case?-property port))

(define (port-set-fold-case?! port fold-case?)
  (set! (fold-case?-property port) fold-case?))

(define (port-line port)
  (+ (scheme-port-line port) 1))

;;; Reader

(define read-syntax
  (case-lambda
    (() (%read-syntax (port-filename (current-input-port))
                      (current-input-port)))
    ((port) (%read-syntax (port-filename port)
                          port))
    ((source port) (%read-syntax source port))))

(define (%read-syntax source port)

  (define (fold-case str)
    (if (port-fold-case? port)
        (string-downcase str)
        str))

  (define (whitespace? c)
    (memv c '(#\space #\newline #\ #\	)))

  (define (delimiter? c)
    (or (whitespace? c)
	(memv c '(#\( #\) #\" #\;))
	(eof-object? c)))

  (define (read-intertoken-space!)
    (let ((c (peek-char port)))
      (case c
	((#\;)
	 (read-comment!)
	 (read-intertoken-space!))
	(else
	 (cond
	  ((whitespace? c)
	   (read-char port)
	   (read-intertoken-space!)))))))

  (define (read-comment!)
    (if (and (not (eof-object? (peek-char port)))
             (not (char=? (read-char port) #\newline)))
	(read-comment!)))

  (define (read-token . char*)
    (let loop ((chars char*))
      (if (delimiter? (peek-char port))
	  (list->string (reverse! chars))
	  (loop (cons (read-char port) chars)))))

  (let read ((handle-dot
	      (lambda (reader-error)
		(reader-error "unexpected dot")))
	     (handle-closing-parenthesis
	      (lambda (reader-error)
		(reader-error "unexpected closing parenthesis"))))

    (read-intertoken-space!)

    (let ((line (port-line port))
          (col (port-column port)))

      (define (make-syntax expression)
        (datum->syntax
         #f
         expression
         (make-source-location source line col
                               (port-line port) (port-column port))))

      (define (reader-error msg)
        (error msg
               (make-source-location source line col
                                     (port-line port) (port-column port))))

      (define (read-abbreviation name)
        (make-syntax (list (make-syntax (string->symbol name))
                           (read handle-dot
                                 handle-closing-parenthesis))))

      (let ((c (read-char port)))
        (case c
          ((#\()
           (call/cc
            (lambda (k)
              (let loop ((datums '()))
                (define (handle-dot reader-error)
                  (let loop ((tail '()))
                    (define (handle-dot reader-error)
                      (reader-error "unexpected dot"))
                    (define (handle-closing-parenthesis reader-error)
                      (if (null? tail)
                          (error "element missing after dot")
                          (k (make-syntax (append-reverse! datums
                                                           (car tail))))))
                    (let ((datum (read handle-dot handle-closing-parenthesis)))
                      (cond
                       ((eof-object? datum)
                        (reader-error "incomplete list tail"))
                       ((pair? tail)
                        (reader-error "multiple tokens in dotted tail"))
                       (else
                        (loop (list datum)))))))
                (define (handle-closing-parenthesis reader-error)
                  (k (make-syntax (reverse! datums))))
                (let ((datum (read handle-dot handle-closing-parenthesis)))
                  (if (eof-object? datum)
                      (reader-error "incomplete list")
                      (loop (cons datum datums))))))))
          ((#\))
           (handle-closing-parenthesis reader-error))
          ((#\')
           (read-abbreviation "quote"))
          ((#\`)
           (read-abbreviation "quasiquote"))
          ((#\,)
           (case (peek-char port)
             ((#\@)
              (read-char port)
              (read-abbreviation "unquote-splicing"))
             (else
              (read-abbreviation "unquote"))))
          ((#\")
           (let loop ((chars '()))
             (let ((c (read-char port)))
               (case c
                 ((#\")
                  (make-syntax (list->string (reverse! chars))))
                 ((#\\)
                  (case (read-char port)
                    ((#\")
                     (loop (cons #\" chars)))
                    ((#\\)
                     (loop (cons #\\ chars)))
                    (else
                     (reader-error "invalid escape in string"))))
                 (else
                  (if (eof-object? c)
                      (reader-error "incomplete string")
                      (loop (cons c chars))))))))
          ((#\#)
           (let ((c (read-char port)))
             (case c
               ((#\')
                (read-abbreviation "syntax"))
               ((#\,)
                (case (peek-char port)
                  ((#\@)
                   (read-char port)
                   (read-abbreviation "unsyntax-splicing"))
                  (else
                   (read-abbreviation "unsyntax"))))
               ((#\`)
                (read-abbreviation "quasisyntax"))
               ((#\t #\T)
                (make-syntax #t))
               ((#\f #\F)
                (make-syntax #f))
               ((#\()
                (call/cc
                 (lambda (k)
                   (let loop ((datums '()))
                     (define (handle-dot reader-error)
                       (reader-error "unexpected dot in vector"))
                     (define (handle-closing-parenthesis reader-error)
                       (k (make-syntax (list->vector (reverse! datums)))))
                     (let ((datum (read handle-dot handle-closing-parenthesis)))
                       (if (eof-object? datum)
                           (reader-error "incomplete vector")
                           (loop (cons datum datums))))))))
               ((#\\)
                (let ((char (read-token)))
                  (cond
                   ((string=? (fold-case char) "space")
                    (make-syntax #\space))
                   ((string=? (fold-case char) "newline")
                    (make-syntax #\newline))
                   ((= (string-length char) 1)
                    (make-syntax (string-ref char 0)))
                   (else
                    (reader-error "invalid character")))))
               (else
                (if (eof-object? c)
                    (reader-error "incomplete sharp syntax")
                    (reader-error "invalid sharp syntax"))))))
          (else
           (cond
            ((eof-object? c) c)
            (else
             (let ((token (read-token c)))
               (cond
                ((string=? token ".")
                 (handle-dot reader-error))
                ((string->number token)
                 => (lambda (number)
                      (make-syntax number)))
                (else
                 ;; TODO: Reject invalid identifier syntax.
                 (make-syntax
                  (string->symbol (fold-case token))))))))))))))

(define (read-file ctx source filename fold-case?)
  (let* ((wrap (if ctx
                   (syntax-object-wrap ctx)
                   (empty-wrap)))
         (marks (wrap-marks wrap))
         (substs (wrap-substs wrap))
         (srcloc (and source (syntax-object-source-location source)))
         (source (and srcloc (source-location-source srcloc)))
         (vicinity (if source
                       (pathname->vicinity source)
                       (library-vicinity)))
         (filename (in-vicinity vicinity filename)))
    (call-with-input-file filename
      (lambda (p)
        (define (read)
          (let ((stx (read-syntax filename p)))
            (if (eof-object? stx)
                stx
                (syntax-object stx marks substs #f))))
        (port-set-fold-case?! p fold-case?)
        (unfold eof-object?
                values
                (lambda (x)
                  (read))
                (read))))))
