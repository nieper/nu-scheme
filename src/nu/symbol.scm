;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(define-module (nu symbol)
  #:use-module ((guile)
		#:select ((gensym . scheme-gensym)))
  #:use-module ((rnrs base)
                #:select (symbol=?))
  #:use-module (nu list)
  #:replace (gensym)
  #:re-export ((make-symbol . string->uninterned-symbol)
               symbol-interned? symbol? symbol->string string->symbol
               symbol=?)
  #:export (gensym->unique-string))

(define name-property (make-object-property))

(define gensym
  (case-lambda
    (()
     (gensym "g"))
    ((name)
     (let ((sym (make-symbol name)))
       (set! (name-property sym)
	 (symbol->string (scheme-gensym name)))
       sym))))

(define (gensym->unique-string sym)
  (name-property sym))

(define (symbol=? sym . sym*)
  (every (lambda (s)
           (eq? sym s))
         sym*))
