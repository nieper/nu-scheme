;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu show unicode)
  #:export (as-unicode unicode-terminal-width)
  #:use-module (system foreign)
  #:use-module (nu list)
  #:use-module (nu show base))

(define libunistring (dynamic-link "libunistring"))

(define uc-width
  (let ((width (pointer->procedure int
				   (dynamic-func "uc_width" libunistring)
				   (list uint32 '*))))
    (lambda (uc encoding)
      (width uc (string->pointer encoding)))))

(define (as-unicode . fmt*)
  (with ((string-width unicode-terminal-width))
    (each-in-list fmt*)))

(define (unicode-terminal-width str)
  (let loop ((chars (string->list str)) (width 0))
    (cond ((null? chars) width)
	  ((and (not (null? (cdr chars)))
		(char=? (car chars) #\x1b)
		(char=? (cadr chars) #\[))
	   (let loop/escape ((chars (cddr chars)))
	     (cond ((null? chars) width)
		   ((char=? (car chars) #\m) (loop (cdr chars) width))
		   (else (loop/escape (cdr chars))))))
	  (else
	   (loop (cdr chars) (+ width
				(max 0 (uc-width (char->integer (car chars))
						 "UTF-8"))))))))
