;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu show color)
  #:export (as-red
	    as-blue
	    as-green
	    as-cyan
	    as-yellow
	    as-magenta
	    as-white
	    as-black
	    as-bold
	    as-underline)
  #:use-module (nu environment-monad)
  #:use-module (nu show base))

(define color (make-environment-variable))
(define underline? (make-environment-variable))
(define bold? (make-environment-variable))

(define ansi-escape
  (fn ((color ,color)
       (underline? ,underline?)
       (bold? ,bold?))
    (each #\x1b #\[
	  (joined displayed
		  (cons "0"
			(append (if color (list color) '())
				(if bold? (list "1") '())
				(if underline? (list "4") '())))
		  ";")
	  #\m)))

(define (colored new-color fmt)
  (each (with ((,color new-color))
	  (each ansi-escape fmt))
	ansi-escape))

(define (as-red . fmt*) (colored "31" (each-in-list fmt*)))
(define (as-blue . fmt*) (colored "34" (each-in-list fmt*)))
(define (as-green . fmt*) (colored "32" (each-in-list fmt*)))
(define (as-cyan . fmt*) (colored "36" (each-in-list fmt*)))
(define (as-yellow . fmt*) (colored "33" (each-in-list fmt*)))
(define (as-magenta . fmt*) (colored "35" (each-in-list fmt*)))
(define (as-white . fmt*) (colored "37" (each-in-list fmt*)))
(define (as-black . fmt*) (colored "30" (each-in-list fmt*)))

(define (as-bold . fmt*)
  (each (with ((,bold? #t))
	  (each-in-list (cons ansi-escape fmt*)))
	ansi-escape))

(define (as-underline . fmt*)
  (each (with ((,underline? #t))
	  (each-in-list (cons ansi-escape fmt*)))
	ansi-escape))
