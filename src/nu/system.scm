;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu system)
  #:use-module (srfi srfi-98)
  #:export (c-compile))

(define (c-compiler)
  (or (get-environment-variable "CC")
      "cc"))

(define (c-flags)
  (or (get-environment-variable "CFLAGS")
      ""))

(define (cpp-flags)
  (or (get-environment-variable "CPPFLAGS")
      ""))

(define (c-compile source)
  (let* ((cc
	  (c-compiler))
	 (status
	  (system* cc "-c" (c-flags) (cpp-flags) source)))
    (unless (zero? (status:exit-val status))
      (error "~a: error invoking C compiler" cc))))
