;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu identifier)
  #:use-module (srfi srfi-9)
  #:use-module (nu list)
  #:use-module (nu syntax-object)
  #:use-module (nu comparator)
  #:use-module (nu hash-table)
  #:export (bound-identifier-member?
	    duplicate-bound-identifiers?
	    valid-bound-identifiers?
	    make-identifier-table
            identifier-table
            identifier-table-update!
            identifier-table-clear!
            identifier-table-ref
            identifier-table-for-each
            identifier-table-fold
            identifier-table->list
            global-environment
            top-level-wrap
            make-binding binding-type binding-value
            make-label identifier->label label->binding make-lexical-label
	    label-kill!
            make-rib rib-extend!
            shift shift?)
  #:replace (identifier? identifier->symbol
                         bound-identifier=? free-identifier=?))

;; XXX: We could make the "empty wrap" the global environment.
;; So globals.scm will import identifier.
;; syntax-case can then produce empty wraps!

;;; Identifiers:

(define (identifier? stx)
  (and (syntax? stx)
       (symbol? (syntax-object-expression stx))))

(define (identifier->symbol id)
  (syntax-object-expression id))

(define (free-identifier=? id1 id2)
  (label=? (identifier->label id1)
           (identifier->label id2)))

(define (bound-identifier=? id1 id2)
  (and (eq? (identifier->symbol id1)
            (identifier->symbol id2))
       (list= mark=?
              (wrap-marks (syntax-object-wrap id1))
              (wrap-marks (syntax-object-wrap id2)))))

(define (bound-identifier-member? id ids)
  (let loop ((ids ids))
    (and (not (null? ids))
         (if (pair? ids)
             (or (bound-identifier=? id (car ids))
                 (loop (cdr ids)))
             (bound-identifier=? id ids)))))

(define (duplicate-bound-identifiers? ids)
  (let loop ((ids ids))
    (and (pair? ids)
         (or (bound-identifier-member? (car ids) (cdr ids))
             (loop (cdr ids))))))

(define (valid-bound-identifiers? ids)
  (and (let loop ((ids ids))
         (or (null? ids)
             (if (pair? ids)
                 (and (identifier? (car ids))
                      (loop (cdr ids)))
                 (identifier? ids))))
       (not (duplicate-bound-identifiers? ids))))

;;; Identifier tables:

(define (make-identifier-table)
  (make-hash-table (make-comparator symbol? eq? #f symbol-hash)))

(define (identifier-table ids values abort)
  (let ((table (make-identifier-table)))
    (for-each
     (lambda (id value)
       (identifier-table-update! table
                                 id
                                 (lambda (prev)
                                   (if (and prev abort)
                                       (abort id))
                                   value)
                                 #f))
     ids values)
    table))

(define (identifier-table-update! table id update default)
  (hash-table-update!/default
   table
   (identifier->symbol id)
   (lambda (entries)
     (let ((marks (syntax-object-marks id)))
       (cond ((assq marks entries)
              => (lambda (entry)
                   (set-cdr! entry (update (cdr entry)))
                   entries))
             (else
              (alist-cons marks (update default) entries)))))
   '()))

(define (identifier-table-clear! table)
  (hash-table-clear! table))

(define (identifier-table-ref table sym marks)
  (hash-table-ref table
                  sym
                  (lambda ()
                    #f)
                  (lambda (labels)
                    (cond ((assoc marks labels) => cdr)
                          (else #f)))))

(define (identifier-table-for-each proc table)
  (hash-table-for-each
   (lambda (sym entries)
     (for-each
      (lambda (entry)
        (proc (cdr entry)))
      entries))
   table))

(define (identifier-table-fold proc seed table)
  (hash-table-fold
   (lambda (sym entries seed)
     (fold
      (lambda (entry seed)
        (proc (cdr entry) seed))
      seed entries))
   seed table))

(define (identifier-table->list table)
  (identifier-table-fold cons '() table))

;;; Substitution ribs:

(define (shift)
  'shift)

(define (shift? rib)
  (eq? rib (shift)))

(define (barrier)
  'barrier)

(define (make-barrier marks)
  (list (barrier) marks))

(define (barrier? chunk)
  (and (pair? barrier)
       (eq? (car chunk) (barrier))))

(define (barrier-marks barrier)
  (cadr barrier))

(define make-rib
  (case-lambda
    (()
     (list (make-identifier-table)))
    ((ids labels)
     (list (identifier-table ids
			     labels
			     (lambda (id)
			       (error "duplicate identifier" id)))))))

(define (rib-extend! rib id label)
  (identifier-table-update! (car rib)
                            id
                            (lambda (prev)
                              (when prev
                                (error "cannot redefine identifier"
                                       id))
                              label)
                            #f))

(define (rib-update! rib id label)
  (identifier-table-update! (car rib)
                            id
                            (lambda (prev)
                              label)
                            #f))

(define (rib-clear! rib)
  (identifier-table-clear! (car rib))
  (set-cdr! rib '()))

(define (rib-add-barrier! rib stx)
  (set-cdr! rib (cons* (make-barrier (syntax-object-marks stx))
                       (car rib)
                       (cdr rib)))
  (set-car! rib (make-identifier-table)))

(define (rib-search rib sym marks)
  (let loop ((chunks rib))
    (and (not (null? chunks))
         (cond ((barrier? (car chunks))
                (if (list= mark=?
                           marks
                           (barrier-marks (car chunks)))
                    sym
                    (loop (cdr chunks))))
               (else (or (identifier-table-ref (car chunks) sym marks)
                         (loop (cdr chunks))))))))

;;; Labels:

(define current-meta-level (make-parameter 0))

(define (identifier->label id)
  (let ((sym (identifier->symbol id))
        (wrap (syntax-object-wrap id)))
    (let search ((substs (wrap-substs wrap))
                 (marks (wrap-marks wrap)))
      (if (null? substs)
          sym
          (let ((subst (car substs)))
            (cond ((shift? subst)
                   (search (cdr substs) (cdr marks)))
                  (else
                   (or (rib-search subst sym marks)
                       (search (cdr substs) marks)))))))))

(define (label=? label1 label2)
  (eq? label1 label2))

(define-record-type <label>
  (make-label binding phase)
  label?
  (binding label-binding label-set-binding!)
  (phase label-phase))

(define (label-kill! label)
  (label-set-binding! label (displaced-lexical-binding)))

(define (label->binding label)
  (cond ((symbol? label)
         (undefined-binding))
        (else
         (label-binding label))))

(define (make-lexical-label var)
  (make-label (make-binding 'lexical var)
	      (current-meta-level)))

;;; Bindings:

(define (make-binding type value)
  (cons type value))

(define (binding-type binding)
  (car binding))

(define (binding-value binding)
  (cdr binding))

(define (undefined-binding)
  (make-binding 'undefined #f))

(define (displaced-lexical-binding)
  (make-binding 'displaced-lexical #f))

;;; Global environment:

(define global-environment
  (let ((env (make-rib)))
    (lambda ()
      env)))

(define top-level-wrap
  (let ((wrap (make-wrap '() (list (global-environment)))))
    (lambda ()
      wrap)))
