;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu quasiquote)
  #:use-module (nu list)
  #:replace (quasiquote))

(define-syntax quasiquote
  (lambda (stx)
    (syntax-case stx ()
      ((_ e) (emit (compile #'e 0))))))

(eval-when (expand load eval)

  (define (compile e level)
    (syntax-case e (unquote quasiquote)
      ((unquote e)
       (if (zero? level)
           #'("value" e)
           (gen-cons #'(quote unquote)
                     (compile #'(e) (- level 1)))))
      ((quasiquote e)
       (gen-cons #'(quote quasiquote)
                 (compile #'(e) (+ level 1))))
      ((h . t)
       (syntax-case #'h (unquote unquote-splicing)
         ((unquote e* ...)
          (if (zero? level)
              (gen-cons* #'((values e*) ...)
                         (compile #'t level))
              (gen-cons (gen-cons #'(quote unquote)
                                  (compile #'(e* ...) (- level 1)))
                        (compile #'t level))))
         ((unquote-splicing e* ...)
          (if (zero? level)
              (gen-append #'((values e*) ...) (compile #'t level))
              (gen-cons (gen-cons #'(quote unquote-splicing)
                                  (compile #'(e* ...) (- level 1)))
                        (compile #'t level))))
         (_
          (gen-cons (compile #'h level) (compile #'t level)))))
      (#(e* ...)
       (gen-vector (compile-vector #'(e* ...) level)))
      (e
       #'(quote e))))

  (define (compile-vector e level)
    (syntax-case e ()
      ((h . t)
       (syntax-case #'h (unquote unquote-splicing)
         ((unquote e* ...)
          (if (zero? level)
              (gen-cons* #'((values e*) ...)
                         (compile-vector #'t level))
              (gen-cons (gen-cons #'(quote unquote)
                                  (compile #'(e* ...) (- level 1)))
                        (compile-vector #'t level))))
         ((unquote-splicing e* ...)
          (if (zero? level)
              (gen-append #'((values e*) ...)
                          (compile-vector #'t level))
              (gen-cons (gen-cons #'(quote unquote-splicing)
                                  (compile #'(e* ...) (- level 1)))
                        (compile-vector #'t level))))
         (_
          (gen-cons (compile #'h level)
                    (compile-vector #'t level)))))
      (()
       #'(quote ()))))

  (define (gen-cons x y)
    (with-syntax ((x x)
                  (y y))
      (syntax-case #'y (quote list cons*)
        ((quote b)
         (syntax-case #'x (quote)
           ((quote a) #'(quote (a . b)))
           (_ (syntax-case #'b ()
                (() #'(list x))
                (_ #'(cons* x y))))))
        ((list . b*)
         #'(list x . b*))
        ((cons* . b*)
         #'(cons* x . b*))
        (_
         #'(cons* x y)))))

  (define (gen-append x y)
    (syntax-case y (quote)
      ((quote ())       
       (cond ((null? x) #'(quote ()))
             ((null? (cdr x)) (car x))
             (else #`(append . #,x))))
      (_
       (cond ((null? x) y)
             (else #`(append #,@x #,y))))))

  (define (gen-cons* x y)
    (fold-right (lambda (a b)
                  (gen-cons a b))
                y x))

  (define (gen-vector x)
    (syntax-case x (quote)
      ((quote (x* ...)) #'(quote #(x* ...)))
      (_
       (let loop ((y x)
                  (k (lambda (list)
                       #`(vector #,@list))))
         (syntax-case y (quote list cons*)
           ((quote (y* ...)) (k #'((quote y*) ...)))
           ((list y* ...) (k #'(y* ...)))
           ((cons* y* ... z) (loop #'z (lambda (list)
                                        (k (append #'(y* ...) list)))))
           (_ #`(list->vector #,x)))))))
  
  (define (emit x)
    (syntax-case x (quote list cons* append vector list->vector values)
      ((quote x) #''x)
      ((list x* ...) #`(list #,@(map emit #'(x* ...))))
      ((cons* x y) #`(cons #,(emit #'x) #,(emit #'y)))
      ((cons* x* ...) #`(cons* #,@(map emit #'(x* ...))))
      ((append x* ...) #`(append #,@(map emit #'(x* ...))))
      ((list->vector x) #`(list->vector #,(emit #'x)))
      ((values x) #'x))))
