;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu globals)
  #:use-module (srfi srfi-8)
  #:use-module (nu identifier)
  #:use-module (nu expand)
  #:use-module (nu syntax-object)
  #:use-module (nu syntax-case)
  #:use-module (nu expression)
  #:use-module (nu expand))

(define-syntax define-global!
  (syntax-rules ()
    ((define-global! name type value)
     (rib-extend! (global-environment)
                  #'name
                  (make-label (make-binding type value) 0)))))

(define-syntax define-core!
  (syntax-rules ()
    ((define-core! name expander)
     (define-global! name 'core expander))))

(define-syntax define-primitive!
  (syntax-rules ()
    ((define-primitive! name op)
     (define-global! name 'primitive op))))

;;; Core syntax:

(define-global! begin 'begin #f)
(define-global! define 'define #f)
(define-global! define-syntax 'define-syntax #f)

;;; Core forms:

(define-core! lambda
  (lambda (stx)
    (receive (formals body)
	(parse-lambda stx)
      (let* ((var* (map make-variable formals))
	     (label* (map make-lexical-label var*))
	     (rib (make-rib formals label*))
	     (body-expr (expand-body stx (add-subst* rib body))))
	(for-each label-kill! label*)
	`(annotation ,(syntax-object-source-location stx)
	   (lambda ,var* ,body-expr))))))

;;; Parsers:

(define (parse-lambda stx)
  (syntax-case stx ()
    ((_ (arg* ...) expr expr* ...)
     (valid-bound-identifiers? #'(arg* ...))
     (values #'(arg* ...) #'(expr expr* ...)))
    ((_ (arg* ... . arg) expr expr* ...)
     (valid-bound-identifiers? #'(arg* ... arg))
     (values #'(arg* ... . arg) #'(expr expr* ...)))
    (_
     (error "invalid lambda syntax" stx))))

;;; Primitives:

;; XXX
(define-primitive! display 'display)
(define-primitive! newline 'newline)

;; Local Variables:
;; eval: (font-lock-add-keywords nil '(("\\(define-global!\\)\\>" 1 font-lock-keyword-face)
;;                                     ("\\(define-core!\\)\\>" 1 font-lock-keyword-face)
;;                                     ("\\(define-primitive!\\)\\>" 1 font-lock-keyword-face)))
;; End:
