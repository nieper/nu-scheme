;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu hash-table)
  #:use-module ((srfi srfi-8))
  #:use-module ((srfi srfi-69)
                #:prefix srfi-69:)
  #:use-module (nu comparator)
  #:re-export ((default-hash . hash-by-identity)
               string-hash
               string-ci-hash
               (srfi-69:hash-table? . hash-table?)
               (srfi-69:hash-table-exists? . hash-table-contains?)
               (srfi-69:hash-table-exists? . hash-table-exists?)
               (srfi-69:hash-table-ref/default . hash-table-ref/default)
               (srfi-69:hash-table-update!/default . hash-table-update!/default)
               (srfi-69:hash-table-size . hash-table-size)
               (srfi-69:hash-table-keys . hash-table-keys)
               (srfi-69:hash-table-values . hash-table-values)
               (srfi-69:hash-table-walk . hash-table-walk)
               (srfi-69:hash-table->alist . hash-table->alist)
               (srfi-69:hash-table-merge! . hash-table-merge!)
               (srfi-69:hash-table-equivalence-function
                . hash-table-equivalence-function))
  #:replace (make-hash-table hash)
  #:export (hash-table
            hash-table-unfold
            alist->hash-table
            hash-table-exists?
            hash-table-empty?
            hash-table=?
            hash-table-mutable?
            hash-table-ref
            hash-table-set!
            hash-table-delete!
            hash-table-intern!
            hash-table-update!
            hash-table-update!/default
            hash-table-pop!
            hash-table-clear!
            hash-table-entries
            hash-table-find
            hash-table-count
            hash-table-map
            hash-table-for-each
            hash-table-walk
            hash-table-map!
            hash-table-map->list
            hash-table-fold
            hash-table-prune!
            hash-table-copy
            hash-table-empty-copy
            hash-table-union!
            hash-table-intersection!
            hash-table-difference!
            hash-table-xor!
            hash-table-equivalence-function
            hash-table-hash-function))

(define hash default-hash)

(define (make-hash-table comparator . arg*)
  (if (comparator? comparator)
      (let ((equality (comparator-equality-predicate comparator)))
        (if (memv equality (list eq? eqv? equal? string=?))
            (srfi-69:make-hash-table equality)
            (srfi-69:make-hash-table equality
                                     (make-hash-function
                                      (comparator-hash-function comparator)))))
      (srfi-69:make-hash-table comparator)))

(define (make-hash-function hash)
  (case-lambda
    ((obj)
     (hash obj))
    ((obj bound)
     (modulo (hash obj) bound))))

(define (hash-table comparator . args)
  (let ((hash-table (make-hash-table comparator)))
    (apply hash-table-set! hash-table args)
    hash-table))

(define (hash-table-unfold stop? mapper successor seed comparator . arg*)
  (let ((hash-table (apply make-hash-table comparator arg*)))
    (let loop ((seed seed))
      (if (stop? seed)
          hash-table
          (receive (key value)
              (mapper seed)
            (hash-table-set! hash-table key value)
            (loop (successor seed)))))))

(define (alist->hash-table alist comparator . arg*)
  (if (comparator? comparator)
      (let ((equality (comparator-equality-predicate comparator)))
        (if (memv equality (list eq? eqv? equal? string=?))
            (srfi-69:alist->hash-table alist equality)
            (srfi-69:alist->hash-table alist equality
                                       (make-hash-function
                                        (comparator-hash-function comparator)))))
      (srfi-69:alist->hash-table alist comparator)))

(define (hash-table-empty? hash-table)
  (zero? (srfi-69:hash-table-size hash-table)))

(define (hash-table=? value-comparator hash-table1 hash-table2)
  (let ((= (comparator-equality-predicate value-comparator)))
    (call/cc
     (lambda (return)
       (hash-table-for-each
        (lambda (key value)
          (unless (= value
                     (srfi-69:hash-table-ref hash-table2
                                             key
                                             (lambda ()
                                               (return #f))))
            (return #f)))
        hash-table1)
       (hash-table-for-each
        (lambda (key value)
          (unless (= value
                     (srfi-69:hash-table-ref hash-table1
                                             key
                                             (lambda ()
                                               (return #f))))
            (return #f)))
        hash-table2)
       #t))))

(define (hash-table-mutable? hash-table)
  #f)

(define hash-table-ref
  (case-lambda
    ((hash-table key)
     (srfi-69:hash-table-ref hash-table key))
    ((hash-table key failure)
     (srfi-69:hash-table-ref hash-table key failure))
    ((hash-table key failure success)
     (call/cc
      (lambda (return)
        (success (srfi-69:hash-table-ref hash-table
                                         key
                                         (lambda ()
                                           (return (failure))))))))))

(define (hash-table-set! hash-table . arg*)
  (let loop ((arg* arg*))
    (when (not (null? arg*))
      (srfi-69:hash-table-set! hash-table (car arg*) (cadr arg*))
      (loop (cddr arg*)))))

(define (hash-table-delete! hash-table . key*)
  (for-each
   (lambda (key)
     (srfi-69:hash-table-delete! hash-table key))
   key*))

(define (hash-table-intern! hash-table key failure)
  (hash-table-ref hash-table
                  key
                  (lambda ()
                    (let ((value (failure)))
                      (hash-table-set! hash-table key value)
                      value))))

(define hash-table-update!
  (case-lambda
    ((hash-table key updater)
     (srfi-69:hash-table-update! hash-table key updater))
    ((hash-table key updater failure)
     (srfi-69:hash-table-update! hash-table key updater failure))
    ((hash-table key updater failure success)
     (hash-table-set! hash-table
                      key
                      (updater (hash-table-ref hash-table
                                               key
                                               failure
                                               success))))))

(define (hash-table-pop! hash-table)
  (call/cc
   (lambda (return)
     (hash-table-for-each (lambda (key value)
                            (hash-table-delete! hash-table key value)
                            (return key value))
                          hash-table))))

(define (hash-table-clear! hash-table)  
  (apply hash-table-delete!
         hash-table
         (srfi-69:hash-table-keys hash-table)))

(define (hash-table-entries hash-table)
  (let ((alist (srfi-69:hash-table->alist hash-table)))
    (values (map car alist)
            (map cdr alist))))

(define (hash-table-find proc hash-table failure)
  (call/cc
   (lambda (return)
     (hash-table-for-each (lambda (key value)
                            (let ((res (proc key value)))
                              (when res
                                (return res))))
                          hash-table)
     (failure))))

(define (hash-table-count pred hash-table)
  (hash-table-fold (lambda (key value count)
                     (if (pred key value)
                         (+ count 1)
                         count))
                   0 hash-table))

(define (hash-table-map proc comparator hash-table)
  (let ((res (make-hash-table comparator)))
    (hash-table-for-each (lambda (key value)
                           (hash-table-set! res
                                            key
                                            (proc value)))
                         hash-table)
    res))

(define (hash-table-for-each proc hash-table)
  (srfi-69:hash-table-walk hash-table proc))

(define (hash-table-map! proc hash-table)
  (let ((alist (srfi-69:hash-table->alist hash-table)))
    (for-each (lambda (entry)
                (hash-table-set! hash-table
                                 (car entry)
                                 (proc (car entry)
                                       (cdr entry))))
              alist)))

(define (hash-table-map->list proc hash-table)
  (hash-table-fold (lambda (key value list)
                     (cons (proc key value)
                           list))
                   '() hash-table))

(define (hash-table-fold proc seed hash-table)
  (if (procedure? proc)
      (srfi-69:hash-table-fold hash-table proc seed)
      (srfi-69:hash-table-fold proc seed hash-table)))

(define (hash-table-prune! proc hash-table)
  (let ((keys (hash-table-fold (lambda (key value keys)
                                 (if (proc key value)
                                     (cons key keys)
                                     keys))
                               '() hash-table)))
    (for-each (lambda (key)
                (hash-table-delete! hash-table key))
              keys)))

(define hash-table-copy
  (case-lambda
    ((hash-table)
     (srfi-69:hash-table-copy hash-table))
    ((hash-table mutable?)
     (srfi-69:hash-table-copy hash-table))))

(define (hash-table-empty-copy hash-table)
  (make-hash-table (srfi-69:hash-table-equivalence-function hash-table)
                   (srfi-69:hash-table-hash-function hash-table)))

(define (hash-table-union! hash-table1 hash-table2)
  (hash-table-for-each (lambda (key value)
                         (hash-table-intern! hash-table1 key (lambda () value)))
                       hash-table2)
  hash-table1)

(define (hash-table-intersection! hash-table1 hash-table2)
  (hash-table-prune! (lambda (key value)
		       (not (srfi-69:hash-table-exists? hash-table2 key)))
		     hash-table1)
  hash-table1)

(define (hash-table-difference! hash-table1 hash-table2)
  (hash-table-prune! (lambda (key value)
		       (srfi-69:hash-table-exists? hash-table2 key))
		     hash-table1)
  hash-table1)

(define (hash-table-xor! hash-table1 hash-table2)
  (hash-table-for-each (lambda (key value)
			 (if (srfi-69:hash-table-exists? hash-table1 key)
			     (hash-table-delete! hash-table1 key)
			     (hash-table-set! hash-table1 key value)))
		       hash-table2)
  hash-table1)

(define (hash-table-hash-function hash-table)
  #f)
