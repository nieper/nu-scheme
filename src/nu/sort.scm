;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu sort)
  #:export (list-merge list-sort))

;; TODO: Implement the rest of SRFI 132.

(define (list-merge < lis1 lis2)
  (let merge ((lis1 lis1) (lis2 lis2))
    (cond
     ((null? lis1)
      lis2)
     ((null? lis2)
      lis1)
     ((< (car lis1) (car lis2))
      (cons (car lis1) (merge (cdr lis1) lis2)))
     (else
      (cons (car lis2) (merge lis1 (cdr lis2)))))))

(define (list-sort < lis)
  (let merge-sort ((lis lis))
    (cond
     ((null? lis)
      '())
     ((null? (cdr lis))
      lis)
     (else
      (let loop ((lis lis) (left '()) (right '()))
        (cond
         ((null? lis)
          (list-merge < (merge-sort left) (merge-sort right)))
         ((null? (cdr lis))
          (loop '() (cons (car lis) left) right))
         (else
          (loop (cddr lis) (cons (car lis) left) (cons (cadr lis) right)))))))))
