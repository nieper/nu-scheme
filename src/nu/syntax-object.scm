;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu syntax-object)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-28)
  #:use-module (nu list)
  #:use-module (nu vector)
  #:export (source-location?
            make-source-location
            source-location-source
            source-location-start-line
            source-location-start-column
            source-location-start-position
            source-location-end-line
            source-location-end-column
            source-location-end-position
            source-location->string
            source-location->prefix
            make-wrap wrap? wrap-marks wrap-substs empty-wrap
            mark=? antimark antimark? gen-mark
            make-syntax-object
            syntax?
            syntax-object-expression
            syntax-object-wrap
            syntax-object-marks
            syntax-object-substs
            syntax-object-source-location
            syntax-object
            unwrap-syntax
            syntax->list
            syntax-pair? syntax-car+cdr)
  #:replace ((scheme-datum->syntax . datum->syntax)
             (scheme-syntax->datum . syntax->datum))
  #:re-export ((datum->syntax . scheme-datum->syntax)
               (syntax->datum . scheme-syntax->datum)))

;;; Source locations:

(define-record-type <source-location>
  (make-source-location source
                        start-line start-column
                        end-line end-column)
  source-location?
  (source source-location-source)
  (start-line source-location-start-line)
  (start-column source-location-start-column)
  (end-line source-location-end-line)
  (end-column source-location-end-column))

(set-record-type-printer! <source-location>
  (lambda (srcloc port)
    (display (format "(make-source-location ~s ~s ~s ~s ~s)"
		     (source-location-source srcloc)
		     (source-location-start-line srcloc)
		     (source-location-start-column srcloc)
		     (source-location-end-line srcloc)
		     (source-location-end-column srcloc))
	     port)))

(define (source-location->string srcloc)
  (if srcloc
      (format "~a: ~a.~a-~a~a"
              (source-location-source srcloc)
              (source-location-start-line srcloc)
              (+ (source-location-start-column srcloc)
                 1)
              (if (= (source-location-start-line srcloc)
                     (source-location-end-line srcloc))
                  ""
                  (format "~a."
                          (source-location-end-line srcloc)))
              (+ (source-location-end-column srcloc)
                 1))
      ""))

(define (source-location->prefix srcloc)
  (let ((s (source-location->string srcloc)))
    (if (zero? (string-length s))
        s
        (string-append s ": "))))

;;; Syntax wraps:

(define-record-type <wrap>
  (make-wrap marks substs)
  wrap?
  (marks wrap-marks)
  (substs wrap-substs))

(define (empty-wrap)
  (make-wrap '() '()))

(define (antimark)
  'antimark)

(define (gen-mark)
  (gensym "mark"))

(define (mark=? m1 m2)
  (eq? m1 m2))

(define (antimark? m)
  (eq? m (antimark)))

;;; Syntax objects:

(define-record-type <syntax-object>
  (%make-syntax-object expr wrap srcloc)
  syntax?
  (expr syntax-object-expression)
  (wrap syntax-object-wrap)
  (srcloc syntax-object-source-location))

(set-record-type-printer! <syntax-object>
  (lambda (stx port)
    (display "#<syntax " port)
    (write (scheme-syntax->datum stx) port)
    (write-char #\> port)))

(define (syntax-object-marks stx)
  (wrap-marks (syntax-object-wrap stx)))

(define (syntax-object-substs stx)
  (wrap-substs (syntax-object-wrap stx)))

(define (make-syntax-object expr wrap srcloc)
  (%make-syntax-object expr
                       (or wrap (empty-wrap))
                       srcloc))

(define scheme-datum->syntax
  ;; XXX: What to do when datum is already a syntax object?
  (case-lambda
    ((ctx datum)
     (scheme-datum->syntax ctx
                           datum
                           (and ctx (syntax-object-source-location ctx))))
    ((ctx datum srcloc)
     (make-syntax-object datum
                         (and ctx (syntax-object-wrap ctx))
                         srcloc))))

;; FIXME: Handle cyclic data structures.
(define (scheme-syntax->datum stx)
  (cond
   ((syntax? stx)
    (scheme-syntax->datum (syntax-object-expression stx)))
   ((vector? stx)
    (vector-map scheme-syntax->datum stx))
   ((pair? stx)
    (cons (scheme-syntax->datum (car stx))
          (scheme-syntax->datum (cdr stx))))
   (else
    stx)))

;;; Unwrapping syntax objects:

(define (syntax-object stx marks substs srcloc)
  (if (syntax? stx)
      (make-syntax-object (syntax-object-expression stx)
                          (join-wraps marks substs (syntax-object-wrap stx))
                          (syntax-object-source-location stx))
      (make-syntax-object stx
                          (make-wrap marks substs)
                          srcloc)))

(define (join-wraps m1* s1* wrap)
  (let ((m2* (wrap-marks wrap))
        (s2* (wrap-substs wrap)))
    (if (and (not (null? m1*))
             (not (null? m2*))
             (antimark? (car m2*)))
        (make-wrap (append/cancel m1* m2*)
                   (append/cancel s1* s2*))
        (make-wrap (append m1* m2*)
                   (append s1* s2*)))))

(define (append/cancel l1 l2)
  (let loop ((l1 l1))
    (let ((x (car l1))
          (l1 (cdr l1)))
      (if (null? l1)
          (cdr l2)
          (cons x (loop l1))))))

(define (unwrap-syntax stx)
  (let ((expr (syntax-object-expression stx))
        (srcloc (syntax-object-source-location stx)))
    (cond
     ((pair? expr)
      (let ((wrap (syntax-object-wrap stx)))
        (let ((marks (wrap-marks wrap))
              (substs (wrap-substs wrap)))
          (cons
           (syntax-object (car expr)
                          marks substs
                          srcloc)
           (if (syntax? (cdr expr))
               (syntax-object (cdr expr)
                              marks substs
                              srcloc)
               (make-syntax-object (cdr expr)
                                   wrap
                                   srcloc))))))
     ((vector? expr)
      (let ((wrap (syntax-object-wrap stx)))
        (vector-map
         (lambda (element)
           (syntax-object element
                          (wrap-marks wrap)
                          (wrap-substs wrap)
                          srcloc))
         expr)))
     ((null? expr)
      '())
     (else
      stx))))

;; TODO: Handle circular lists.
(define (syntax->list stx)
  (let ((expr (syntax-object-expression stx)))
    (cond ((pair? expr)
           (let ((expr (unwrap-syntax stx)))
             (cons (car expr)
                   (syntax->list (cdr expr)))))
          ((null? expr)
           '())
          (else
           stx))))

(define (syntax-pair? stx)
  (pair? (syntax-object-expression stx)))

(define (syntax-car+cdr stx)
  (car+cdr (unwrap-syntax stx)))
