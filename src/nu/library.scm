;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu library)
  #:use-module (nu comparator)
  #:use-module (nu hash-table)
  #:use-module (nu error)
  #:export ())

(define (make-library-name-comparator)
  (make-list-comparator (make-default-comparator)
			list?
			null?
			car
			cdr))

(define (make-library-collection)
  (let ((hash-table (make-hash-table (make-library-name-comparator))))
    (case-lambda
      (()
       hash-table)
      ((lib)
       (hash-table-set! hash-table (library-name lib) lib)))))

(define (library-find name)
  (hash-table-ref/default ((current-library-collection))
			  name
			  #f))

(define (library-find/die name)
  (or (library-find name)
      (error "library `~s' not found" name)))

;;; Libraries:

(define-record-type <library>
  (make-library name imports visits invokes subst store visit invoke visible?)
  library?
  (name library-name)
  (imports library-imports)
  (visits library-visits)
  (invokes library-invokes)
  (subst library-subst)
  (store library-store)
  (visit library-visit library-set-visit!)
  (invoke library-invoke library-set-invoke!)
  (visible? library-visible?))


(define (library-install! name imports visits invokes subst
			  store visit invoke visible?)
  (let ((imports (map library-find/die imports))
	(visits (map library-find/die visits))
	(invokes (map library-find invokes)))
    (when (library-find name)
      (error "library `~s' is already installed" name))
    (let ((lib (make-library name imports visits invokes subst store
			     visit invoke visible?)))
      (library-install lib))))

(define (library-visit! lib)
  (let ((visit (library-visit lib)))
    (when (procedure? visit)
      (library-set-visit!
       lib
       (lambda ()
	 (error "circular import of library `~s' at visit-time"
		(library-name lib))))))

  )

;;; Default library collection:

(define current-library-collection
  (make-parameter (make-library-collection)))
