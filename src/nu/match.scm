;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu match)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs exceptions)
  #:use-module (nu list)
  #:export (match ->)
  #:re-export (guard unquote))

(define-syntax ->
  (lambda (stx)
    (syntax-case stx ()
      (k
       #'(raise (syntax-violation '-> "misplaced aux keyword" k))))))

(define-syntax match
  (lambda (stx)
    (syntax-case stx ()
      ((_ expr clause* ... clause)
       #`(let loop ((e expr))
           (let ((failure (lambda ()
                            (if #f #f))))
             #,(let loop ((clause #'clause)
                          (clause* (reverse! #'(clause* ...))))
                 (if (null? clause*)
                     (gen-clause clause)
                     #`(let ((failure (lambda ()
                                        #,(gen-clause clause))))
                         #,(loop (car clause*) (cdr clause*)))))))))))

(eval-when (expand load eval)

  (define (ellipsis-identifier? e)
    (and (identifier? e)
         (free-identifier=? e #'(... ...))))

  (define (gen-clause clause)
    (let*-values (((pattern guard-expr body)
                   (parse-clause clause))
                  ((matcher vars catas)
                   (gen-matcher #'e pattern '() '())))
      (with-syntax ((((x e) ...) vars)
                    (((y (cata var* ...) lvl) ...) catas))
        (with-syntax (((c ...) (generate-temporaries #'(cata ...))))
          (matcher (lambda ()
                     #`(let ((c cata) ...)
                         (let ((x e) ...)
                           (if #,guard-expr
                               (let*-values (((var* ...) (mapn lvl c y)) ...)
                                 #,@body)
                               (failure))))))))))

  (define (parse-clause clause)
    (syntax-case clause (guard)
      ((pattern (guard guard-expr) body body* ...)
       (values #'pattern #'guard-expr #'(body body* ...)))
      ((pattern body body* ...)
       (values #'pattern #'#t #'(body body* ...)))))

  (define (gen-matcher e pattern vars catas)
    (with-syntax ((e e))
      (syntax-case pattern (-> unquote)
        (,(cata -> var* ...)
         (every identifier? #'(var* ...))
         (with-syntax (((var) (generate-temporaries '(#f))))
           (values (lambda (k)
                     (k))
                   (cons #'(var e) vars)
                   (cons #'(var (cata var* ...) 0) catas))))
        (,(var* ...)
         (every identifier? #'(var* ...))
         (with-syntax (((var) (generate-temporaries '(#f))))
           (values (lambda (k)
                     (k))
                   (cons #'(var e) vars)
                   (cons #'(var (loop var* ...) 0) catas))))
        (,var
         (identifier? #'var)
         (values (lambda (k)
                   (k))
                 (cons #'(var e) vars)
                 catas))
        ((x ::: y* ... . ,z)
         (ellipsis-identifier? #':::)
         (gen-ellipsis #'e #'x #'(y* ...) #',z vars catas))
        ((x ::: y* ... . z)
         (ellipsis-identifier? #':::)
         (gen-ellipsis #'e #'x #'(y* ...) #'z vars catas))
        ((x . y)
         (with-syntax (((car-e cdr-e) (generate-temporaries '(#f #f))))
           (let*-values (((car-matcher vars catas)
                          (gen-matcher #'car-e #'x vars catas))
                         ((cdr-matcher vars catas)
                          (gen-matcher #'cdr-e #'y vars catas)))
             (values (lambda (k)
                       #`(if (pair? e)
                             (let ((car-e (car e))
                                   (cdr-e (cdr e)))
                               #,(car-matcher (lambda ()
                                                (cdr-matcher k))))
                             (failure)))
                     vars
                     catas))))
        (#(x ...)
         (let*-values (((matcher vars catas)
                        (gen-matcher #'e #'(x ...) vars catas)))
           (values (lambda (k)
                     #`(if (vector? e)
                           (let ((e (vector->list e)))
                             #,(matcher k))))
                   vars
                   catas)))
        (x
         (values (lambda (k)
                   #`(if (equal? e 'x)
                         #,(k)
                         (failure)))
                 vars
                 catas)))))

  (define (gen-ellipsis e x y* z vars catas)
    (with-syntax ((e e) (x x) ((y* ...) y*) (z z))
      (let ((l (length #'(y* ...))))
        (with-syntax ((l l)
                      ((head-e tail-e) (generate-temporaries '(#f #f))))
          (let*-values (((head-matcher vars catas)
                         (gen-map #'head-e #'x vars catas))
                        ((tail-matcher vars catas)
                         (gen-matcher* #'tail-e #'(y* ...) #'z vars catas)))
            (values (lambda (k)
                      #`(let ((n (length+ e)))
                          (if (and n (>= n l))
                              (receive (head-e tail-e)
                                  (split-at! e (- n l))
                                #,(head-matcher (lambda ()
                                                  (tail-matcher k))))
                              (failure))))
                    vars
                    catas))))))

  (define (gen-map e pattern vars catas)
    (receive (matcher inner-vars inner-catas)
        (gen-matcher #'f pattern '() '())
      (with-syntax ((e e)
                    ((loop) (generate-temporaries '(#f)))
                    ((f* ...) (generate-temporaries inner-vars))
                    (((x* e*) ...) inner-vars)
                    (((var (cata var* ...) level) ...) inner-catas))
        (values (lambda (k)
                  #`(let loop ((e (reverse e))
                               (f* '())
                               ...)
                      (if (null? e)
                          #,(k)
                          (let ((f (car e)))
                            #,(matcher (lambda ()
                                         #`(loop (cdr e)
                                                 (cons e* f*)
                                                 ...)))))))
                (append #'((x* f*) ...) vars)
                (append #'((var (cata var* ...) (+ level 1)) ...) catas)))))

  (define (gen-matcher* e pattern* tail vars catas)
    (let loop ((e e) (pattern* pattern*) (vars vars) (catas catas))
      (with-syntax ((e e))
        (if (null? pattern*)
            (syntax-case tail ()
              (() (values (lambda (k)
                            #`(if (null? e)
                                  #,(k)
                                  (failure)))
                          vars
                          catas))
              (tail (gen-matcher #'e #'tail vars catas)))
            (with-syntax (((car-e cdr-e) (generate-temporaries '(#f #f))))
              (let*-values (((car-matcher vars catas)
                             (gen-matcher #'car-e (car pattern*) vars catas))
                            ((cdr-matcher vars catas)
                             (loop #'cdr-e (cdr pattern*) vars catas)))
                (values (lambda (k)
                          #`(receive (car-e cdr-e)
                                (car+cdr e)
                              #,(car-matcher (lambda ()
                                               (cdr-matcher k)))))
                        vars
                        catas))))))))

(define (mapn n proc l)
  (let loop ((n n) (l l))
    (if (zero? n)
        (proc l)
        (let ((n (- n 1)))
          (map (lambda (x)
                 (loop n x))
               l)))))
