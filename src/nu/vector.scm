;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu vector)
  #:export (vector-map vector-fold)
  #:replace (vector-for-each))

;; TODO: More than one vector argument.
(define (vector-map proc vec)
  (let* ((len (vector-length vec))
	 (result (make-vector len)))
    (do ((i 0 (+ i 1)))
	((= i len) result)
      (vector-set! result i (proc (vector-ref vec i))))))

;; TODO: More than one vector argument.
(define (vector-for-each proc . vec*)
  (let* ((len (apply min (map vector-length vec*))))
    (do ((i 0 (+ i 1)))
        ((= i len))
      (apply proc (map (lambda (vec)
                         (vector-ref vec i))
                       vec*)))))

;; TODO: More than one vector argument.
(define (vector-fold proc seed vec)
  (let ((len (vector-length vec)))
    (let loop ((seed seed) (i 0))
      (if (= i len)
	  seed
	  (loop (proc seed (vector-ref vec i)) (+ i 1))))))

;;; TODO: Implement the rest of the R7RS vector procedures.
