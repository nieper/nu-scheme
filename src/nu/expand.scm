;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu expand)
  #:use-module (srfi srfi-8)
  #:use-module (nu list)
  #:use-module (nu error)
  #:use-module (nu vector)
  #:use-module (nu syntax-object)
  #:use-module (nu identifier)
  #:use-module (nu syntax-case)
  #:use-module (nu expression)
  #:export (expand expand-body expand-r4rs add-subst add-subst*))

;; Libraries and implicit imports.  How to handle this.
;; Idea: In the normal level, we just need libraries expanded.
;; In the meta-level, we need libraries evaluated.
;; We need to collect the used libraries...
;; There has to be a current collector... when expanding: what to visit...
;; rtc collector: what to visit ; vtc: what to invoke.

(define (add-mark mark stx)
  (syntax-object stx (list mark) (list (shift)) #f))

(define (add-subst subst stx)
  (if subst
      (syntax-object stx '() (list subst) #f)
      stx))

(define (add-subst* subst stx*)
  ;; XXX: Can subst* be a list together with proc?
  (map (lambda (stx)
         (add-subst subst stx))
       stx*))

(define (self-evaluating? stx)
  (let ((datum (syntax-object-expression stx)))
    (or (boolean? datum)
        (number? datum)
        (char? datum)
        (string? datum)
        (vector? datum))))

(define (expand-macro transformer stx rib)
  (let ((expr
         (let ((mark (gen-mark)))
           (let loop ((out (transformer (add-mark (antimark) stx))))
             (cond ((syntax? out)
                    (let ((marks (syntax-object-marks out))
                          (substs (syntax-object-substs out)))
                      (make-syntax-object
		       (syntax-object-expression out)
		       (if (and (pair? marks)
				(antimark? (car marks)))
			   (make-wrap (cdr marks)
				      (cdr substs))
			   (make-wrap (cons mark marks)
				      (let ((substs
					     (cons (shift) substs)))
					(if rib
					    (cons rib substs)
					    substs))))
		       (syntax-object-source-location out))))
                   ((pair? out)
                    (cons (loop (car out)) (loop (cdr out))))
                   ((vector? out)
                    (vector-map loop out))
                   ((symbol? out)
                    (error "encountered raw symbol in macro output" stx out))
                   (else
                    out))))))
    (syntax-object expr '() '() (syntax-object-source-location stx))))

(define (syntax-type stx rib kwds)
  (syntax-case stx ()
    (id
     (identifier? #'id)
     (let* ((label (identifier->label #'id))
            (binding (label->binding label))
            (type (binding-type binding)))
       (case type
         ((macro macro-parameter)
          (syntax-type (expand-macro (binding-value binding)
                                     stx
                                     rib)
                       rib
                       (cons #'id kwds)))
         ((undefined lexical global primitive displaced-lexical)
          (values type (binding-value binding) stx kwds))
         (else
          (values 'other #f stx kwds)))))
    ((id . _)
     (identifier? #'id)
     (let* ((label (identifier->label #'id))
            (binding (label->binding label))
            (type (binding-type binding)))
       (case type
         ((macro macro-parameter)
          (syntax-type (expand-macro (binding-value binding)
                                     stx
                                     rib)
                       rib
                       (cons #'id kwds)))
         ((core
           begin
           define
           define-syntax
           define-module
           local-syntax
           syntax-parameterize
           import
           import-only)
          (values type (binding-value binding) stx kwds))
         (else
          (values 'call #f stx kwds)))))
    ((_ . _)
     (values 'call #f stx kwds))
    (x
     (self-evaluating? #'x)
     (values 'constant (syntax->datum #'x) stx kwds))
    (_
     (values 'other #f stx kwds))))

(define (expand expr)
  (expand-expression (add-subst (global-environment) expr)))

(define (expand-expression stx)
  (let* ((srcloc
	  (syntax-object-source-location stx))
	 (expr
	  (receive (type value expr kwds)
	      (syntax-type stx #f '())
	    (case type
	      ((begin)
	       `(begin ,@(expand-expression* (parse-begin expr #f))))
	      ((lexical)
	       `,value)
	      ((core)
	       (value expr))
	      ((primitive)
	       `,value)
	      ((constant)
	       ``,value)
	      ((call)
	       (expand-application expr))
	      ((define-syntax)
	       (parse-define-syntax expr)
	       (error "invalid context for syntax definition" expr))
	      ((undefined)
	       `(error "variable `~s' is not bound"
		       `,srcloc
		       `,(identifier->symbol stx)))
	      ((displaced-lexical)
	       (error "identifier out of context" expr))
	      (else
	       (error "syntax error: ~s" expr))))))
    `(annotation ,srcloc
                 ,expr)))

(define (expand-expression* stx*)
  (define (proc stx tail)
    (if (procedure? stx)
        (stx proc tail)
        (cons (expand-expression stx) tail)))
  (reverse! (fold proc '() stx*)))

;; TODO: Positions and reloading of files for showing errors!

(define (expand-application app)
  (syntax-case app ()
    ((operator operand* ...)
     `(,(expand-expression #'operator)
       ,@(expand-expression* #'(operand* ...))))
    (_
     (error "invalid application syntax" app))))

(define (expand-r4rs program)
   ;; TODO
  (expand-expression
   (syntax-object #`(begin . #,(datum->syntax #'_ program)) '() '() #f)))

;; Reverses the list of inits.
(define (expand-inits inits)
  (define (proc init tail)
    (if (procedure? init)
	(init proc tail)
	(cons (expand-expression init) tail)))
  (fold proc '() inits))

(define (expand-body outer-stx body)
  (let ((rib (make-rib)))
    (receive (body vars inits labels kwds)
	(expand-internal (add-subst* rib body)
			 '() '() '() '() rib #f)
      (if (null? body)
	  (error "no expressions in body" outer-stx))
      (let* ((inits (expand-inits inits))
	     (body (expand-expression* body)))
	(for-each label-kill! labels)
	`(annotation ,outer-stx
	   (letrec* ,(map list (reverse! vars) inits) . ,body))))))

(define (expand-internal body vars inits labels kwds rib top?)
  (let loop ((body body) (vars vars) (inits inits) (labels labels) (kwds kwds))
    (if (null? body)
	(values '() vars inits labels kwds)
	(let ((expr (car body)))
	  (receive (type value expr kwds)
	      (syntax-type expr rib kwds)
	    (case type
	      ((begin)
	       (let ((expr* (parse-begin expr #t)))
		 (loop (append expr* (cdr body))
		       vars inits labels kwds)))
	      #; ; TODO
	      ((define-syntax)
	       (receive (keyword transformer)
		   (parse-define-syntax expr)
		 (when (bound-identifier-member? keyword kwds)
		   (error "cannot redefine keyword" keyword))
		 (let* ((binding
			 (make-binding (if value 'macro-parameter 'macro)
				       (eval-transformer transformer
							 (expand-meta transformer))))
			(label
			 (make-label binding (current-meta-level))))
		   ;; TODO: Check whether an enclosing local-syntax
		   ;; binding for id is being overwritten.
		   (rib-extend! rib keyword label)
		   (loop (cdr body) vars inits (cons label labels) kwds))))	      
	      (else
	       (if top?
		   (loop (cdr body)
			 (cons (make-variable 'dummy/body) vars)
			 (cons expr inits)
			 labels
			 kwds)
		   (values (cons expr (cdr body))
			   vars inits labels kwds)))))))))

;;; Parsers:

(define (parse-begin stx empty-allowed?)
  (syntax-case stx ()
    ((_ expr* ...)
     (or empty-allowed? (not (null? #'(expr* ...))))
     #'(expr* ...))
    (_
     (error "invalid begin syntax" stx))))

(define (parse-define-syntax stx)
  (syntax-case stx ()
    ((_ keyword transformer)
     (identifier? #'keyword)
     (values #'keyword #'transformer))
    (_
     (error "invalid define-syntax syntax" stx))))
