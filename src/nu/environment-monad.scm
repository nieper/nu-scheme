;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu environment-monad)
  #:export (make-environment-variable
	    make-environment
	    environment-ref
	    environment-update
	    environment-update!
	    environment-copy
	    make-computation
	    return
	    sequence
	    run
	    ask
	    local
	    fn
	    with
	    with!
	    forked
	    default-computation)
  #:replace (bind)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-111)
  #:use-module (nu hash-table)
  #:use-module (nu comparator)
  #:use-module (nu mapping))

(define make-environment-variable
  (let ((count -1))
    (lambda ()
      (set! count (+ count 1))
      count)))

(define variable-comparator
  (make-default-comparator))

(define default-computation
  (make-environment-variable))

(define-record-type <environment>
  (%make-environment global local)
  environment?
  (global environment-global)
  (local environment-local))

(define (make-environment)
  (%make-environment (hash-table variable-comparator)
		     (mapping variable-comparator)))

(define (environment-ref env var)
  (mapping-ref (environment-local env)
	       var
	       (lambda ()
		 (hash-table-ref/default (environment-global env) var #f))
	       unbox))

(define (environment-update env var val)
  (%make-environment (environment-global env)
		     (mapping-set (environment-local env) var (box val))))

(define (environment-update! env var val)
  (mapping-ref (environment-local env)
	       var
	       (lambda ()
		 (hash-table-set! (environment-global env) var val))
	       (lambda (cell)
		 (set-box! cell val))))

(define (environment-copy env)
  (let ((global (hash-table-copy (environment-global env) #t)))
    (mapping-for-each (lambda (var cell)
			(hash-table-set! global var (unbox cell)))
		     (environment-local env))
    (%make-environment global (mapping variable-comparator))))

(define (execute computation env)
  (let ((coerce (if (procedure? computation)
		    values
		    (or (environment-ref env default-computation)
			(error "not a computation" computation)))))
    ((coerce computation) env)))

(define (make-computation proc)
  (lambda (env)
    (proc (lambda (c) (execute c env)))))

(define (run computation)
  (execute computation (make-environment)))

(define (return . args)
  (make-computation
   (lambda (compute)
     (apply values args))))

(define (sequence a . a*)
  (make-computation
   (lambda (compute)
     (let loop ((a a) (a* a*))
       (if (null? a*)
	   (compute a)
	   (begin
	     (compute a)
	     (loop (car a*) (cdr a*))))))))

(define (bind a . f*)
  (make-computation
   (lambda (compute)
     (let loop ((a a) (f* f*))
       (if (null? f*)
	   (compute a)
	   (loop (call-with-values
		     (lambda () (compute a))
		   (car f*))
		 (cdr f*)))))))

(define (ask)
  (lambda (env)
    env))

(define (local updater computation)
  (lambda (env)
    (computation (updater env))))

(define-syntax fn
  (syntax-rules ()
    ((_ ((id var) ...) expr ... computation)
     (%fn ((id var) ...) () expr ... computation))))

(define-syntax %fn
  (syntax-rules ()
    ((_ () ((id var tmp) ...) expr ... computation)
     (let ((tmp var) ...)
       (bind (ask) (lambda (env)
		     (let ((id (environment-ref env tmp)) ...)
		       expr ...
		       computation)))))
    ((_ ((id var) . rest) (p ...) expr ... computation)
     (%fn rest (p ... (id `var tmp)) expr ... computation))
    ((_ (id . rest) (p ...) expr ... computation)
     (%fn rest (p ... (id 'id tmp)) expr ... computation))))

(define-syntax with
  (syntax-rules ()
    ((_ ((var val) ...) a* ... a)
     (%with ((`var val) ...) () () a* ... a))))

(define-syntax %with
  (syntax-rules ()
    ((_ () ((var u val v) ...) ((a b) ...))
     (let ((u var) ... (v val) ... (b a) ...)
       (local (lambda (env)
		(let* ((env (environment-update env u v)) ...)
		  env))
	 (sequence b ...))))
    ((_ ((var val) . rest) (p ...) () a* ...)
     (%with rest (p ... (var u val v)) () a* ...))
    ((_ () p* (q ...) a . a*)
     (%with () p* (q ... (a b)) . a*))))

(define-syntax with!
  (syntax-rules ()
    ((_ (var val) ...)
     (%with! (`var val) ... ()))))

(define-syntax %with!
  (syntax-rules ()
    ((_ ((var u val v) ...))
     (let ((u var) ... (v val) ...)
       (bind (ask) (lambda (env)
		     (environment-update! env u v) ...
		     (return (if #f #f))))))
    ((_ (var val) r ... (p ...))
     (%with! r ... (p ... (var u val v))))))

(define (forked a . a*)
  (make-computation
   (lambda (compute)
     (let loop ((a a) (a* a*))
       (if (null? a*)
	   (compute a)
	   (begin
	     (compute (local (lambda (env) (environment-copy env)) a))
	     (loop (car a*) (cdr a*))))))))

