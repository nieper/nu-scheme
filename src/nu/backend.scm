;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu backend)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-28)
  #:use-module (rnrs exceptions)
  #:use-module (nu comparator)
  #:use-module (nu error)
  #:use-module (nu hash-table)
  #:use-module (nu list)
  #:use-module (nu io)
  #:use-module (nu match)
  #:export (output-code))

;;; From (nu expression)

(define (variable? name)
  (symbol? name))

(define (variable-callee-save? name)
  (and (memq name '(c1 c2 c3 c4)) #t))

(define (variable->datum name)
  name)

(define (primitive-function op)
  (case op
    ((fx+) "NU_FX_ADD")
    ((fxzero?) "NU_FX_ZERO_P")))

(define (c-name var)
  (case var
    ((unspecified)
     "NU_UNSPECIFIED")
    ((c1)
     "NU_REG_1")
    ((c2)
     "NU_REG_2")
    ((c3)
     "NU_REG_3")
    ((c4)
     "NU_REG_4")
    (else
     (symbol->string (variable->datum var)))))

(define (format-initializer vals)
  (let ((p (open-output-string)))
    (write-string "{" p)
    (do ((vals vals (cdr vals)) (sep "" ", "))
	((null? vals)
	 (write-string "}" p)
	 (get-output-string p))
      (write-string sep p)
      (display (car vals) p))))

(define port (make-parameter #f))
(define indent (make-parameter 0)) ;; TODO: Indenting not implemented yet.

(define (output-code prog filename)
  (emit-with-output-file filename
    (lambda ()
      (emit-comment "A scheme program, compiled by Nu Scheme.")
      (emit-newline)
      (emit-include "libnuscheme.h")
      (emit-include "stdio.h") ;;XXX
      (emit-newline)
      (emit-declarations prog)
      (emit-newline)
      (emit-definitions prog)
      (emit-newline)
      ;; TODO: Emit initializations!
      )))

(define (emit-with-output-file filename thunk)
  (with-exception-handler
      (lambda (exc)
	(guard (exc ((file-error? exc) #f))
	  (delete-file filename))
	(raise exc))
    (lambda ()
      (call-with-output-file filename
	(lambda (p)
	  (parameterize ((port p))
	    (thunk)))))))

(define (emit text . args)
  (write-string (apply format text args) (port)))

(define (emit-indent)
  (emit (make-string (indent) #\space)))

(define (emit-newline)
  (newline (port)))

(define (emit-comment txt)
  (emit "/* ~a */" txt)
  (emit-newline))

(define (emit-include name)
  (emit "#include <~a>" name)
  (emit-newline))

(define (emit-begin-function name . params)
  (emit "void ~a " name)
  (emit-parameter-list params)
  (emit-newline)
  (emit "{")
  (emit-newline))

(define (emit-end-function)
  (emit "}")
  (emit-newline))

(define (emit-parameter-list params)
  (emit "(")
  (do ((params params (cdr params)) (sep "" ", "))
      ((null? params)
       (emit ")"))
    (emit sep)
    (emit "NuValue ~a" (car params))))

(define (emit-call name . args)
  (emit "~a " name)
  (emit-argument-list args)
  (emit ";")
  (emit-newline))

(define (emit-argument-list args)
  (emit "(")
  (do ((args args (cdr args)) (sep "" ", "))
      ((null? args)
       (emit ")"))
    (emit sep)
    (emit "~a" (car args))))

(define (emit-declarations prog)
  (for-each emit-declaration prog))

(define (emit-declaration def)
  (match def
    ((define (,f ,a* ...) . ,_)
     (guard (every variable? (cons f a*)))
     (emit-function-declaration (c-name f)
				(filter-map (lambda (a)
					      (and (not (variable-callee-save? a))
						   (c-name a)))
					    a*)))
    ((define ,x (make-label . ,_))
     (emit-label-declaration (c-name x)))
    ((define ,x ',datum)
     (guard (variable? x))
     (emit-datum-declaration (c-name x) datum))))

(define (emit-function-declaration name params)
  (unless (and (>= (string-length name) 3)
	       (string=? (substring name 0 3) "nu_"))
    (emit "static "))
  (emit "noreturn void ~a " name)
  (emit-parameter-list params)
  (emit ";")
  (emit-newline))

(define (emit-label-declaration name)
  (emit "static NuLabel ~a;~%" name))

(define (emit-datum-declaration name datum)
  (let ((count 0)
	(labels (make-hash-table (make-eq-comparator))))
    (let loop ((count count) (datum datum))
      (unless (hash-table-ref/default labels datum #f)
	(hash-table-set! labels datum count)
	(emit "static ")
	(let ((name (if (zero? count) name (format "~a_~a" name count))))
	  (cond
	   ((integer? datum)
	    ;; FIXME: Use macro that decides whether datum is a fixnum or bignum.
	    (emit "NuValue ~a;~%" name))))))))

(define (emit-definitions prog)
  (for-each emit-definition prog))

(define (emit-definition def)
  (match def
    ((define (,f ,a* ...) ,expr)
     (guard (every variable? (cons f a*)))
     (emit-function-definition (c-name f)
			       (filter-map (lambda (a)
					     (and (not (variable-callee-save? a))
						  (c-name a)))
					   a*)
			       expr))
    ((define ,x (make-label ,num-slots ,label-slots ,self-slot
			    ,entry0 ,entry1 ,entry2 ,entry3 ,entry))
     (guard (variable? x))
     (emit-label-definition (c-name x)
			    num-slots label-slots self-slot
			    entry0 entry1 entry2 entry3 entry))
    ((define ,x ',datum)
     (guard (variable? x))
     (emit-datum-definition (c-name x) datum))))

(define (emit-function-definition name params expr)
  (unless (and (>= (string-length name) 3)
	       (string=? (substring name 0 3) "nu_"))
    (emit "static "))
  (emit "void ~a " name)
  (emit-parameter-list params)
  (emit-newline)
  (emit "{~%")
  (emit-statement expr)
  (emit "}~%")
  (emit-newline))

(define (emit-label-definition name num-slots label-slots self-slot
			       entry0 entry1 entry2 entry3 entry)
  (emit "static NuLabel ~a = NU_LABEL_INITIALIZER (~a, ~a, ~a, ~a, ~a, ~a, ~a, ~a);~%"
	name num-slots label-slots self-slot
	(if entry0 (c-name entry0) "0")
	(if entry1 (c-name entry1) "0")
	(if entry2 (c-name entry2) "0")
	(if entry3 (c-name entry3) "0")
	(if entry (c-name entry) "0")))

(define (emit-datum-definition name datum)
  (let ((count 0)
	(labels (make-hash-table (make-eq-comparator))))
    (let loop ((count count) (datum datum))
      (unless (hash-table-ref/default labels datum #f)
	(hash-table-set! labels datum count)
	(emit "static ")
	(let ((name (if (zero? count) name (format "~a_~a" name count))))
	  (cond
	   ((integer? datum)
	    (emit "NuValue ~a = NU_FIXNUM_INITIALIZER (~aLL);~%" name datum))))))))

(define (emit-statement expr) ;; expr is a statement
  (match expr
    ((if ,e ,s1 ,s2)
     (emit "if (nu_value_to_bool (~a))~%{~%" (c-name e))
     (emit-statement s1)
     (emit "}~%{~%")
     (emit-statement s2)
     (emit "}~%"))
    ((receive (,f) (make-procedure ,n ,i ,v* ...) ,expr)
     (receive (labels vars) (split-at v* n)
       (apply emit-call "NU_MAKE_PROCEDURE" (c-name f) i
	      (append
	       (map (lambda (l) (format "{.label = &~a}" (c-name l))) labels)
	       (map (lambda (v) (format "{.var = &~a}" (c-name v))) vars))))
     (emit-statement expr))
    ((receive (,c) (make-continuation ,c1 ,cx ,a* ...) ,expr)
     (apply emit-call "NU_MAKE_CONT_PROCEDURE" (c-name c)
	    (if c1 (c-name c1) "0")
	    (if cx (c-name cx) "0")
	    (length a*) (map c-name a*))
     (emit-statement expr))
    ((receive (,v) (continuation-variable ,c ,i) ,expr)
     (emit-call "NU_CONT_VARIABLE" (c-name v) (c-name c) i)
     (emit-statement expr))
    ((receive (,x* ...) (,op ,y* ...) ,expr)
     (apply emit-call (primitive-function op) (map c-name (append x* y*)))
     (emit-statement expr))
    ((call ,a* ...)
     (apply emit-call "NU_CALL" (map c-name a*)))
    ((continuation-call ,c ,c1 ,c2 ,c3 ,c4 ,a)
     (emit-call "nu_cont_call" (c-name c) (c-name c1) (c-name c2) (c-name c3)
		(c-name c4) (c-name a)))
    ((thread-exit)

     ;; XXX
     (emit "printf (\"Result: %\" NU_PRIiFX \"\\n\", nu_value_to_int (a));")

     (emit-call "nu_thread_exit"))))
