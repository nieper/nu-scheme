;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu list)
  #:use-module (srfi srfi-1)
  #:use-module ((srfi srfi-1)
		#:select ((map . scheme-map)
			  (for-each . scheme-for-each)))
  #:re-export (xcons make-list list-tabulate cons* list-copy
                     proper-list? circular-list? dotted-list? not-pair?
                     null-list? list= circular-list iota
                     first second third fourth fifth sixth seventh eighth ninth
                     tenth
                     car+cdr take drop take-right drop-right take!
                     drop-right! split-at split-at!
                     last last-pair
                     zip unzip1 unzip2 unzip3 unzip4 unzip5 count
                     append! append-reverse append-reverse!
                     concatenate concatenate!
                     unfold fold pair-fold reduce
                     unfold-right fold-right pair-fold-right reduce-right
                     append-map append-map! map! pair-for-each filter-map
                     map-in-order filter partition remove
                     filter! partition! remove!
                     find find-tail any every list-index
                     take-while drop-while take-while!
                     span break span! break!
                     delete delete!
                     alist-cons alist-copy
                     delete-duplicates delete-duplicates!
                     alist-delete alist-delete!
                     reverse!
                     lset<= lset= lset-adjoin
                     lset-union lset-intersection lset-difference lset-xor
                     lset-diff+intersection lset-union!
                     lset-intersection! lset-difference! lset-xor!
                     lset-diff+intersection!
                     cons pair? null? car cdr set-car! set-cdr!
                     list append reverse
                     caar cadr cdar cddr
                     caaar caadr cadar caddr cdaar cdadr cddar cdddr
                     caaaar caaadr caadar caaddr cadaar cadadr caddar cadddr
                     cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
                     memq memv assq assv
                     member assoc)
  #:export (length+)
  #:replace (length map for-each))

;; This version is defined for arbitrary finite lists.
(define (length list)
 (let loop ((list list) (len 0))
   (if (pair? list)
       (loop (cdr list) (+ len 1))
       len)))

(define (length+ x)
  (let lp ((x x) (lag x) (len 0))
    (if (pair? x)
	(let ((x (cdr x))
	      (len (+ len 1)))
	  (if (pair? x)
	      (let ((x   (cdr x))
		    (lag (cdr lag))
		    (len (+ len 1)))
		(and (not (eq? x lag)) (lp x lag len)))
	      len))
	len)))

(define (map f list . lists)
  (if (null? lists)
      (let loop ((list list))
	(cond ((null? list)
	       '())
	      ((pair? list)
	       (let ((tail (cdr list))
		     (val (f (car list))))
		 (cons val (loop tail))))
	      (else
	       (f list))))
      (apply scheme-map f list lists)))

(define (for-each proc list . lists)
  (if (null? lists)
      (let loop ((list list))
	(cond ((pair? list)
	       (proc (car list))
	       (loop (cdr list)))
	      ((not (null? list))
	       (proc list))))
      (let loop ((lists (cons list lists)))
	(when (every (lambda (x) (not (null? x))) lists)
	  (apply proc (map %car lists))
	  (loop (map %cdr lists))))))

(define (%car obj)
  (if (pair? obj)
      (car obj)
      obj))

(define (%cdr obj)
  (if (pair? obj)
      (cdr obj)
      '()))
