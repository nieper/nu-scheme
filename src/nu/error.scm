;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu error)
  #:use-module (rnrs conditions)
  #:use-module (rnrs exceptions)
  #:use-module (rnrs io simple)
  #:export (error-object? file-error?
                          error-object-message error-object-irritants)
  #:replace (error))

(define-condition-type &error-object &condition make-error-object error-object?
  (message error-object-message)
  (irritants error-object-irritants))

(define (file-error? obj)
  (i/o-error? obj))

(define (error message . irritant*)
  (raise (make-error-object message irritant*)))
