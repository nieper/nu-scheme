;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu syntax-case)
  #:use-module (system syntax)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-11)
  #:use-module (nu syntax-object)
  #:use-module (nu list)
  #:use-module ((nu identifier)
                #:prefix scheme-)
  #:replace ((scheme-syntax-case . syntax-case)
             (scheme-syntax . syntax)
             (scheme-quasisyntax . quasisyntax))
  #:re-export ((syntax-case . scheme-syntax-case)
               (syntax . scheme-syntax)
               (quasisyntax . scheme-quasisyntax)))

;;; Syntax Case:

(define-syntax scheme-syntax-case
  (lambda (stx)
    (syntax-case stx ()
      ((_ expr (lit* ...) clause* ...)
       (every identifier? #'(lit* ...))
       (let ((lits #'(lit* ...))
             (clauses (reverse! #'(clause* ...)))
             (error #'(error "syntax error" e)))
         #`(let ((e expr))
             #,(if (null? clauses)
                   error
                   #`(let ((f (lambda ()
                                #,error)))
                       #,(let loop ((clause (car clauses))
                                    (clause* (cdr clauses)))
                           (if (null? clause*)
                               (gen-clause lits clause)
                               #`(let ((f (lambda ()
                                            #,(gen-clause lits clause))))
                                   #,(loop (car clause*) (cdr clause*)))))))))))))

(eval-when (expand load eval)

  (define (ellipsis? id)
    (free-identifier=? id #'(... ...)))

  (define binding-property (make-object-property))

  (define (make-pattern-variable pvar id depth)
    (let ((transformer
           (lambda (stx)
             (raise (syntax-violation 'syntax-case
                                      "reference to pattern variable outside syntax"
                                      pvar)))))
      (set! (binding-property transformer) (list id depth))
      transformer))

  (define (pattern-variable x)
    (receive (type value)
        (syntax-local-binding x)
      (binding-property value)))

  (define (gen-clause lits clause)
    (syntax-case clause ()
      ((pattern output-expr)
       (gen-output lits #'pattern #'#t #'output-expr))
      ((pattern fender output-expr)
       (gen-output lits #'pattern #'fender #'output-expr))))

  (define (gen-output lits pattern fender output-expr)
    (receive (matcher vars)
        (gen-matcher #'e lits pattern '())
      (with-syntax ((((x e l) ...) vars))
        (matcher (lambda ()
                   #`(let-syntax ((x (make-pattern-variable #'x #'e l)) ...)
                       (if #,fender
                           #,output-expr
                           (f))))))))

  (define (gen-matcher e lits pattern vars)
    (with-syntax ((e e))
      (syntax-case pattern ()
        ((x ::: y* ... . z)
         (and (identifier? #':::)
              (ellipsis? #':::))
         (let ((l (length #'(y* ...))))
           (with-syntax ((l l)
                         ((h t) (generate-temporaries '(#f #f))))
             (let*-values (((head-matcher vars) (gen-map #'h lits #'x vars))
                           ((tail-matcher vars)
                            (gen-matcher* #'t lits #'(y* ... . z) vars)))
               (values (lambda (k)
                         #`(let* ((e* (syntax->list e))
                                  (n (length+ e*)))
                             (if (and n (>= n l))
                                 (receive (h t)
                                     (split-at! e* (- n l))
                                   #,(head-matcher (lambda ()
                                                     (tail-matcher k))))
                                 (f))))
                       vars)))))
        ((x . y)
         (with-syntax (((e1 e2) (generate-temporaries '(#f #f))))
           (let*-values (((car-matcher vars)
                          (gen-matcher #'e1 lits #'x vars))
                         ((cdr-matcher vars)
                          (gen-matcher #'e2 lits #'y vars)))
             (values (lambda (k)
                       #`(if (syntax-pair? e)
                             (receive (e1 e2)
                                 (syntax-car+cdr e)
                               #,(car-matcher (lambda ()
                                                (cdr-matcher k))))
                             (f)))
                     vars))))
        ;; TODO: Vectors
        (x
         (identifier? #'x)
         (cond ((member #'x lits free-identifier=?)
                (values (lambda (k)
                          #`(if (scheme-free-identifier=? (scheme-syntax x)
                                                          e)
                                #,(k)
                                (f)))
                        vars))
               ((ellipsis? #'x)
                (raise (syntax-violation 'syntax-case
                                         "misplaced ellipsis"
                                         #'x)))
               ((free-identifier=? #'x #'_)
                (values (lambda (k)
                          (k))
                        vars))
               (else
                (values (lambda (k)
                          (k))
                        (alist-cons #'x (list #'e 0) vars)))))
        (x
         (values (lambda (k)
                   #`(if (equal? (syntax->datum e) 'x)
                         #,(k)
                         (f)))
                 vars)))))

  (define (gen-map h lits pattern vars)
    (receive (matcher inner-vars)
        (gen-matcher #'g lits pattern '())
      (with-syntax ((h h)
                    ((loop) (generate-temporaries '(#f)))
                    ((g* ...) (generate-temporaries inner-vars))
                    (((p* x* l*) ...) inner-vars))
        (values (lambda (k)
                  #`(let loop ((h (reverse h))
                               (g* '())
                               ...)
                      (if (null? h)
                          #,(k)
                          (let ((g (car h)))
                            #,(matcher (lambda ()
                                         #`(loop (cdr h)
                                                 (cons x* g*)
                                                 ...)))))))
                (fold (lambda (p l g vars)
                        (alist-cons p (list g (+ l 1)) vars))
                      vars #'(p* ...) #'(l* ...) #'(g* ...))))))

  (define (gen-matcher* e lits pattern* vars)
    (let loop ((e e) (pattern* pattern*) (vars vars))
      (with-syntax ((e e))
        (cond ((null? (syntax->datum pattern*))
               (values (lambda (k)
                         #`(if (null? e)
                               #,(k)
                               (f)))
                       vars))
              ((pair? pattern*)
               (with-syntax (((e1 e2)
                              (generate-temporaries '(#f #f))))
                 (let*-values (((car-matcher vars)
                                (gen-matcher #'e1 lits (car pattern*) vars))
                               ((cdr-matcher vars)
                                (loop #'e2 (cdr pattern*) vars)))
                   (values (lambda (k)
                             #`(receive (e1 e2)
                                   (car+cdr e)
                                 #,(car-matcher (lambda ()
                                                  (cdr-matcher k)))))
                           vars))))
              (else
               (gen-matcher #'e lits pattern* vars)))))))

;;; Syntax and Quasisyntax:

(define-syntax scheme-syntax
  (lambda (stx)
    (syntax-case stx ()
      ((_ template)
       (receive (out envs var?)
           (gen-template #'template
                         '()
                         ellipsis?
                         #f)
         out)))))

(define-syntax scheme-quasisyntax
  (lambda (stx)
    (syntax-case stx ()
      ((_ template)
       (receive (out envs var?)
           (gen-template #'template
                         '()
                         ellipsis?
                         0)
         out)))))

(eval-when (expand load eval)

  (define (gen-template tmpl envs ell? level)
    (syntax-case tmpl (scheme-quasisyntax unsyntax unsyntax-splicing)
      (#,x
       (and level (zero? level))
       (values #'x
               envs
               #t))
      (#,x
       level
       (receive (out envs var?)
           (gen-template #'x envs ell? (- level 1))
	   (if var?
	       ;; TODO: Use (car #,)!
             (values #`(list #,(gen-data 'unsyntax) #,out)
                     envs
                     #t)
             (values (gen-data tmpl)
                     envs
                     #f))))
      ((scheme-quasisyntax x)
       level
       (receive (out envs var?)
           (gen-template #'x envs ell? (+ level 1))
         (if var?
             (values #`(list #,(gen-data 'quasisyntax) #,out)
                     envs
                     #t)
             (values (gen-data tmpl)
                     envs
                     #f))))
      ((::: x)
       (and (identifier? #':::)
            (ell? #':::))
       (gen-template #'x envs
                     (lambda (id)
                       #f)
                     level))
      ((x ::: . y)
       (and (identifier? #':::)
            (ell? #':::))
       (let*-values (((output* envs vars?)
                      (syntax-case #'y ()
                        (() (values #''() envs #f))
                        (_ (gen-template #'y envs ell? level))))
                     ((output envs vars?)
                      (gen-template #'x (cons '() envs) ell? level)))
         (when (null? (car envs))
           (raise (syntax-violation 'syntax
                                    "too many ellipses following syntax template"
                                    #'x)))
         (with-syntax (((x* ...) (car envs)))
           (values #`(fold-right (lambda (x* ... stx)
                                   (cons #,output stx))
                                 #,output* x* ...)
                   (cdr envs)
                   #t))))
      ((x . y)
       (let*-values (((out1 envs var1?)
                      (gen-template #'x envs ell? level))
                     ((out2 envs var2?)
                      (syntax-case #'y ()
                        (() (values #''() envs #f))
                        (_ (gen-template #'y envs ell? level)))))
         (if (or var1? var2?)
             (values #`(cons #,out1 #,out2)
                     envs
                     #t)
             (values (gen-data tmpl)
                     envs
                     #f))))
      ;; TODO: Vectors. Unquote-splicing. Unquote in lists.
      (id
       (identifier? #'id)
       (cond ((ell? #'id)
              (raise (syntax-violation 'syntax
                                       "misplaced ellipsis in syntax template"
                                       #'id)))
             ((pattern-variable #'id) =>
              (lambda (binding)
                (values (car binding)
                        (update-envs #'id (car binding) (cadr binding) envs)
                        #t)))
             (else
              (values (gen-data #'id)
                      envs
                      #f))))
      (x
       (values (gen-data #'x)
               envs
               #f))))

  (define (gen-data x)
    #`(make-syntax-object '#,x (scheme-top-level-wrap) #f))

  (define (update-envs id x level envs)
    (let loop ((level level) (envs envs))
      (cond ((zero? level)
             envs)
            ((null? envs)
             (raise (syntax-violation 'syntax
                                      "too few ellipses following syntax template"
                                      id)))
            (else
             (let ((outer-envs (loop (- level 1) (cdr envs))))
               (cond ((member x (car envs) bound-identifier=?)
                      envs)
                     (else
                      (cons (cons x (car envs))
                            outer-envs)))))))))
