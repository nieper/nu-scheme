;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Copyright (C) John Cowan (2015). All Rights Reserved.
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use,
;;; copy, modify, merge, publish, distribute, sublicense, and/or
;;; sell copies of the Software, and to permit persons to whom the
;;; Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.

(define-module (nu comparator)
  #:use-module (srfi srfi-9)
  #:use-module ((srfi srfi-69)
                #:prefix scheme-)
  #:use-module ((guile)
                #:select ((symbol-hash . scheme-symbol-hash)))
  #:use-module ((rnrs base)
                #:select (infinite? boolean=? symbol=?))
  #:use-module (rnrs unicode)
  #:use-module (nu error)
  #:use-module (nu bytevector)
  #:replace (symbol-hash string-hash)
  #:export (comparator?
            comparator-ordered?
            comparator-hashable?
            make-comparator
            make-pair-comparator
            make-list-comparator
            make-vector-comparator
            make-eq-comparator
            make-eqv-comparator
            make-equal-comparator
            boolean-hash
            char-hash
            char-ci-hash
            number-hash
            string-ci-hash
            make-default-comparator
            default-hash
            comparator-register-default!
            comparator-type-test-predicate
            comparator-equality-predicate
            comparator-ordering-predicate
            comparator-hash-function
            comparator-test-type
            comparator-check-type
            comparator-hash
            hash-bound
            hash-salt
            =? <? >? <=? >=?
            comparator-if<=>))

(define-syntax comparator-if<=>
  (syntax-rules ()
    ((_ a b less equal greater)
     (comparator-if<=> (make-default-comparator) a b less equal greater))
    ((comparator-if<=> comparator a b less equal greater)
     (cond ((=? comparator a b) equal)
           ((<? comparator a b) less)
           (else greater)))))

(define-record-type <comparator>
  (%make-comparator type-test equality ordering hash
                    ordering? hash?)
  comparator?
  (type-test comparator-type-test-predicate)
  (equality comparator-equality-predicate)
  (ordering comparator-ordering-predicate)
  (hash comparator-hash-function)
  (ordering? comparator-ordered?)
  (hash? comparator-hashable?))

(define-syntax hash-bound
  (syntax-rules ()
    ((hash-bound) 33554432)))

(define current-hash-salt
  (make-parameter 16064047))

(define-syntax hash-salt
  (syntax-rules ()
    ((hash-salt) (current-hash-salt))))

(define (%with-hash-salt new-salt hash-func obj)
  (let ((saved-salt (current-hash-salt)))
    (dynamic-wind
      (lambda ()
	(current-hash-salt new-salt))
      (lambda ()
	(hash-func obj))
      (lambda ()
	(current-hash-salt saved-salt)))))

(define (make-comparator type-test equality ordering hash)
  (%make-comparator
    (if (eq? type-test #t) (lambda (x) #t) type-test)
    (if (eq? equality #t) (lambda (x y) (eqv? (ordering x y) 0)) equality)
    (if ordering ordering (lambda (x y) (error "ordering not supported")))
    (if hash hash (lambda (x y) (error "hashing not supported")))
    (if ordering #t #f)
    (if hash #t #f)))

(define (comparator-test-type comparator obj)
  ((comparator-type-test-predicate comparator) obj))

(define (comparator-check-type comparator obj)
  (if (comparator-test-type comparator obj)
    #t
    (error "comparator type check failed" comparator obj)))

(define (comparator-hash comparator obj)
  ((comparator-hash-function comparator) obj))

(define (%=? comparator a b)
  ((comparator-equality-predicate comparator) a b))

(define (%<? comparator a b)
  ((comparator-ordering-predicate comparator) a b))

(define (%>? comparator a b)
  (%<? comparator b a))

(define (%<=? comparator a b)
  (not (%>? comparator a b)))

(define (%>=? comparator a b)
  (not (%<? comparator a b)))

(define (=? comparator a b . objs)
  (let loop ((a a) (b b) (objs objs))
    (and (%=? comparator a b)
	 (if (null? objs) #t (loop b (car objs) (cdr objs))))))

(define (<? comparator a b . objs)
  (let loop ((a a) (b b) (objs objs))
    (and (%<? comparator a b)
	 (if (null? objs) #t (loop b (car objs) (cdr objs))))))

(define (>? comparator a b . objs)
  (let loop ((a a) (b b) (objs objs))
    (and (%>? comparator a b)
	 (if (null? objs) #t (loop b (car objs) (cdr objs))))))

(define (<=? comparator a b . objs)
  (let loop ((a a) (b b) (objs objs))
    (and (%<=? comparator a b)
	 (if (null? objs) #t (loop b (car objs) (cdr objs))))))

(define (>=? comparator a b . objs)
  (let loop ((a a) (b b) (objs objs))
    (and (%>=? comparator a b)
	 (if (null? objs) #t (loop b (car objs) (cdr objs))))))

(define (boolean-hash obj)
  (if obj (current-hash-salt) 0))

(define (char-hash obj)
  (modulo (* (current-hash-salt) (char->integer obj)) (hash-bound)))

(define (char-ci-hash obj)
  (modulo (* (current-hash-salt) (char->integer (char-foldcase obj))) (hash-bound)))

(define (number-hash obj)
  (cond
    ((nan? obj) (current-hash-salt))
    ((and (infinite? obj) (positive? obj)) (* 2 (current-hash-salt)))
    ((infinite? obj) (* (current-hash-salt) 3))
    ((real? obj) (abs (inexact->exact (round obj))))
    (else (+ (number-hash (real-part obj)) (number-hash (imag-part obj))))))

(define string-hash
  (case-lambda
    ((obj)
     (modulo (* (current-hash-salt) (scheme-string-hash obj))
             (hash-bound)))
    ((obj arg)
     (string-hash obj))))

(define string-ci-hash
  (case-lambda
    ((obj)
     (modulo (* (current-hash-salt) (scheme-string-ci-hash obj))
             (hash-bound)))
    ((obj arg)
     (string-ci-hash obj))))

(define (symbol<? a b) (string<? (symbol->string a) (symbol->string b)))

(define (symbol-hash obj)
  (modulo (* (current-hash-salt) (scheme-symbol-hash obj))
          (hash-bound)))

(define (%constructor-object-type obj)
  (cond
   ((null? obj) 0)
   ((pair? obj) 1)
   ((boolean? obj) 2)
   ((char? obj) 3)
   ((string? obj) 4)
   ((symbol? obj) 5)
   ((number? obj) 6)
   ((vector? obj) 7)
   ((bytevector? obj) 8)
   (else
    (registered-index obj))))

(define default-hash
  (case-lambda
    ((obj)
     (let ((type (%constructor-object-type obj)))
       (case (%constructor-object-type obj)
         ((0) 0)
         ((1) ((make-pair-hash (make-default-comparator)
                               (make-default-comparator))
               obj))
         ((2) (boolean-hash obj))
         ((3) (char-hash obj))
         ((4) (string-hash obj))
         ((5) (symbol-hash obj))
         ((6) (number-hash obj))
         ((7) ((make-vector-hash (make-default-comparator)
                                 vector? vector-length vector-ref)
               obj))
         ((8) ((make-vector-hash (make-default-comparator)
                                 bytevector? bytevector-length bytevector-u8-ref) obj))
         (else (comparator-hash (registered type) obj)))))
    ((obj arg)
     (default-hash obj))))

(define (make-eq-comparator)
  (make-comparator #t eq? #f default-hash))

(define (make-eqv-comparator)
  (make-comparator #t eqv? #f default-hash))

(define (make-equal-comparator)
  (make-comparator #t equal? #f default-hash))

(define (make-hasher)
  (let ((result (current-hash-salt)))
    (case-lambda
     (() result)
     ((n) (set! result (+ (modulo (* result 33) (hash-bound)) n))
          result))))

(define (make-pair-comparator car-comparator cdr-comparator)
  (define (make-pair-type-test car-comparator cdr-comparator)
    (lambda (obj)
      (and (pair? obj)
	   (comparator-test-type car-comparator (car obj))
	   (comparator-test-type cdr-comparator (cdr obj)))))
  (make-comparator
     (make-pair-type-test car-comparator cdr-comparator)
     (make-pair=? car-comparator cdr-comparator)
     (make-pair<? car-comparator cdr-comparator)
     (make-pair-hash car-comparator cdr-comparator)))

(define (make-pair=? car-comparator cdr-comparator)
  (lambda (a b)
    (and ((comparator-equality-predicate car-comparator) (car a) (car b))
	 ((comparator-equality-predicate cdr-comparator) (cdr a) (cdr b)))))

(define (make-pair<? car-comparator cdr-comparator)
  (lambda (a b)
    (if (=? car-comparator (car a) (car b))
	(<? cdr-comparator (cdr a) (cdr b))
	(<? car-comparator (car a) (car b)))))

(define (make-pair-hash car-comparator cdr-comparator)
  (lambda (obj)
    (let ((acc (make-hasher)))
      (acc (comparator-hash car-comparator (car obj)))
      (acc (comparator-hash cdr-comparator (cdr obj)))
      (acc))))

(define (make-list-comparator element-comparator type-test empty? head tail)
  (define (make-list-type-test element-comparator type-test empty? head tail)
    (lambda (obj)
      (and
       (type-test obj)
       (let ((elem-type-test (comparator-type-test-predicate element-comparator)))
	 (let loop ((obj obj))
	   (cond
            ((empty? obj) #t)
            ((not (elem-type-test (head obj))) #f)
            (else (loop (tail obj)))))))))
  (define (make-list=? element-comparator type-test empty? head tail)
    (lambda (a b)
      (let ((elem=? (comparator-equality-predicate element-comparator)))
	(let loop ((a a) (b b))
	  (cond
	   ((and (empty? a) (empty? b) #t))
	   ((empty? a) #f)
	   ((empty? b) #f)
	   ((elem=? (head a) (head b)) (loop (tail a) (tail b)))
	   (else #f))))))
  (make-comparator
   (make-list-type-test element-comparator type-test empty? head tail)
   (make-list=? element-comparator type-test empty? head tail)
   (make-list<? element-comparator type-test empty? head tail)
   (make-list-hash element-comparator type-test empty? head tail)))

(define (make-list<? element-comparator type-test empty? head tail)
  (lambda (a b)
    (let ((elem=? (comparator-equality-predicate element-comparator))
	  (elem<? (comparator-ordering-predicate element-comparator)))
      (let loop ((a a) (b b))
	(cond
	 ((and (empty? a) (empty? b)) #f)
	 ((empty? a) #t)
	 ((empty? b) #f)
	 ((elem=? (head a) (head b)) (loop (tail a) (tail b)))
	 ((elem<? (head a) (head b)) #t)
	 (else #f))))))

(define (make-list-hash element-comparator type-test empty? head tail)
  (lambda (obj)
    (let ((elem-hash (comparator-hash-function element-comparator))
	  (acc (make-hasher)))
      (let loop ((obj obj))
	(cond
	 ((empty? obj) (acc))
	 (else (acc (elem-hash (head obj))) (loop (tail obj))))))))

(define (make-vector-comparator element-comparator type-test length ref)
  (define (make-vector-type-test element-comparator type-test length ref)
    (lambda (obj)
      (and
       (type-test obj)
       (let ((elem-type-test (comparator-type-test-predicate element-comparator))
	     (len (length obj)))
	 (let loop ((n 0))
	   (cond
            ((= n len) #t)
            ((not (elem-type-test (ref obj n))) #f)
            (else (loop (+ n 1)))))))))
  (make-comparator
   (make-vector-type-test element-comparator type-test length ref)
   (make-vector=? element-comparator type-test length ref)
   (make-vector<? element-comparator type-test length ref)
   (make-vector-hash element-comparator type-test length ref)))

(define (make-vector=? element-comparator type-test length ref)
  (lambda (a b)
    (and
     (= (length a) (length b))
     (let ((elem=? (comparator-equality-predicate element-comparator))
	   (len (length b)))
       (let loop ((n 0))
	 (cond
	  ((= n len) #t)
	  ((elem=? (ref a n) (ref b n)) (loop (+ n 1)))
	  (else #f)))))))

(define (make-vector<? element-comparator type-test length ref)
  (lambda (a b)
    (cond
     ((< (length a) (length b)) #t)
     ((> (length a) (length b)) #f)
     (else
      (let ((elem=? (comparator-equality-predicate element-comparator))
	    (elem<? (comparator-ordering-predicate element-comparator))
	    (len (length a)))
	(let loop ((n 0))
	  (cond
	   ((= n len) #f)
	   ((elem=? (ref a n) (ref b n)) (loop (+ n 1)))
	   ((elem<? (ref a n) (ref b n)) #t)
	   (else #f))))))))

(define (make-vector-hash element-comparator type-test length ref)
  (lambda (obj)
    (let ((elem-hash (comparator-hash-function element-comparator))
	  (acc (make-hasher))
	  (len (length obj)))
      (let loop ((n 0))
	(cond
	 ((= n len) (acc))
	 (else (acc (elem-hash (ref obj n))) (loop (+ n 1))))))))

(define %*comparator-registered*
  (list (make-comparator
	 (lambda (obj) #t)
	 (lambda (a b) #t)
	 (lambda (a b) #f)
	 (lambda (obj) 0))))

(define (comparator-register-default! comparator)
  (set! %*comparator-registered* (cons comparator %*comparator-registered*)))

(define (registered-index obj)
  (let loop ((registered %*comparator-registered*) (i 9))
    (and (not (null? registered))
	 (let ((comparator (car registered)))
	   (if (comparator-test-type comparator obj)
	       i
	       (loop (cdr registered) (+ i 1)))))))

(define (registered index)
  (list-ref %*comparator-registered* (- index 9)))

(define (make-default-comparator)
  (define (boolean<? a b)
    (and (not a) b))
  (define (complex<? a b)
    (if (= (real-part a) (real-part b))
	(< (imag-part a) (imag-part b))
	(< (real-part a) (real-part b))))
  (define (dispatch-equality type a b)
    (case type
      ((0) #t)
      ((1) ((make-pair=? (make-default-comparator)
				     (make-default-comparator))
	    a b))
      ((2) (boolean=? a b))
      ((3) (char=? a b))
      ((4) (string=? a b))
      ((5) (symbol=? a b))
      ((6) (= a b))
      ((7) ((make-vector=? (make-default-comparator)
				       vector? vector-length vector-ref) a b))
      ((8) ((make-vector=? (make-comparator exact-integer?
							= < default-hash)
				       bytevector? bytevector-length
				       bytevector-u8-ref)
	    a b))
      (else (%=? (registered type) a b))))
  (define (dispatch-ordering type a b)
    (case type
      ((0) #f)
      ((1) ((make-pair<? (make-default-comparator)
				     (make-default-comparator))
	    a b))
      ((2) (boolean<? a b))
      ((3) (char<? a b))
      ((4) (string<? a b))
      ((5) (symbol<? a b))
      ((6) (complex<? a b))
      ((7) ((make-vector<? (make-default-comparator)
				       vector? vector-length vector-ref)
	    a b))
      ((8) ((make-vector<? (make-comparator exact-integer? = < default-hash)
			   bytevector? bytevector-length bytevector-u8-ref) a b))
      (else (%<? (registered type) a b))))
  (define (default-ordering a b)
    (let ((a-type (%constructor-object-type a))
	  (b-type (%constructor-object-type b)))
      (cond
       ((< a-type b-type) #t)
       ((> a-type b-type) #f)
       (else
	(dispatch-ordering a-type a b)))))
  (define (default-equality a b)
    (let ((a-type (%constructor-object-type a))
	  (b-type (%constructor-object-type b)))
      (if (= a-type b-type)
	  (dispatch-equality a-type a b) #f)))
  (make-comparator
   (lambda (obj) #t)
   default-equality
   default-ordering
   default-hash))
