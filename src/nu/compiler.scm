;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu compiler)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-28)
  #:use-module (srfi srfi-37)
  #:use-module (rnrs exceptions)
  #:use-module (nu config)
  #:use-module (nu list)
  #:use-module (nu error)
  #:use-module (nu syntax-object)
  #:use-module (nu expand)
  #:use-module (nu globals)
  #:use-module (nu io)
  #:use-module (nu expression)
  #:use-module (nu backend)
  #:export (main))

(define program-invocation-name (make-parameter #f))

(define (program-invocation-short-name)
  (let loop ((char* (reverse! (string->list (program-invocation-name))))
             (acc '()))
    (if (or (null? char*)
            (char=? (car char*)
                    #\/))
        (list->string acc)
        (loop (cdr char*) (cons (car char*) acc)))))

(define (copyright-year) 2019)

(define (version-etc opt name arg sources)
  (write-string (format "~a ~a~%"
                        (package)
                        (version)))
  (write-string (format "Copyright (C) ~a Marc Nieper-Wißkirchen~%"
                        (copyright-year)))
  (write-string "
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
")
  (exit 0))

(define (emit-bug-reporting-address)
  (write-string (format "~%Report bugs to: ~a~%" (package-bugreport)))
  (write-string (format "~a home: page: <~a>~%" (package-name) (package-url))))

(define (help opt name arg sources)
  (write-string (format "Usage: ~a [OPTION]... [fILE]...
Compile scheme code with Nu Scheme

  -h, --help     display this help and exit
  -v, --version  display version information and exit
"
                        (program-invocation-short-name)))
  (emit-bug-reporting-address)
  (exit 0))

(define (main args)
  (parameterize
      ((program-invocation-name (car args)))
    (guard (exc
            ((error-object? exc)
             (flush-output-port (current-output-port))
             (let ((irritants (error-object-irritants exc)))
               (receive (prefix irritants)
                   (cond ((and (not (null? irritants))
                               (source-location? (car irritants)))
                          (values (source-location->prefix (car irritants))
                                  (cdr irritants)))
                         ((and (not (null? irritants))
                               (syntax? (car irritants)))
                          (let ((srcloc (syntax-object-source-location (car irritants))))
                            (values (source-location->prefix srcloc)
                                    (cdr irritants))))
                         (else
                          (values (format "~a: " (program-invocation-short-name))
                                  irritants)))
                 (write-string prefix (current-error-port))
                 (write-string (apply format
                                      (error-object-message exc)
                                      irritants)
                               (current-error-port))))
             (newline (current-error-port))
             (exit 1)))
      (let ((source
             (args-fold (cdr args)
                        (list (option '(#\v "version") #f #f version-etc)
                              (option '(#\h "help") #f #f help))
                        (lambda (opt name arg sources)
                          (error "unrecognized command line option '~a'" name))
                        (lambda (operand source)
                          (when source
                            (error "more than one input file"))
                          operand)
                        #f)))
        (unless source
          (error "no input file"))
        (let ((output (string-append (basename source ".scm") ".c")))
          (let* ((program (guard (exc
                                  ((file-error? exc)
                                   (error "~a: error reading file" source)))
                            (read-file #f #f source #t)))
		 #; XXX!
		 (program (syntax->datum program))
		 #;
                 (expr (expand-r4rs program))
		 #;
		 (code (expression->datum expr)))
            (guard (exc
                    ((file-error? exc)
                     (error "~a: error writing file" output)))
	      (output-code program output))))))))
