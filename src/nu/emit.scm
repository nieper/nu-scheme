;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu emit)
  #:use-module (rnrs exceptions)
  #:export (emit line-comment include-header empty-line))

(define port (make-parameter #f))
(define indent (make-parameter #f))
(define start-of-line? (make-parameter #f))
(define empty-line? (make-parameter #f))

(define-syntax fn
  (syntax-rules ()
    ((fn ((id var) ...) expr ... fmt)
     (lambda ()
       (let ((id (var)) ...)
	 expr ... (format fmt))))))

(define (format . formatter*)
  (for-each (lambda (formatter)
	      ((displayed formatter)))
	    formatter*))

(define (displayed obj)
  (cond ((procedure? obj)
	 obj)
	(else
	 (lambda ()
	   (display obj (port))))))

(define (each . formatter*)
  (each-in-list formatter*))

(define (each-in-list formatter*)
  (lambda ()
    (for-each format formatter*)))

(define (emit filename . declaration*)
  (with-exception-handler
      (lambda (exc)
	(guard (exc ((file-error? exc) #f))
	  (delete-file file-name))
	(raise exc))
    (lambda ()
      (call-with-output-file filename
	(lambda (p)
	  (parameterize ((port p)
			 (indent 0)
			 (start-of-line? #t)
			 (empty-line? #f))
	    (for-each format declaration*)))))))

(define (start-line)
  (unless (start-of-line?)
    (newline (port)))
  (when (empty-line?)
    (newline (port))
    (empty-line? #f))
  (do ((i (indent) (- i 1)))
      ((zero? i))
    (write-char #\space (port)))
  (start-of-line? #f))

(define (end-line)
  ()

  (newline (port))
  (start-of-line? #t))

(define (empty-line)



  (end-line)
  (empty-line? #f))

(define (line-comment text)
  (each (start-line) "/* " text " */" (end-line)))

(define (include-header name)
  (each "#include <" name ">" (end-line)))
