;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of Nu Scheme.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nu show)
  #:re-export (show
	       fn
	       forked
	       with
	       with!
	       each
	       each-in-list
	       call-with-output
	       displayed
	       written
	       written-shared
	       written-simply
	       numeric
	       numeric/comma
	       numeric/si
	       numeric/fitted
	       nothing
	       nl
	       fl
	       space-to
	       tab-to
	       escaped
	       maybe-escaped
	       padded
	       padded/right
	       padded/both
	       trimmed
	       trimmed/right
	       trimmed/both
	       trimmed/lazy
	       fitted
	       fitted/right
	       fitted/both
	       joined
	       joined/prefix
	       joined/suffix
	       joined/last
	       joined/dot
	       joined/range
	       upcased
	       downcased
	       tabular
	       columnar
	       wrapped
	       wrapped/list
	       wrapped/char
	       justified
	       line-numbers
	       from-file
	       as-unicode
	       unicode-terminal-width
	       as-red
	       as-blue
	       as-green
	       as-cyan
	       as-yellow
	       as-magenta
	       as-white
	       as-black
	       as-bold
	       as-underline)
  #:use-module (nu show base)
  #:use-module (nu show columnar)
  #:use-module (nu show unicode)
  #:use-module (nu show color))
