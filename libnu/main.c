/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <libnuscheme.h>
#include <locale.h>
#include <stdlib.h>

#include "closeout.h"
#include "nuthread.h"
#include "progname.h"



#include <stdio.h>

#ifdef NU_CALLEE_SAVED_REGISTERS
volatile thread_local sig_atomic_t nu_the_sig;
#else
thread_local struct nu_thread_local_register_file nu_thread_local_register_file;
#endif

static void run (NuValue arg);

LIBNUSCHEME_DLL_EXPORTED int main (int argc, char **argv)
{
  set_program_name (argv [0]);

  setlocale (LC_ALL, "");

  atexit (close_stdout);

  nu_init_threads (2, 65536L);

  nu_thread_create (run, NU_UNSPECIFIED, NULL);
  do
    {
      nu_join_threads ();
      fprintf (stderr, "TODO: Run GC.\n");
      // RUN GC. TODO We have write the GC and the interface to the
      // threads management.  All living data shall be in the thunks.
      // We could loop over it...
    }
  while (nu_resume_threads ());
}

void
run (NuValue arg)
{
  NU_REG_1 = NU_UNSPECIFIED;
  NU_REG_2 = NU_UNSPECIFIED;
  NU_REG_3 = NU_UNSPECIFIED;
  NU_REG_4 = NU_UNSPECIFIED;
  nu_run ();
}
