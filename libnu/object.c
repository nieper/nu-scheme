/*
 * This file is part of Nu Scheme.
 *
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <assert.h>
#include <string.h>

#include "nuobject.h"

void
nu_default_entry_0 (NuValue self, NuValue cont)
{
  assert (0);
  NU_UNREACHABLE;
}

void
nu_default_entry_1 (NuValue self, NuValue cont, NuValue arg1)
{
  assert (0);
  NU_UNREACHABLE;
}

void
nu_default_entry_2 (NuValue self, NuValue cont, NuValue arg1, NuValue arg2)
{
  assert (0);
  NU_UNREACHABLE;
}

void
nu_default_entry_3 (NuValue self, NuValue cont, NuValue arg1,
			 NuValue arg2, NuValue arg3)
{
  assert (0);
  NU_UNREACHABLE;
}

void
nu_default_entry_x (NuValue self, NuValue cont, NuValue arg1,
		    NuValue arg2, NuValue arg3, NuValue args)
{
  assert (0);
  NU_UNREACHABLE;
}

void
nu_default_cont_entry (NuValue self, NuValue arg)
{
  assert (0);
  NU_UNREACHABLE;
}

/* /\* Garbage collector methods. *\/ */

/* NuObject * */
/* default_forward (NuObject *obj) */
/* { */
/*   return obj; */
/* } */

/* void */
/* default_update (NuObject *obj) */
/* { */
/*   /\* Do nothing. *\/ */
/* } */

/* NuObject * */
/* forward_pair (NuObject *obj) */
/* { */
/*   NuObject *copy = NU_ALLOCATE (NuPair); */
/*   copy->pair.car = obj->pair.car; */
/*   copy->pair.cdr = obj->pair.cdr; */
/*   nu_forward_init (&obj->pair.fwd, copy); */
/*   update_pair (copy); */
/*   return copy; */
/* } */

/* void */
/* update_pair (NuObject *obj) */
/* { */
/*   obj->pair.car = nu_object_forward (obj->pair.car); */
/*   obj->pair.cdr = nu_object_forward (obj->pair.cdr); */
/* } */

/* NuObject * */
/* forward_fixnum (NuObject *obj) */
/* { */
/*   NuObject *copy = NU_ALLOCATE (NuFixnum); */
/*   copy->fx.val = obj->fx.val; */
/*   nu_forward_init (&obj->fx.fwd, obj); */
/*   return copy; */
/* } */

/* NuObject * */
/* nu_label_forward (NuObject *obj) */
/* { */
/*   NuLabel *label = get_label (obj); */
/*   NuObject **clo = get_closure (obj); */
/*   size_t clo_size = sizeof (*clo) * label->slots; */
/*   NuObject *copy = nu_allocate (clo_size); */
/*   memcpy (copy, clo, clo_size); */
/*   for (size_t i = 0; i < label->labels; ++i) */
/*     { */
/*       NuForwardTag *tag = (NuForwardTag *) NU_ALLOCATE_TEMP (NuForwardTag); */
/*       // XXX TODO: tag.tag.forward & tag.tag.update */
/*       tag->obj = (NuObject *) ((NuObject **) copy + label->slot); */
/*       clo [i] = (NuObject *) tag; // cast as value */
/*     } */
/*   nu_label_update (copy); */
/*   return (NuObject *) ((NuObject **) copy + label->slot); */
/* } */

/* void */
/* nu_label_update (NuObject *obj) */
/* { */
/*   NuLabel *label = get_label (obj); */
/*   NuObject **clo = get_closure (obj); */
/*   for (size_t i = label->labels; i < label->slots; ++i) */
/*     clo [i] = nu_object_forward (clo [i]); */
/* } */

/* NuObject *label_link_forward (NuObject *obj) */
/* { */
/*   NuLabel *label = *((NuProcedure *) obj); */
/*   return label->fwd; */
/* } */

/* NuLabel *get_label (NuObject *proc) */
/* { */
/*   return *((NuProcedure *) proc); */
/* }; */

/* NuObject **get_closure (NuObject *proc) */
/* { */
/*   NuLabel *label = get_label (proc); */
/*   return ((NuObject **) proc) - label->slot; */
/* }; */

/* /\* Continuation procedures. *\/ */
/* NuObject *forward_cont_procedure (NuObject *proc) */
/* { */
/*   size_t n = offsetof (NuContProcedure, var) */
/*     + proc->cont.num_vars * sizeof (NuObject *); */
/*   NuObject *copy = nu_allocate (n); */
/*   memcpy (copy, proc, n); */
/*   nu_forward_init (&proc->cont.fwd, copy); */
/*   update_cont_procedure (copy); */
/*   return copy; */
/* } */

/* void update_cont_procedure (NuObject *proc) */
/* { */
/*   for (int i = 0; i < proc->cont.num_vars; ++i) */
/*     proc->cont.var [i] = nu_object_forward (proc->cont.var [i]); */
/* } */

/* TODO!
extern LIBNUSCHEME_DLL_EXPORTED const NuObject *const NU_UNSPECIFIED = ...;
extern LIBNUSCHEME_DLL_EXPORTED const NuObject *const NU_NULL = ...;
extern LIBNUSCHEME_DLL_EXPORTED const NuObject *const NU_FALSE = ...;
extern LIBNUSCHEME_DLL_EXPORTED const NuObject *const NU_TRUE = ...;
extern LIBNUSCHEME_DLL_EXPORTED const NuObject *const NU_EOF_OBJECT = ...;


extern LIBNUSCHEME_DLL_EXPORTED inline unsigned nu_tag (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline unsigned nu_immediate_type (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_immediatep (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_pointerp (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline NuObject nu_null (void);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_nullp (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline NuObject nu_eof_object (void);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_eof_object_p (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline NuObject nu_void (void);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_void_p (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline NuObject nu_boolean (bool);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_boolean_p (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_false_p (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_true_p (NuObject);
extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_pairp (NuObject);
*/
