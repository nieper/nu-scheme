/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NUTHREAD_H_INCLUDED
#define NUTHREAD_H_INCLUDED

#include <libnuscheme.h>
#include <stdbool.h>

typedef struct nu_thread NuThread;
typedef struct nu_root_object_iterator NuRootObjectIterator;

void nu_init_threads (size_t, size_t);
void nu_join_threads (void);
bool nu_resume_threads (void);
void nu_thread_create (NuContinuation *, NuValue, NuThread **);

/* The following two functions are used by the garbage collector. */
void nu_root_object_iterator (NuRootObjectIterator *);
bool nu_root_object_iterator_next (NuRootObjectIterator *, NuValue **);

struct nu_root_object_iterator
{
  NuThread *thread;
};

#endif /* NUTHREAD_H_INCLUDED */
