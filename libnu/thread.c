/*
 * Copyright (C) 2019 Marc Nieper-Wißkirchen.
 *
 * This file is part of Nu Scheme.
 *
 * Nu Scheme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>

#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <sigsegv.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <threads.h>
#include <unistd.h>

#include "error.h"
#include "pagealign_alloc.h"
#include "nuthread.h"
#include "xalloc.h"

typedef struct nu_thunk NuThunk;
static void free_threads (void);
static int sigsegv_handler (void *, int);
static void stackoverflow_handler (int, stackoverflow_context_t);
static int area_handler (void *, void *);
static NuThunk *thunk_create (NuContinuation *cont, NuValue arg);
static void thread_join (NuThread *);
static void thread_resume (NuThread *);
static void thread_free (NuThread *);
static struct card *card_create (void);
static void free_cards (void);
static bool cards_available (void);
static void protect (void *addr, size_t len);
static void unprotect (void *addr, size_t len);
static void lock (void);
static void unlock (void);
static void *start (void *arg);
static size_t align (size_t n);

extern LIBNUSCHEME_DLL_EXPORTED inline bool nu_thread_signal_p ();

struct card
{
  struct card *prev_card;
  unsigned char *mem;
  void *sig_stack;
};

struct nu_thunk
{
  NuContinuation *cont;
  NuValue arg;
};

struct nu_thread
{
  NuThread *prev_thread;
  NuThunk *thunk;
  sem_t sem;
  pthread_t thread, parent_thread;
  bool has_parent;
  struct card *card;
  void *ticket;
};

static unsigned char *extra_stack;
static size_t page_size;
static size_t guard_size;
static size_t card_size;
static size_t max_cards;
static size_t num_cards;
static struct card *last_card;
static NuThread *last_thread;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_attr_t attr;
static sigsegv_dispatcher dispatcher;
thread_local NuThread *this_thread;

void
nu_init_threads (size_t mc, size_t cs)
{
  num_cards = 0;
  last_card = NULL;
  max_cards = mc;
  page_size = getpagesize ();
  guard_size = align (PTHREAD_STACK_MIN);
  card_size = align (cs);
  extra_stack = pagealign_xalloc (SIGSTKSZ);
  if (stackoverflow_install_handler (stackoverflow_handler,
				     extra_stack,
				     SIGSTKSZ))
    error (EXIT_FAILURE, 0, "stackoverflow_install_handler");
  sigsegv_init (&dispatcher);
  if (sigsegv_install_handler (sigsegv_handler) < 0)
    error (EXIT_FAILURE, 0, "sigsegv_install_handler");
  if (pthread_mutex_init (&mutex, NULL) < 0)
    error (EXIT_FAILURE, errno, "pthread_mutex_init");
  if (pthread_attr_init (&attr))
    error (EXIT_FAILURE, errno, "pthread_attr_init");
  atexit (free_threads);
}

void
nu_thread_create (NuContinuation *cont, NuValue arg, NuThread **thread_ptr)
{
  NuThread *thread = XMALLOC (NuThread);
  if (sem_init (&thread->sem, 0, 0) < 0)
    error (EXIT_FAILURE, errno, "sem_init");
  thread->thunk = thunk_create (cont, arg);
  thread->has_parent = false;
  lock ();
  thread->prev_thread = last_thread;
  last_thread = thread;
  unlock ();
  thread_resume (thread);
  if (thread_ptr != NULL)
    *thread_ptr = thread;
}

void
nu_join_threads (void)
{
  NuThread *last_joined_thread = NULL;
  lock ();
  while (last_thread != NULL)
    {
      NuThread *joining_thread = last_thread;
      last_thread = last_thread->prev_thread;
      unlock ();
      thread_join (joining_thread);
      sigsegv_unregister (&dispatcher, joining_thread->ticket);
      if (joining_thread->thunk != NULL)
	{
	  joining_thread->prev_thread = last_joined_thread;
	  last_joined_thread = joining_thread;
	}
      else
	thread_free (joining_thread);
      lock ();
    }
  unlock ();
  last_thread = last_joined_thread;
}

bool
nu_resume_threads (void)
{
  free_cards ();
  NuThread *last_resumable_thread = last_thread;
  last_thread = NULL;
  while (last_resumable_thread != NULL)
    {
      NuThread *resumable_thread = last_resumable_thread;
      last_resumable_thread = resumable_thread->prev_thread;
      thread_resume (resumable_thread);
    }
  return last_thread != NULL;
}

void
nu_thread_suspend (NuContinuation* cont, NuValue arg)
{
  NuThunk *thunk = thunk_create (cont, arg);
  if (cards_available ())
    {
      this_thread->thunk = thunk;
      this_thread->parent_thread = this_thread->thread;
      this_thread->has_parent = true;
      thread_resume (this_thread);
      pthread_exit (NULL);
    }
  else
    {
      sem_post (&this_thread->sem);
      pthread_exit (thunk);
    }
}

void
nu_thread_exit (void)
{
  sem_post (&this_thread->sem);
  pthread_exit (NULL);
}

void
nu_root_object_iterator (NuRootObjectIterator *iter)
{
  iter->thread = last_thread;
}

bool
nu_root_object_iterator_next (NuRootObjectIterator *iter, NuValue **val)
{
  if (iter->thread == NULL)
    return false;
  if (val != NULL)
    *val = &iter->thread->thunk->arg;
  iter->thread = iter->thread->prev_thread;
  return true;
}

void
free_threads (void)
{
  pthread_attr_destroy (&attr);
  if (pthread_mutex_destroy (&mutex) < 0)
    error (EXIT_FAILURE, errno, "pthread_mutex_destroy");
  sigsegv_deinstall_handler ();
  stackoverflow_deinstall_handler ();
  pagealign_free (extra_stack);
}

int
sigsegv_handler (void *fault_address, int serious)
{
  return sigsegv_dispatch (&dispatcher, fault_address);
}

void
stackoverflow_handler (int emergency, stackoverflow_context_t scp)
{
  return;
}

int
area_handler (void *fault_address, void *arg)
{
  NuThread *thread = arg;
  struct card *card = thread->card;
  unprotect (card->mem, guard_size);
  unprotect (card->mem + guard_size + card_size, guard_size);
  NU_SIG = 1;
  return 1;
}

NuThunk *
thunk_create (NuContinuation *cont, NuValue arg)
{
  NuThunk thunk = {.cont = cont, .arg = arg};
  NuThunk *p = XMALLOC (NuThunk);
  memcpy (p, &thunk, sizeof thunk);
  return p;
}

void
thread_join (NuThread *thread)
{
  sem_wait (&thread->sem);
  pthread_join (thread->thread, (void **) &thread->thunk);
}

void
thread_resume (NuThread *thread)
{
  thread->card = card_create ();
  thread->ticket =
    sigsegv_register (&dispatcher, thread->card->mem,
		      2 * guard_size + card_size,
		      area_handler, thread);
  pthread_attr_setstack (&attr, thread->card->mem + guard_size, card_size);
  if (pthread_create (&thread->thread, &attr, start, thread) < 0)
    error (EXIT_FAILURE, errno, "pthread_create");
}

void
thread_free (NuThread *thread)
{
  if (sem_destroy (&thread->sem) < 0)
    error (EXIT_FAILURE, errno, "sem_destroy");
  free (thread);
}

struct card *
card_create (void)
{
  struct card *card = XMALLOC (struct card);
  card->sig_stack = pagealign_xalloc (SIGSTKSZ);
  card->mem = pagealign_xalloc (2 * guard_size + card_size);
  protect (card->mem, guard_size);
  protect (card->mem + guard_size + card_size, guard_size);
  lock ();
  card->prev_card = last_card;
  num_cards += 1;
  last_card = card;
  unlock ();
  return card;
}

static void
free_cards (void)
{
  while (last_card != NULL)
    {
      struct card *prev_card = last_card->prev_card;
      pagealign_free (last_card->sig_stack);
      pagealign_free (last_card->mem);
      free (last_card);
      last_card = prev_card;
    }
  num_cards = 0;
}

static bool
cards_available (void)
{
  lock ();
  bool res = num_cards < max_cards;
  unlock ();
  return res;
}

static void protect (void *addr, size_t len)
{
  if (mprotect (addr, len, PROT_READ) < 0)
    error (EXIT_FAILURE, errno, "mprotect");
}

static void unprotect (void *addr, size_t len)
{
  if (mprotect (addr, len, PROT_READ | PROT_WRITE) < 0)
    error (EXIT_FAILURE, errno, "mprotect");
}

static void lock (void)
{
  if (pthread_mutex_lock (&mutex) < 0)
    error (EXIT_FAILURE, errno, "pthread_mutex_lock");
}

static void unlock (void)
{
  if (pthread_mutex_unlock (&mutex) < 0)
    error (EXIT_FAILURE, errno, "pthread_mutex_unlock");
}

void *
start (void *t)
{
  NU_INIT_SIGNAL;
  NU_SIG = 0;
  NuThread *thread = t;
  if (thread->has_parent)
    {
      pthread_join (thread->parent_thread, NULL);
      thread->has_parent = false;
    }
  struct nu_thunk *thunk = thread->thunk;
  NuContinuation *cont = thunk->cont;
  NuValue arg = thunk->arg;
  free (thunk);
  thread->thunk = NULL;
  this_thread = thread;
  stack_t ss = (stack_t) { .ss_sp = thread->card->sig_stack,
			   .ss_size = SIGSTKSZ,
			   .ss_flags = 0 };
  if (sigaltstack (&ss, NULL) < 0)
    error (EXIT_FAILURE, 0, "sigaltstack");
  cont (arg); // TODO: Later we want the continuation to not return.
  assert (0); // XXX (see line above)
  sem_post (&thread->sem);
  return NULL;
}

size_t
align (size_t n)
{
  n = (n + page_size - 1) & ~(page_size - 1);
  return n;
}
